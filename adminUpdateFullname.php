<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
    $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if(in_array($_FILES["file"]["type"],$allowedFileType))
    {
        $targetPath = 'uploadExcel/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);

            foreach ($Reader as $Row)
            {

                $username = "";
                if(isset($Row[0]))
                {
                    $username = mysqli_real_escape_string($conn,$Row[0]);
                }
                $fullname = "";
                if(isset($Row[0]))
                {
                    $fullname = mysqli_real_escape_string($conn,$Row[1]);
                }
                // $point = "";
                // if(isset($Row[1]))
                // {
                //     $point = mysqli_real_escape_string($conn,$Row[7]);
                // }

                // if (!empty($userName) || !empty($point) || !empty($userID))
                if (!empty($username) || !empty($fullname))
                {
                    // $query = "UPDATE `user` SET `fullname`='".$fullname."' WHERE  username = '".$username."' ";
                    $query = "UPDATE `user` SET `fullname`='".$fullname."' WHERE  username = '".$username."' ";
                    $result = mysqli_query($conn, $query);
                    if (! empty($result))
                    {
                        echo "<script>alert('Excel Uploaded !');window.location='../adminUpdateFullname.php'</script>";  
                        // echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminUpdateFullname.php'</script>";  
                    }
                    else
                    {
                        echo "<script>alert('Fail To Upload !');window.location='../adminUpdateFullname.php'</script>";  
                        // echo "<script>alert('Fail To Upload !');window.location='../poppif/adminUpdateFullname.php'</script>";  
                    }
                }
                else
                {
                    // echo "dunno";    
                    echo "<script>alert('ERROR !');window.location='../adminUpdateFullname.php'</script>";  
                    // echo "<script>alert('ERROR');window.location='../poppif/adminUpdateFullname.php'</script>";   
                }

            }

        }
    }
    else
    {
        echo "<script>alert('ERROR 2 !');window.location='../adminUpdateFullname.php'</script>";  
        // echo "<script>alert('ERROR 2');window.location='../poppif/adminUpdateFullname.php'</script>";    
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminUpdateFullname.php" />
    <link rel="canonical" href="https://victory5.co/adminUpdateFullname.php" />
    <meta property="og:title" content="Admin Update Fullname  | Victory 5" />
    <title>Admin Update Fullname  | Victory 5</title>
   
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center">

    <!-- <div class="width100 text-center overflow margin-bottom50">
        <button class="clean blue-button one-button-width">
            <a href="addOnBonus.php" class="red-link">Add On Point</a>
        </button>
    </div>  -->
	<h1 class="pop-h1">Update Fullname</h1>
    <div class="outer-container">
        <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
            <label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
            <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
            <button type="submit" id="submit" name="import"  class="clean blue-button one-button-width"><?php echo _JS_SUBMIT ?></button>
            <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
        </form>
    </div>

    <div class="clear"></div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>