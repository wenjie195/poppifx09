<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/editPassword.php" />
    <link rel="canonical" href="https://victory5.co/editPassword.php" />
    <meta property="og:title" content="Edit Password  | Victory 5" />
    <title>Edit Password  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">

    <form action="utilities/editPasswordFunction.php" method="POST">
    <div class="width100 overflow text-center">
    	<img src="img/password.png" class="middle-title-icon" alt="<?php echo _USERHEADER_EDIT_PASSWORD ?>" title="<?php echo _USERHEADER_EDIT_PASSWORD ?>">
    </div>    
    <div class="width100 overflow"> 
        <h1 class="h1-title pop-h1 text-center"><?php echo _USERHEADER_EDIT_PASSWORD ?></h1>
    </div>
    <div class="password-width margin-auto overflow">
        <div class="per-input">
            <p class="input-top-text"><?php echo _JS_CURRENT_PASSWORD ?></p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="<?php echo _JS_CURRENT_PASSWORD ?>" id="current_password" name="current_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>
        </div>
        
        <div class="clear"></div>

        <div class="per-input">
            <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>"  id="new_password" name="new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>
        </div>
		<div class="clear"></div>
        <div class="per-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_NEW_PASSWORD ?></p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
            </div>
        </div>
	</div>
        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>