<?php
require_once dirname(__FILE__) . '/multibankAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userDetails = $userRows[0];

// $docRows = getStatus($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $docDetails = $docRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
   
    <meta property="og:url" content="https://victory5.co/multibankViewSignature.php" />
    <link rel="canonical" href="https://victory5.co/multibankViewSignature.php" />
    <meta property="og:title" content="LPOA  | Victory 5" />
    <title>LPOA  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'multibankHeader.php'; ?>

<div class="width100 same-padding menu-distance">

    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");

    $userDoc = getStatus($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

  	<div class="document-div2 document-div3 black-text">

  		<!--<img src="img/multibank-white.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">-->
        <div class="page1-div">
        <h2 class="document-h2  black-text print-margin-top">Limited Power of Attorney<br>授权委托书</h2>
        <div class="left-client">
        	<p class="document-p  black-text">Client’s Account:<br>客户账户</p>
        </div>
        <div class="right-client-signature">
            <!-- <p class="print-data-p">MT4 ID XXXXXXXXXXXXXX</p> -->
            <p class="print-data-p"><?php echo $userDetails[0]->getMpId();?></p>
            <div class="print-border"></div>
        </div>
        <div class="clear"></div>
        <div class="left-client">
        	<p class="document-p  black-text">Client’s Name:<br>客户姓名</p>
        </div>
        <div class="right-client-signature">
            <!-- <p class="print-data-p">Full Name XXXXXXXXXXXXX</p> -->
            <!-- <p class="print-data-p"><?php //echo $userDetails[0]->getUsername();?></p> -->
            <p class="print-data-p"><?php echo $userDetails[0]->getFullname();?></p>
            <div class="print-border"></div>
        </div>
        <div class="clear"></div>        
        <div class="left-client">
        	<p class="document-p  black-text">Money Manager’s Name:<br>资金经理姓名</p>
        </div>
        <div class="right-client-signature">
        	<img src="img/logo.png" class="poppifx4u-signature">
            <div class="print-border"></div>
        </div>
        <div class="clear"></div>          
        <div class="left-client">
        	<p class="document-p  black-text">Money Manager’s Address:<br>资金经理地址</p>
        </div>
        <div class="right-client-signature">
            <!-- <p class="print-data-p">1XX, XXXXXXXX XXXXXXXXXXXXXX, XXXXXXXXXX XXXXXX, XXXXXXX, XXXX XXXXXXXX XXXXXXXXXXXX</p> -->
            <p class="print-data-p"></p>
            <div class="print-border"></div>
        </div>
        <div class="clear"></div>          
        
        <h2 class=" document-h2  black-text">TERMS & CONDITIONS<br>条款</h2>
        <p class="document-p  black-text eng-p">
            This Agreement sets out the terms that the Money Manager listed above may act on behalf of the named client in its dealings with MEX.</p>
<p class="document-p  black-text cn-p">此协议（授权委托书）授权上述指定资金经理可以代表上述指定客户在MEX开立账户并操作交易。</p>
<p class="document-p  black-text eng-p">
It also sets out the details of any remuneration that may take effect under this Agreement in relation to the set up and trading on the client’s Multi-Account Manager (MAM) by the Money Manager.</p>
<p class="document-p  black-text cn-p">
此协议同时也规定资金经理可以从客户委托交易账户中提取一定的资金作为报酬。</p>
<p class="document-p  black-text eng-p">
The client is unable to trade in their MAM unless this Agreement is terminated by either party given prior written notice as required by Paragraph 3 of Acknowledgment below. However, the client can see all current open trades, Profit & Loss and trade history in the MAM.</p>
<p class="document-p  black-text cn-p">
客户并不能够自己操作其委托交易账户，除非此协议被任何一方书面终止（如确认声明中第三条款所述）。但顾客可以在其账户中看到所有的开启仓位，盈亏状况和交易记录。</p>
<p class="document-p  black-text eng-p">
The client’s MAM may be pooled to other client’s MAM which has been managed by the same Money Manager.</p>
<p class="document-p  black-text cn-p">
客户的账户可能会被其资金经理同其他客户的账户合并交易。</p>
<p class="document-p  black-text eng-p">
All MAM has a standard leverage of 1:500. By signing this agreement, the client agrees that the original setting of the client’s account (if it differs from the above) to be changed to the above level.</p>
<p class="document-p  black-text cn-p">
所有MAM账户均采用1:500杠杆比例的标准设置，并不计算账户经理的任何形式的佣金。选择签署此协议，客户即同意其账户的初始设置（如有不同）更改为上述水平。</p>
<p class="document-p  black-text eng-p">
The client can at any time withdraw fund, however this may result in insufficient margin in the MAM. Hence this may lead to force liquidation. Since the MAM may be placed in a pool, withdrawal of fund from the client’s MAM may result in greater loss than expected or the client to trade on their own.</p>
<p class="document-p  black-text cn-p">
客户可以随时提款，但这可能会导致账户保证金不足，而由此产生仓位被强制平仓。由于MAM账户可能会与其他账户合并交易，因此提款可能导致比预期或自己交易更大的损失。</p>
        </p>
        </div>
         <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 1 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
       <div class="page-break"></div>

        
        
        <div class="page1-div">
        <h2 class=" document-h2  black-text print-margin-top2">AUTHORISATION<br>授权</h2>
        <p class="document-p  black-text eng-p">
         The client hereby gives the above Money Manager full permission to:</p>
         <p class="document-p  black-text cn-p">
客户在此授予资金经理以下权利：</p>
<p class="document-p  black-text eng-p">
1) Open MAM(s) in the client’s name and on the client’s behalf with MEX.</p>
<p class="document-p  black-text cn-p">
1)资金经理以客户名义在MEX开立交易账户。</p>
<p class="document-p  black-text eng-p">
2) Provide MEX all the required documentation needed to open a MAM.</p>
<p class="document-p  black-text cn-p">
2) 资金经理提供给MEX开户所需的相关资料。</p>
<p class="document-p  black-text eng-p">
3) Accept and receive any documentation and information from MEX relating to the client’s MAM and executed transactions.</p>
<p class="document-p  black-text cn-p">
3) 资金经理可以接收MEX发送的客户文件及交易信息。</p>
<p class="document-p  black-text eng-p">
4) Trade and place orders on the client’s MAM(s) with MEX.</p>
<p class="document-p  black-text cn-p">
4) 资金经理在客户交易账户中建仓交易或下单。</p>
		</p>       
        <h2 class=" document-h2  black-text">DEPOSITS<br>入金</h2>
        <p class="document-p  black-text eng-p">
The client understands that this Agreement, while granting the Money Manager access to the client’s MAMs, does not allow the Money Manager to make deposits to or withdrawals from the client’s MAM either directly or indirectly.</p>
<p class="document-p  black-text cn-p">
客户明白此委托书虽然授权资金经理操作客户的交易账户，但不允许资金经理直接或者间接在客户账户中进行出入金操作。</p>        
        </p>
        <h2 class=" document-h2  black-text">THE MONEY MANAGER<br>资金经理</h2>        
<p class="document-p  black-text eng-p">
1) The Money Manager, although appointed by the client, will have to pass the complete identification and Customer Due Diligence (CDD) checks by MEX before the Money Manager will be allowed to act on behalf of any client.</p>
<p class="document-p  black-text cn-p">
1) 尽管资金经理由客户授权，但在可以代替客户交易之前仍然需要通过MEX完整的身份验证及尽职调查。</p>
<p class="document-p  black-text eng-p">
2) MEX is not responsible for any action taken by the Money Manager under the authorization unless is required by law.</p>
<p class="document-p  black-text cn-p">
2) 除法律规定之外, MEX对资金经理的授权行为不承担任何责任。</p>
<p class="document-p  black-text eng-p">
3) All instructions received from the Money Manager will legally be deemed to have been from the client.</p>
<p class="document-p  black-text cn-p">
3) 所有资金经理代客户发出的指令将被视为客户发出的指令。</p>
<p class="document-p  black-text eng-p">
4) No payments or withdrawals can be made into or from the client’s MAM by the Money Manager.</p>
<p class="document-p  black-text cn-p">
4) 资金经理不允许在客户的账户里进行出金或者入金的操作。出入金操作。</p>        
        </p> 
        </div>
         <div class="bottom-page-div bottom-page-div2">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 2 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
        <div class="page-break"></div>        
        
        
        <div class="page1-div">
        <h2 class=" document-h2  black-text print-margin-top2">ACKNOWLEDGEMENT<br>确认声明</h2>
        <p class="document-p  black-text eng-p">
        1) The client understands that if the client gives anyone access to the client’s MAM, including the Money Manager, by providing them with log in details and passwords, that the client is allowing them full access to trade on the MAM and that the client accepts that all trades, and the resulting profits and losses, are ultimately the client’s liability and responsibility.</p>
        <p class="document-p  black-text cn-p">
1) 客户明白如果客户提供给他人登陆信息及密码，包括给资金经理，即是授权他人在其账户中进行交易，那么客户就有最终责任和义务承担他人进入其账户进行的任何交易及损益。</p>
		<p class="document-p  black-text eng-p">
2) The client will indemnify MEX for all costs including losses incurred from trading on their MAM, expenses, remuneration and damages that arise from the enforcement of this Agreement.</p>
		<p class="document-p  black-text cn-p">
2) MEX将免于承担, 包括但并不仅限于下列费用：交易损失，履行此协议所产生的费用、酬劳和损失。</p>
		<p class="document-p  black-text eng-p">
3) This Agreement may be terminated by the Money Manager or the client at any time by either party sending written, signed and dated notification to MEX. It can also be terminated by MEX at any stage by notifying both the client and the Money Manager. The Client and the Money Manager should have solved all remaining issues (e.g. open position, remuneration payment) at the time of the termination.<br>
If the client requests to detach his or her MAM from the Money Manager, to MEX, this request will be regarded as to terminate this agreement. MEX can resume this MAM as a normal trading account however subject to no open positions remaining in the MAM. MEX offers no guarantee on how long this process will take.</p>
		<p class="document-p  black-text cn-p">
3) 此授权委托书可以随时被双方中的任何一方取消，只需要提供给MEX有签名和日期的书面通知。MEX也可以在任何阶段通知客户及资金经理取消此协议。客户和资金经理应在此协议终止时已经解决所有遗留问题（如开仓仓位，酬劳等）。<br>
如果客户要求其帐户与资金经理分离，对于MEX来讲，该请求将被视为终止本协议。MEX可以将此帐户作为一个普通的交易账户，但前提条件是该帐户中并无未平仓头寸。MEX并不能保证这一过程将需要多少时间完成。</p>
		<p class="document-p  black-text eng-p">
4) This Agreement is governed by the laws of British Virgin Islands and any disputes arising from it will be under that jurisdiction.</p>
		<p class="document-p  black-text cn-p">
4) 此授权委托书受英属维尔京群岛法律约束。执行本协议中产生的任何争议都受该地区司法管辖。</p>
		<p class="document-p  black-text eng-p">
5) This Agreement takes effect from the date that both the client and MEX signed this Agreement.</p>
		<p class="document-p  black-text cn-p">
5) 此协议从双方签署的日期生效。</p>
		<p class="document-p  black-text eng-p">
6) This Agreement runs in conjunction with the MEX full Terms and Conditions of the Client Service Agreement (CSA) and Product Disclosure Statement (PDS), and this Agreement takes precedence if any conflicts arise.</p>
		<p class="document-p  black-text cn-p">
6) 此授权委托书与MEX合约条款同时有效，如有出入，以此协议为准。</p>
		<p class="document-p  black-text eng-p">
7) MEX is not responsible for the legal effectiveness of this Agreement to any of the parties involved.</p>
		<p class="document-p  black-text cn-p">
7) MEX不对签署协议的任何一方承担法律效率责任。</p>
		<p class="document-p  black-text eng-p">
8) MEX, at its sole discretion, may refuse to accept an instruction from the Money Manager if it believes in doing so would create a conflict of interest or put it in jeopardy of breaking the law.</p>
		<p class="document-p  black-text cn-p">
8) MEX如果认定资金经理的指令会引发利益冲突或者违法，可能会单方面拒绝接收资金经理的指令。</p>
		<p class="document-p  black-text eng-p">
9) At no stage will MEX be liable to the client or the Money Manager if it refuses to act on the Account Manager’s instructions.</p>
		<p class="document-p  black-text cn-p">
9) 在任何阶段，如果资金经理的指令不被接收，MEX对客户及资金经理均不负有责任。</p>
		<p class="document-p  black-text eng-p">
10) If there is a dispute between the client and the Money Manager, they must solve the issue between themselves or seek legal advice. MEX has no responsibility to resolve or become involved in or mediate the dispute or the dispute resolution.</p>
		<p class="document-p  black-text cn-p">
10) 如果客户和操盘帐户经理发生纠纷，双方需要自行解决或寻求法律意见。MEX没有责任解决或协助提供解决方案。</p>
        </p>
        </div>
         <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 3 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
        <div class="page-break"></div>          
        
        
        
        <div class="page1-div">
        <h2 class=" document-h2  black-text print-margin-top2">REMUNERATION<br>酬劳</h2>        
        <p class="document-p  black-text eng-p">
The client understands and accepts that there may be an increase in the spread of certain markets over and above the core MEX spreads to help cover the Money Manager’s associated third party software costs and that the Money Manager will charge the client a performance and management fee which will be debited from their MAM Account directly by MEX on the Money Manager’s behalf in accordance with the performance fees set out below.</p>
<p class="document-p  black-text cn-p">
客户理解并接受在某些市场状况下MAM账户里的点差可能会比MEX的核心报价点差高, 高出的部分用于帮助支付资金经理使用第三方软件的成本。资金经理也可以向客户收取操盘及管理费用, 这部分费用将由MEX代资金经理按照此协议规定在客户账户中直接扣除。</p>              
        </p>  
        <h2 class=" document-h2  black-text">PERFORMANCE FEE<br>收益表现费用</h2>
        <p class="document-p  black-text eng-p">
		The client authorizes MEX to debit their MAM(s), until further notice in writing, a performance fee will be calculated based on High-Water Mark in practice (See Appendix 1), out of positive funds held on their MAM, and payable within 21 days (if applicable) from the end of the selected period , to the Money Manager based on the following criteria:</p>
        <p class="document-p  black-text cn-p">
客户授权MEX从其账户中扣除收绩效费，计算方法按照高水位条款准则(请参考附件1)直至委托终止。并在选择的周期到期之后21天内（如适用）按以下标准支付给资金经理：
		
        </p>
        <!-- <div class="straight-p">
        	<p class="straight-p1">Calculation Period: </p>
            <p class="straight-p2">22/2/2020</p>
            <p class="straight-p3">to</p>
            <p class="straight-p2">22/2/2022</p>
        </div>
		<div class="clear"></div>
        <div class="straight-p">
        	<p class="straight-p1">费用结算周期: </p>
            <p class="straight-p2">22/2/2020</p>
            <p class="straight-p3">至</p>
            <p class="straight-p2">22/2/2022</p>
        </div>         -->

        <div class="straight-p">
        	<p class="straight-p1">Calculation Period: </p>
            <p class="straight-p2">  </p>
            <p class="straight-p3">to</p>
            <p class="straight-p2">  </p>
        </div>
		<div class="clear"></div>
        <div class="straight-p">
        	<p class="straight-p1">费用结算周期: </p>
            <p class="straight-p2">  </p>
            <p class="straight-p3">至</p>
            <p class="straight-p2">  </p>
        </div>   

        <div class="clear"></div>
        <p class="small-info"><i>*Sample: 1st to 31st of the period 例子周期：1号到 31号</i></p>
        <div class="big-container-div">
        	<div class="left-container-div1">
            	<div class="left-fee">
                	<p class="document-p  black-text">
                    	<b>Management Fee:<br>
                        管理费</b>
                    </p>
                </div>
                <!-- <div class="right-fee">
                	<p class="line-p">10</p><p class="next-to-line-p">%</p>
                </div> -->

                <div class="right-fee">
                	<p class="line-p"></p><p class="next-to-line-p"></p>
                </div>

            </div>
            <div class="right-container-div1">
            	<div class="top-div1">
                	<p class="document-p  black-text">
                    	On equity before 
                      
                          <input type="checkbox" id="eq-before"  class="input-checkbox" />
    					  <label for="eq-before"></label>
                          
						 /after 
                        
                          <input type="checkbox" id="eq-after"  class="input-checkbox" />
    					  <label for="eq-after"></label>
                         
                         
                          the period<br>
                        按计算净值周期之前
                       
                          <input type="checkbox" id="eq-before2"  class="input-checkbox" />
    					  <label for="eq-before2"></label>
                          
                        
                        / 之后
                        
                          <input type="checkbox" id="eq-after2"  class="input-checkbox" />
    					  <label for="eq-after2"></label>
                                          
                        
                    </p>                
                </div>
                <div class="bottom-div0">
                	<div class="bottom-div1">
                    	<p class="document-p  black-text topline">
                        	<b>per month</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每月</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p> -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>
                    </div>
                	<div class="bottom-div2">
                     	<p class="document-p  black-text topline">
                        	<b>per quarter</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每季度</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                    -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>
                    </div>                
                	<div class="bottom-div3">
                     	<p class="document-p  black-text topline">
                        	<b>per 6 months</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每六个月度</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                      -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>  
                    	
                    </div>       
                	<div class="bottom-div4">
                     	<p class="document-p  black-text topline">
                        	<b>per year</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每年</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                     -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p> 
                    </div>                      
                    
                              
                </div>
                
                
                
            </div>
        
        </div>  
        
        <div class="big-container-div">
        	<div class="left-container-div1">
            	<div class="left-fee">
                	<p class="document-p  black-text">
                    	<b>Performance Fee:<br>
                        绩效费</b>
                    </p>
                </div>
                <div class="right-fee">
                	<p class="line-p">
                        <!-- 10 -->

                        <?php 
                        // $cenvertedTime = "2020-04-05 12:10:49";
                        // $userSignUpDate = $userDetails[0]->getDateCreated();
                        // if($userSignUpDate <= "2020-04-05 12:10:49")

                        $cenvertedTime = "2020-04-09 12:00:00";
                        $userSignUpDate = $userDetails[0]->getDateCreated();
                        if($userSignUpDate <= "2020-04-09 12:00:00")

                        {
                            echo "20";
                        }
                        else
                        {
                            echo "40";
                        }
                        ?>

                    </p><p class="next-to-line-p">%</p><!-- Remove the data-->
                </div>
            </div>
            <div class="right-container-div1">
            	<div class="top-div1">
                	<p class="document-p  black-text">
                    	On equity after the period<br>
                       按计算净值周期之后                    
                        
                    </p>                
                </div>
                <div class="bottom-div0">
                	<div class="bottom-div1">
                     	<p class="document-p  black-text topline">
                        	<b>per month</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每月</b></p>
                        <p class="document-p  black-text bottomdata">    
                            
                            10%<!-- Remove the data-->
                        </p>                    
                    </div>
                	<div class="bottom-div2">
                     	<p class="document-p  black-text topline">
                        	<b>per quarter</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每季度</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                      -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>
                    </div>                
                	<div class="bottom-div3">
                     	<p class="document-p  black-text topline">
                        	<b>per 6 months</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每六个月度</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                      -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>
                    </div>       
                	<div class="bottom-div4">
                     	<p class="document-p  black-text topline">
                        	<b>per year</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每年</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                     -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>     
                    </div>                      
                    
                              
                </div>
                
                
                
            </div>
        
        </div>        
        <div class="big-container-div bottom-big-container-div">
        	<div class="left-container-div1">
            	<div class="left-fee">
                	<p class="document-p  black-text">
                    	<b>Other Fees:<br>
                        其他费用</b>
                    </p>
                </div>
                <!-- <div class="right-fee">
                	<p class="line-p">10</p><p class="next-to-line-p">%</p>
                </div> -->

                <div class="right-fee">
                	<p class="line-p"></p><p class="next-to-line-p"></p>
                </div>

            </div>
            <div class="right-container-div1">
            	<div class="top-div1">
                	<p class="document-p  black-text">
                    	On equity before 
                        <input type="checkbox" id="eq-before3"  class="input-checkbox" />
    					<label for="eq-before3"></label>
						 /after 
                        <input type="checkbox" id="eq-after3"  class="input-checkbox" />
    					<label for="eq-after3"></label>
                         
                          the period<br>
                        按计算净值周期之前
                        <input type="checkbox" id="eq-before4"  class="input-checkbox" />
    					<label for="eq-before4"></label>
                        
                        / 之后
                        <input type="checkbox" id="eq-after4"  class="input-checkbox" />
    				   <label for="eq-after4"></label>                        
                        
                    </p>                
                </div>
                <div class="bottom-div0">
                	<div class="bottom-div1">
                     	<p class="document-p  black-text topline">
                        	<b>per month</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每月</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                     -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p> 
                    </div>
                	<div class="bottom-div2">
                     	<p class="document-p  black-text topline">
                        	<b>per quarter</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每季度</b></p>
                        <p class="document-p  black-text bottomdata">    
                            
                            10%<!-- Remove the data-->
                        </p>                      
                    </div>                
                	<div class="bottom-div3">
                     	<p class="document-p  black-text topline">
                        	<b>per 6 months</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每六个月度</b></p>
                        <p class="document-p  black-text bottomdata">    
                            
                            10%<!-- Remove the data-->
                        </p>                    
                    </div>       
                	<div class="bottom-div4">
                     	<p class="document-p  black-text topline">
                        	<b>per year</b></p>
                        <p class="document-p  black-text bottomline">    
                            <b>每年</b></p>
                        <!-- <p class="document-p  black-text bottomdata">    
                            
                            10%
                        </p>                      -->
                        <p class="document-p  black-text bottomdata">    
                            
                        </p>   
                    </div>                      
                    
                              
                </div>
                
                
                
            </div>
        
        </div>          
        <p class="document-p  black-text eng-p">
        *Please add the applicable fee type % amount above to the relevant section.</p>
        <p class="document-p  black-text cn-p">
		*请将相应费用类型的%数额添加到相关部分。</p>
        

        </p>    
        </div>    
        <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 4 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
        <div class="page-break"></div>         
        
        
        
		<div class="page1-div">
        <p class="document-p  black-text eng-p print-margin-top2">
        If allocation method by P/L is used, NO additional deposit is allowed during the entire fund management indicated below:</p>
        <p class="document-p  black-text cn-p">
        如选择 by P/L分配模式, 在以下资金管理周期内将不容许额外的资金加入:</p>        
        
        
        
        <!-- <div class="straight-p">
        	<p class="straight-p1">Fund management period: </p>
            <p class="straight-p2">22/22/2020</p>
            <p class="straight-p3">to</p>
            <p class="straight-p2">22/22/2022</p>
        </div>
		<div class="clear"></div>
        <div class="straight-p">
        	<p class="straight-p1">资金管理周期: </p>
            <p class="straight-p2">22/22/2020</p>
            <p class="straight-p3">至</p>
            <p class="straight-p2">22/22/2022</p>
        </div>           -->

        <div class="straight-p">
        	<p class="straight-p1">Fund management period: </p>
            <p class="straight-p2">  </p>
            <p class="straight-p3">to</p>
            <p class="straight-p2">  </p>
        </div>
		<div class="clear"></div>
        <div class="straight-p">
        	<p class="straight-p1">资金管理周期: </p>
            <p class="straight-p2">  </p>
            <p class="straight-p3">至</p>
            <p class="straight-p2">  </p>
        </div> 

		<div class="clear"></div>
        <p class="small-info"><i>*Period: DD/MM/YYYY to DD/MM/YYYY 周期：日日/月月/年年年年 至 日日/月月/年年年年</i></p>        
        <p class="document-p  black-text eng-p">
        The client accepts that MEX will pay any fees owed to the Money Manager from the client’s MAM under this Agreement, even if the client terminates this Agreement, up until the date of termination, unless to do so would put MEX in breach of law.</p>
        <p class="document-p  black-text cn-p">
        客户接受MEX根据此协议支付资金经理的费用直到授权终止之日，除非这样做违反法律规定。</p>
        <p class="document-p  black-text eng-p">
        * If there is discrepancy between English version and Chinese translation, the English version shall prevail.</p>
        <p class="document-p  black-text cn-p">
        * 如中文翻译与英文条款有差异，则以英文条款为准。</p>        
        
        </p>
       
        </div>
        <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 5 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
        <div class="page-break"></div>  
        <div class="page1-div"> 
 <h2 class=" document-h2  black-text print-margin-top2">CONFIRMATION<br>确认</h2>        
        <p class="document-p  black-text eng-p">
        I confirm that I have read and accept the terms of this MEX Client Power of Attorney & Discretionary Account Terms and that I have also read in full the Terms and Conditions of Client Service Agreement and Product Disclosure Statement and agree to be legally bound by them.</p>
        <p class="document-p  black-text cn-p">
我确定我已经阅读并接受授权委托书及全权委托账户条款，我也阅读了MEX所有的客户服务协议和产品披露声明条款, 同意受到以上条款约束。</p>      
        </p>
        <div class="big-container-div">
          <div class="big1">
        	<div class="big1-div">
            	<p class="document-p  black-text four-box-title">
                    <b>Client’s signature:<br>
                    	客户签名：</b>
                </p>
                <div class="name-p-div">
                    <p class="right-name-p">
                        Name:<br>姓名
                    </p>
                    <!-- <p class="left-line-p name-line-p">
                        Full Name XXXX 
                    </p> -->
                    <p class="left-line-p name-line-p">
                      <!-- <?php //echo $userDetails[0]->getUsername();?> -->
                      <?php echo $userDetails[0]->getFullname();?>
                    </p>
                </div>
                <div class="clear"></div>
                <div class="signature-div3">
                    <p class="right-name-p">
                        Signature:<br>签名
                    </p>
                
                    <!-- <p class="left-line-p">
                        <img src="img/1b.jpg" class="box-signature">
                    </p>                 -->
                    <p class="left-line-p">
                      <img src="uploads/<?php echo $userDoc[0]->getSignature();?>" class="box-signature">
                    </p>  
                </div>
                <p class="right-name-p">
                	Date:<br>日期
                </p>
                <!-- <p class="left-line-p">
                	11/11/2020
                </p>              -->
                <p class="left-line-p">
                    <?php echo date("d-m-Y",strtotime($userDoc[0]->getSignatureTimeline()));?>
                </p>  
            </div>
        	<div class="big1-div big2-div">
            	<p class="document-p  black-text four-box-title">
                    <b>Money Manager’s signature:<br>
                    资金经理签：</b>
                </p>
                <div class="name-p-div">
                    <p class="right-name-p">
                        Name:<br>姓名
                    </p>
                    <p class="left-line-p name-line-p">
                        <img src="img/logo.png" class="box-signature">
                    </p>

                </div>
                <div class="clear"></div>
                <div class="signature-div3">
                    <p class="right-name-p">
                        Signature:<br>签名
                    </p>
                    <!-- <p class="left-line-p">
                        <img src="img/1c.jpg" class="box-signature">
                    </p>   -->
                    <p class="left-line-p">

                    </p>
                </div>              
                <div class="clear"></div>
                <p class="right-name-p">
                	Date:<br>日期
                </p>
                <!-- <p class="left-line-p">
                	11/11/2020
                </p>              -->
                <p class="left-line-p">
                    <?php echo date("d-m-Y",strtotime($userDoc[0]->getSignatureTimeline()));?>
                </p>  
            </div>
          </div>
          <div class="big1">
        	<div class="big1-div bottom-div">
            	<p class="document-p  black-text four-box-title">
                    <b>Witness signature:<br>
                    见证人签名</b>
                </p>
                <div class="name-p-div">
                    <p class="right-name-p">
                        Name:<br>姓名
                    </p>
                    <!-- <p class="left-line-p name-line-p">
                        Full Name XXXX XXXXXXXXXXXXXX XXXXX 
                    </p> -->
                    <p class="left-line-p name-line-p">

                    </p>
                </div>
                <div class="clear"></div>
                <div class="signature-div3">
                    <p class="right-name-p">
                        Signature:<br>签名
                    </p>
                    <!-- <p class="left-line-p">
                        <img src="img/1c.jpg" class="box-signature">
                    </p>   -->
                    <p class="left-line-p">

                    </p>  
                </div>              
                <div class="clear"></div>
                <p class="right-name-p">
                	Date:<br>日期
                </p>
                <!-- <p class="left-line-p">
                	11/11/2020
                </p>              -->
                <p class="left-line-p">

                </p>  
            </div>            
         	<div class="big1-div big2-div bottom-div">
            	<p class="document-p  black-text four-box-title">
                    <b>Witness signature:<br>
                    见证人签名</b>
                </p>
                <div class="name-p-div">
                    <p class="right-name-p">
                        Name:<br>姓名
                    </p>
                    <!-- <p class="left-line-p name-line-p">
                        Full Name XXX
                    </p> -->
                    <p class="left-line-p name-line-p">

                    </p>
                </div>
                <div class="clear"></div>
                <div class="signature-div3">
                    <p class="right-name-p">
                        Signature:<br>签名
                    </p>
                    <!-- <p class="left-line-p">
                        <img src="img/1b.jpg" class="box-signature">
                    </p>      -->
                    <p class="left-line-p">

                    </p>
                </div>           
                <div class="clear"></div>
                <p class="right-name-p">
                	Date:<br>日期
                </p>
                <!-- <p class="left-line-p">
                	11/11/2020
                </p>              -->
                <p class="left-line-p">

                </p> 
            </div>              
          </div> 
        </div>     
        </div>   
       <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 6 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
       <div class="page-break"></div>         
        
        <div class="page1-div">        
        <h2 class=" document-h2  black-text print-margin-top2">Appendix 1<br>附录1</h2>
        <p class="document-p  black-text eng-p">
        	Example of Hight-Water Mark in Practice calculation:
        </p>        
        <p class="document-p  black-text cn-p">
        	高水位计算方法案例:
        </p>
        <p class="document-p  black-text cn-p">
        	Performance Fee: 30% 绩效利润分成: 30%
        </p>
        <p class="document-p  black-text cn-p">
        	Period: Monthly 1st -31st of month. 周期: 每月1-31号
        </p>
        
        
        <h2 class=" document-h2  black-text">Month 1:<br>第一个月</h2>        
        	<p class="document-p  black-text eng-p left-p-ma float-left">        
            Starting Equity: $1,000</p>
        	<p class="document-p  black-text eng-p float-left">        
            Ending Equity: $2,000</p>    
            <div class="clear"></div>        
            <p class="document-p  black-text cn-p  left-p-ma float-left">
            开始净值:$1,000</p>
            <p class="document-p  black-text cn-p float-left">
            结束净值: $2,000</p>    
            <div class="clear"></div>        
            <p class="document-p  black-text cn-p">
            Period P/L, 周期P/L = $2,000 - $1,000 = $1,000</p>
            <p class="document-p  black-text cn-p">
            Accumulated P/L, 累计 P/L = $1,000</p>
            <p class="document-p  black-text eng-p"> 
            Performance Fee (PF)= Accumulated P/L x 30% = $1,000 x 30%= $300</p>
            <p class="document-p  black-text cn-p">
            绩效利润分成 = （$2,000-$1,000）x30% = $300</p>
            <p class="document-p  black-text eng-p">
            Ending Equity after PF = $2,000 - $300 = $1,700</p>
            <p class="document-p  black-text cn-p">
            扣绩效利润分成后的净值 = $2,000 - $300 = $1,700</p>
            <p class="document-p  black-text eng-p">    
            Money Manager will receive $300 performance fee from Month 1.</p>
            <p class="document-p  black-text cn-p">
            第一个月资金经理将会收到 $300绩效分成.</p>
            <p class="document-p  black-text eng-p">
            PS: Accumulated P/L will reset to zero once performance fee is distributed.</p>
            <p class="document-p  black-text cn-p">
            备注: 绩效利润分成一旦分发之后，累计P/L将会归零.</p>        
        </p>
        <h2 class=" document-h2  black-text">Month 2:<br>第二个月</h2>          
        	<p class="document-p  black-text eng-p left-p-ma float-left">        
            Starting Equity: $1,700</p>
        	<p class="document-p  black-text eng-p float-left">        
            Ending Equity: $1,000</p>    
            <div class="clear"></div>        
            <p class="document-p  black-text cn-p  left-p-ma float-left">
            开始净值:$1,700</p>
            <p class="document-p  black-text cn-p float-left">
            结束净值: $1,000</p>    
            <div class="clear"></div>     

            <p class="document-p  black-text cn-p">
            Period P/L, 周期P/L = $1,000 - $1,700 = - $700</p>
            <p class="document-p  black-text cn-p">
            Accumulated P/L, 累计 P/L = -$700</p>
            <p class="document-p  black-text eng-p">
            Money Manager will not receive any performance fee from Month 2 due to Accumulated P/L is negative.</p>
            <p class="document-p  black-text cn-p">
            因为绩效利润分成为负数, 第二个月资金经理将不会收到绩效分成.</p>        	
        </p>   
        
       </div> 
       <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 7 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
        <div class="page-break"></div>   

        
        <div class="page1-div">     
        <h2 class=" document-h2  black-text print-margin-top2">Month 3:<br>第三个月</h2> 
        <p class="document-p  black-text  eng-p three-p1">
        	Starting Equity: $1000
        </p>
        <p class=" document-p  black-text eng-p three-p1 mid-three-p1">
        	Net Deposit during Month 3: $5,000    
        </p>
        <p class="document-p  black-text eng-p three-p1">
        	Ending Equity: $6500
        </p>  
        <div class="clear"></div>
        <p class="document-p  black-text  cn-p three-p1">
			开始净值:$1,000 
        </p>
        <p class=" document-p  black-text cn-p three-p1 mid-three-p1">
			第三个月净入金: $5,000        
        </p>
        <p class="document-p  black-text cn-p three-p1">
            结束净值: $6,500
        </p>        
        
        
        
        <div class="clear"></div>                       
        <p class="document-p  black-text eng-p">
            Adjusted Starting Equity: $1,000 + $5,000 = $6,000</p>
            <p class="document-p  black-text cn-p">
            调整后的开始净值:$1,000 + $5,000 = $6,000</p>
            <p class="document-p  black-text cn-p">
            Period P/L, 周期P/L = $6,500 - $6,000 = $500</p>
            <p class="document-p  black-text cn-p">
            Accumulated P/L, 累计 P/L = -$700 + 500 = - $200</p>
            <p class="document-p  black-text eng-p">
            Even through there is profit for month 3, Money Manager still not receiving any performance fee from Month 3 due to Accumulated P/L is still negative.</p>
            <p class="document-p  black-text cn-p">
            虽然第三个月有盈利，但补上二月的亏损，累计P/L还是为负数, 第三个月资金经理将不会收到绩效分成.
            </p>      	
        </p>         
        <h2 class=" document-h2  black-text">Month 4:<br>第四个月</h2> 
        <p class="document-p  black-text  eng-p three-p1">
        	Starting Equity: $6,500
        </p>
        <p class=" document-p  black-text eng-p three-p1 mid-three-p1">
        	Net Deposit during Month 3: - $4,000    
        </p>
        <p class="document-p  black-text eng-p three-p1">
        	Ending Equity: $6,000
        </p>  
        <div class="clear"></div>
        <p class="document-p  black-text  cn-p three-p1">
			开始净值:$6,500 
        </p>
        <p class=" document-p  black-text cn-p three-p1 mid-three-p1">
			第三个月净入金: - $4,000        
        </p>
        <p class="document-p  black-text cn-p three-p1">
            结束净值: $6,000
        </p>        
                  
        <div class="clear"></div>                       
        <p class="document-p  black-text eng-p">
            Adjusted Starting Equity: $6,500 - $4,000 = $2,500</p>
            <p class="document-p  black-text cn-p">
            调整后的开始净值: $6,500 - $4,000 = $2,500</p>
            <p class="document-p  black-text cn-p">
            Period P/L, 周期P/L = $6,000 - $2,500 = $3,500</p>
            <p class="document-p  black-text cn-p">
            Accumulated P/L, 累计 P/L = -$700 + 3,500 = 2,800</p>
            <p class="document-p  black-text eng-p">
            Ending Equity after PF = $6,000 - $840 = $5,160<br>
Money Manager will receive $840 performance fee from Month 4 and Accumulated P/L reset to Zero</p>
			<p class="document-p  black-text cn-p">
            补上之前的亏损后，总绩效利润分成为正数, 第四个月资金经理将会收到绩效分成 $840, 累计P/L归零.      	
        </p>         
       </div>
     
       <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Limited Power of Attorney</td>
                        <td><p class="bottom-left-text">Client Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td><p class="bottom-left-text">Money Manager Initial </p><p class="bottom-left-border">&nbsp;</p></td>
                        <td class="right-page-number">Page 8 of 8</td>
                	</tr>
                </tbody>
            </table>
        </div>                   
       </div>

    <?php
    }
    ?>

    </div> 
<style>
/* Base for label styling */
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
[type="checkbox"]:not(:checked) + label,
[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 1.35em;
  cursor: pointer;
}

/* checkbox aspect */
[type="checkbox"]:not(:checked) + label:before,
[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left: 0;
  top: 3px;
  width: 0.8em;
  height: 0.8em;
  border: 1px solid black;
  background: #fff;


}
/* checked mark aspect */
[type="checkbox"]:not(:checked) + label:after,
[type="checkbox"]:checked + label:after {
  content: '\2713\0020';
  position: absolute;
  top: .3em;
  left: .1em;
  font-size: 1em;
  line-height: 0.8;
  color: black;
  transition: all .2s;
  font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
}
/* checked mark aspect changes */
[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
/* disabled checkbox */
[type="checkbox"]:disabled:not(:checked) + label:before,
[type="checkbox"]:disabled:checked + label:before {
  box-shadow: none;
  border-color: black;
  background-color: white;
}
[type="checkbox"]:disabled:checked + label:after {
  color: black;
}
[type="checkbox"]:disabled + label {
  color: black;
}
/* accessibility */
[type="checkbox"]:checked:focus + label:before,
[type="checkbox"]:not(:checked):focus + label:before {
  border: 1px solid black;
}

/* hover style just for information */
label:hover:before {
  border: 1px solid black !important;
}


</style>
<?php include 'js.php'; ?>
<img src="img/print.png" class="print-png opacity-hover"  onclick="window.print()" alt="Print" title="Print">
</body>
</html>
