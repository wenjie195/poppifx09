<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://victory5.co/brokerLink.php" />
    <link rel="canonical" href="https://victory5.co/brokerLink.php" />
    <meta property="og:title" content="Broker Link  | Victory 5" />
    <title>Broker Link  | Victory 5</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height text-center" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/share.png" class="middle-title-icon" alt="<?php echo _BROKER_LINK ?>" title="<?php echo _BROKER_LINK ?>">
    </div>
	<div class="width100 overflow">
		<h1 class="pop-h1"><?php echo _BROKER_LINK ?></h1>
    </div>
    
    <div class="spacing-div"></div>

    <div class="width100 overflow text-center">

    <h3 class="invite-h3">
        <a href="https://myaccount.infinox.bs/register/index?invitationCode=XZN9C6D1&language=en" class="invitation-link-a black-link-underline" target="_blank">
            https://myaccount.infinox.bs/register/index?invitationCode=XZN9C6D1&language=en
        </a>
    </h3>

    </div>

    <div class="clear"></div>

</div>
<?php include 'js.php'; ?>
</body>
</html>
