<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $dateCreated = '2020-05-04';
// $dateEnd = '2020-05-04';
$dateCreated = rewrite($_POST['dateStart']);
$dateEnd = rewrite($_POST['dateEnd']);

if ($dateCreated) {
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}else {
  $dateCreated = "01/01/1970";
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}

if ($dateEnd) {
  $dateEndNew = str_replace("/","-",$dateEnd);
  $dateEndMin = date('Y-m-d',strtotime($dateEndNew));
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}else {
  $dateEndMin = date('Y-m-d');
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}

$dailyBonusDetailsDaily = getDailyBonus($conn, "WHERE uid = ? and date_created >= ? and date_created < ? and display = 1", array("uid,date_created,date_created"), array($uid,$dateCreatedMin,$dateCreatedMax), "sss");

if ($dailyBonusDetailsDaily) {
  for ($cnt=0; $cnt <count($dailyBonusDetailsDaily) ; $cnt++) {
    $username = $dailyBonusDetailsDaily[$cnt]->getUsername();
    $from = $dailyBonusDetailsDaily[$cnt]->getFromWho();
    $bonus = $dailyBonusDetailsDaily[$cnt]->getBonus();
    $display = $dailyBonusDetailsDaily[$cnt]->getDisplay();
    $date = date('d/m/Y',strtotime($dailyBonusDetailsDaily[$cnt]->getDateCreated()));
    $time = date('h:i a',strtotime($dailyBonusDetailsDaily[$cnt]->getDateCreated()));

    $totalPayout[] = array("date" => $dateCreatedMax, "username" => $username,
                          "from" => $from, "bonus" => $bonus, "dateCreated" => $date, "timeCreated" => $time);
  }
}


echo json_encode($totalPayout);
 ?>
