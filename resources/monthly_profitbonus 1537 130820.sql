-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2020 at 09:37 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_poppifx`
--

-- --------------------------------------------------------

--
-- Table structure for table `monthly_profitbonus`
--

CREATE TABLE `monthly_profitbonus` (
  `id` bigint(20) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `monthly_profitbonus`
--

INSERT INTO `monthly_profitbonus` (`id`, `level`, `amount`, `date_created`, `date_updated`) VALUES
(1, '1', '5', '2020-03-13 01:49:02', '2020-08-13 07:36:12'),
(2, '2', '4', '2020-03-13 01:49:02', '2020-08-13 07:36:15'),
(3, '3', '3', '2020-03-13 01:49:17', '2020-08-13 07:36:17'),
(4, '4', '2', '2020-03-13 01:49:17', '2020-08-13 07:36:20'),
(5, '5', '1', '2020-03-13 01:49:28', '2020-08-13 07:36:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `monthly_profitbonus`
--
ALTER TABLE `monthly_profitbonus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `monthly_profitbonus`
--
ALTER TABLE `monthly_profitbonus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
