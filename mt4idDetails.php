<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://victory5.co/userBankDetails.php" />
    <link rel="canonical" href="https://victory5.co/userBankDetails.php" />
    <meta property="og:title" content="Bank Details  | Victory 5" />
    <title>Bank Details  | Victory 5</title>
    <script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>

	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'userHeader.php'; ?>


<div class="width100 same-padding menu-distance darkbg min-height">

    <form action="utilities/addMt4idFunction.php" method="POST">
    <div class="width100 overflow text-center">
    	<img src="img/add-user3.png" class="middle-title-icon" alt="<?php echo _USERHEADER_ADD_NEW ?>" title="<?php echo _USERHEADER_ADD_NEW ?>">
    </div>
    <div class="width100 overflow">
    	<h1 class="pop-h1 text-center"><?php echo _USERHEADER_ADD_NEW ?></h1>
    </div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo "MT4ID" ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo "MT4ID" ?>" id="mt4idInput" name="add_mt4id" required>
            <a id="alert"></a>
        </div>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button id="submitBtn" class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>
<?php include 'js.php'; ?>
<?php if(isset($_GET['type']) && isset($_SESSION['messageType']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Add New MT4ID.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Error To Add MT4ID.";
        }
        if($_GET['type'] == 11)
        {
            $messageType = "Successfully Upload Excel.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>
</body>
<script>
  $("#mt4idInput").on("keyup",function(){
    var mt4idVal = $(this).val();

    if (mt4idVal != '') {
      $.ajax({
        url: 'utilities/mp4idChecker.php',
        data: {mt4idVal:mt4idVal},
        type: 'post',
        dataType: 'json',
        success:function(data){
          var success = data[0]['success'];
          if (success == 1) {
          $("#submitBtn").prop('disabled',false);
            $("#alert").text("Available MT4ID");
            $("#alert").css({
                "color":"green",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "left",
            });
          }else if(success == 2){
            $("#alert").text("Existing MT4ID");
            $("#submitBtn").prop('disabled',true);
            $("#alert").css({
                "color":"red",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "left",
            });
          }
        }
      });
    }else {
    $("#submitBtn").prop('disabled',true);
      $("#alert").text("");
    }
  });
</script>
</html>
