<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$companyBalance = getCompanyBalance($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminCompanyBalance.php" />
    <link rel="canonical" href="https://victory5.co/adminCompanyBalance.php" />
    <meta property="og:title" content="View Company Balance  | Victory 5" />
    <title>View Company Balance  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
.blue-btn {
    border: 0;
    background-color: #003c80;
    font-size: 15px;
    padding: 5px;
    border-radius: 3px;
}
.blue-btn:hover{
  cursor:pointer;
  background-color:#002b5d;
  transition:.15s ease-in-out;
}

td.tdh{
    text-align: center;
  }
  th.thh{
    text-align: center;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
	<h1 class="pop-h1 text-center"><?php echo _ADMINHEADER_COMPANY_BALANCE ?></h1>
    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_COMPANY_TOTAL ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_TOTAL_LOT_SIZE ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_LAST_UPDATED ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($companyBalance)
                    {
                        for($cntA = 0;$cntA < count($companyBalance) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td class="tdh"><?php echo ($cntA+1)?></td>
                                <td class="tdh"><?php echo "$ ".number_format($companyBalance[$cntA]->getCtb(),2);?></td>
                                <td class="tdh"><?php echo number_format($companyBalance[$cntA]->getTls());?></td>
                                <td class="tdh"><?php echo date('d/m/Y',strtotime($companyBalance[$cntA]->getDateUpdated())); ?></td>
                                <td class="tdh"><form class="" action="editCompanyBalance.php" method="post">
                                  <input type="hidden" name="id" value="<?php echo $companyBalance[$cntA]->getId() ?>">
                                  <a class="blue-link"><button class="blue-btn clean" type="submit" name="editBalance"><?php echo _ADMINVIEWBALANCE_EDIT ?></button> </a>
                                </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
