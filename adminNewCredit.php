<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
    $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if(in_array($_FILES["file"]["type"],$allowedFileType))
    {
        $targetPath = 'uploadExcel/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);

            foreach ($Reader as $Row)
            {

                $userName = "";
                if(isset($Row[0]))
                {
                    $userName = mysqli_real_escape_string($conn,$Row[0]);
                }

                $userID = "";
                if(isset($Row[0]))
                {
                    $userID = mysqli_real_escape_string($conn,$Row[1]);
                }
                // $userName = "";
                // if(isset($Row[0]))
                // {
                //     $userName = mysqli_real_escape_string($conn,$Row[1]);
                // }
                $point = "";
                if(isset($Row[0]))
                {
                    $point = mysqli_real_escape_string($conn,$Row[2]);
                }

                // if (!empty($userName) || !empty($point) || !empty($userID))
                if (!empty($point) || !empty($userID))
                {
                    $query = "UPDATE `equityplrawdata` SET `balance`='".$point."' WHERE  mp_id = '".$userID."' ";
                    $result = mysqli_query($conn, $query);
                    if (! empty($result))
                    {
                        // echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminAddOnCredit.php'</script>";

                        // $query = "INSERT INTO equityplbackupdata (mp_id,balance) VALUES ('".$userID."','".$point."') ";
                        $query = "INSERT INTO equityplbackupdata (name,mp_id,balance) VALUES ('".$userName."','".$userID."','".$point."') ";
                        $result = mysqli_query($conn, $query);
                        if (! empty($result))
                        {
                            // echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminNewCredit.php'</script>";
                            echo "<script>alert('Excel Uploaded !');window.location='../testing/adminNewCredit.php?type=11'</script>";
                        }
                        else
                        {
                            // echo "<script>alert('Fail To Upload !');window.location='../poppifx/adminNewCredit.php'</script>";
                            echo "<script>alert('Fail To Upload !');window.location='../testing/adminNewCredit.php?type=12'</script>";
                        }


                    }
                    else
                    {
                        // echo "<script>alert('Fail To Upload !');window.location='../poppifx/adminNewCredit.php'</script>";
                        echo "<script>alert('Fail To Upload !');window.location='../testing/adminNewCredit.php?type=13'</script>";
                    }
                }
                else
                {
                    // echo "dunno";
                    // echo "<script>alert('ERROR');window.location='../poppifx/adminNewCredit.php'</script>";
                    echo "<script>alert('ERROR');window.location='../testing/adminNewCredit.php?type=14'</script>";
                }

            }

        }
    }
    else
    {
        // echo "<script>alert('ERROR 2');window.location='../poppifx/adminNewCredit.php'</script>";
        echo "<script>alert('ERROR 2');window.location='../testing/adminNewCredit.php?type=15'</script>";
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminNewCredit.php" />
    <link rel="canonical" href="https://victory5.co/adminNewCredit.php" />
    <meta property="og:title" content="Admin New Credit  | Victory 5" />
    <title>Admin New Credit  | Victory 5</title>
   
   <link rel="stylesheet" href="css/font-awesome.min.css">
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center">

    <!-- <div class="width100 text-center overflow margin-bottom50">
        <button class="clean blue-button one-button-width">
            <a href="addOnBonus.php" class="red-link">Add On Point</a>
        </button>
    </div>  -->
	<h1 class="pop-h1"><?php echo _ADMINDASHBOARD_UPLOAD_EQUITY_PL ?></h1>
    <div class="outer-container">
        <form action="" method="post" name="frmExcelImport" id="frmExcelImport2" enctype="multipart/form-data">
            <label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
            <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
            <button type="submit" id="submit" name="import"  class="clean blue-button one-button-width"><?php echo _JS_SUBMIT ?></button>
            <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
        </form>
         <div class="clear"></div>
         <div class="width100 text-center">
              <button type="submit" id="Monthly" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top0">
                <i id="loadingAnimate3" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName3"><?php echo _ADMINADDON_MONTHLY_BONUS ?></a>

              </button>
         </div>
    </div>

    <input type="hidden" name="getValue2" value="<?php echo $valueGet ?>">



    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
