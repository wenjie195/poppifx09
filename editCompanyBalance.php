<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$id = rewrite($_POST['id']);
$companyBalanceDet = getCompanyBalance($conn, "WHERE id = ?",array("id"),array($id), "s");
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/editCompanyBalance.php" />
    <link rel="canonical" href="https://victory5.co/editCompanyBalance.php" />
    <meta property="og:title" content="Edit Company Balance  | Victory 5" />
    <title>Edit Company Balance  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
    <div class="width100 overflow text-center">
    	<img src="img/company.png" class="middle-title-icon" alt="<?php echo _ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE ?>" title="<?php echo _ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE ?>">
    	
    </div>
    <div class="width100 overflow text-center">
    	<h1 class="pop-h1"><?php echo _ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE ?></h1>
	</div>    
    <form action="utilities/editCompanyBalanceFunction.php" method="POST">

	<p class="width100 signup-p">
	</p>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _ADMINVIEWBALANCE_COMPANY_TOTAL ?></p>
            <input class="clean pop-input" type="text" placeholder="CTB" value="<?php echo $companyBalanceDet[0]->getCtb() ?>" name="ctb" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _ADMINVIEWBALANCE_TOTAL_LOT_SIZE ?></p>
            <input class="clean pop-input" type="text" placeholder="TLS" value="<?php echo $companyBalanceDet[0]->getTls() ?>" name="tls" required>
        </div>
        <div class="clear"></div>

		<div class="width100 text-center">
      <input type="hidden" name="id" value="<?php echo $companyBalanceDet[0]->getId() ?>">
        	<button class="clean blue-button one-button-width" name="editBalance"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>
<?php include 'js.php'; ?>
</body>
</html>
