<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_poppifx");//for localhost
    // $conn = new mysqli("localhost", "ichibang_vicuser", "Y7XsZNmswdJP","ichibang_victoryfive"); //for ichibang temporarily
    // $conn = new mysqli("localhost", "poppifxu_user", "Ssxmn8ixrttc","poppifxu_live"); //for bigdomain access actual
    // $conn = new mysqli("localhost", "poppifxu_tester", "e0jG1bI12ITL","poppifxu_testing"); //for bigdomain access testing
    // $conn = new mysqli("localhost", "vidatech_popuser", "bdYcIvKqyR0c","vidatech_poppifx"); //for testing vidatech
    // $conn = new mysqli("localhost", "poppifx_popuser", "rE2mxy2RPMc1","poppifx_live"); //for godaddy server
    // $conn = new mysqli("localhost", "poppifx_testuser", "j11pPFClALMA","poppifx_testing"); //for godaddy test server
    // $conn = new mysqli("localhost", "bossinte_popuser", "NctbnQAaMw7^","bossinte_poppifx"); //for testing boss
    // $conn = new mysqli("localhost", "i7fwpacdxx76", "u2GYoP?~ha7o","poppifx_live"); //for godaddy cpanel login
    // $conn = new mysqli("localhost", "pingolag_vicuser", "FRjvILdAjZ4w","pingolag_victory"); // pingola server
    // $conn = new mysqli("localhost", "victoryc_user", "py8gVEykAOMR","victoryc_live"); // victory5 server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
