<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$mpIdData = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://victory5.co/profile.php" />
    <link rel="canonical" href="https://victory5.co/profile.php" />
    <meta property="og:title" content="Profile  | Victory 5" />
    <title>Profile  | Victory 5</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height profile-big-div user-dash user-dash2" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/profile-icon.png" class="middle-title-icon" alt="<?php echo _USERHEADER_BANK_DETAILS ?>" title="<?php echo _USERHEADER_BANK_DETAILS ?>">
    </div>
    <div class="width100 overflow">
		<h1 class="text-center pop-h1"><?php echo _USERHEADER_PROFILE ?></h1>
    </div>
    <div class="top-profile-div">
      	<div class="profile-h3 text-center white-div-css">
          <img src="img/user.png" class="white-div-img" alt="<?php echo _JS_USERNAME ?>" title="<?php echo _JS_USERNAME ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_USERNAME ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getUsername();?></p>
          </div>
        </div>
        <div class="profile-h3 text-center white-div-css mid-profile-h3 second-profile-h32">
          <img src="img/mt4.png" class="white-div-img" alt="MT4" title="MT4">
          <div class="inner-white-div">
            <p class="white-div-p">MT4</p>
            <p class="white-div-p white-div-amount"><?php if ($mpIdData) {
              for ($i=0; $i <count($mpIdData) ; $i++) {
                echo $mpIdData[$i]->getMpID()."<br>";
              }
            }else {
              echo "No MP4 Found";
            } ?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
      	<div class="profile-h3 text-center white-div-css">
          <img src="img/ic.png" class="white-div-img" alt="<?php echo _INDEX_IC_NO ?>" title="<?php echo _INDEX_IC_NO ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_IC_NO ?></p>
            <p class="white-div-p white-div-amount"><?php echo _INDEX_IC_NO ?></p>
          </div>
        </div>
		<div class="tempo-three-clear"></div>
      	<div class="profile-h3 text-center white-div-css second-profile-h32">
          <img src="img/fullname.png" class="white-div-img" alt="<?php echo _JS_FULLNAME ?>" title="<?php echo _JS_FULLNAME ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_FULLNAME ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getLastname();?> <?php echo $userData->getFirstname();?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
        <div class="profile-h3 text-center white-div-css mid-profile-h3">
          <img src="img/first-name.png" class="white-div-img" alt="<?php echo _JS_FIRSTNAME ?>" title="<?php echo _JS_FIRSTNAME ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_FIRSTNAME ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getFirstname();?></p>
          </div>
        </div>
      	<div class="profile-h3 text-center white-div-css second-profile-h32">
          <img src="img/last-name.png" class="white-div-img" alt="<?php echo _JS_LASTNAME ?>" title="<?php echo _JS_LASTNAME ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_LASTNAME ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getLastname();?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
        <div class="tempo-three-clear"></div>
      	<div class="profile-h3 text-center white-div-css">
          <img src="img/email.png" class="white-div-img" alt="<?php echo _JS_EMAIL ?>" title="<?php echo _JS_EMAIL ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_EMAIL ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getEmail();?></p>
          </div>
        </div>
        <div class="profile-h3 text-center white-div-css mid-profile-h3 second-profile-h32">
          <img src="img/birthday.png" class="white-div-img" alt="<?php echo _INDEX_DOB ?>" title="<?php echo _INDEX_DOB ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_DOB ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getBirthDate();?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
      	<div class="profile-h3 text-center white-div-css">
          <img src="img/phone.png" class="white-div-img" alt="<?php echo _INDEX_MOBILE_NO ?>" title="<?php echo _INDEX_MOBILE_NO ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_MOBILE_NO ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getPhoneNo();?></p>
          </div>
        </div>
		<div class="tempo-three-clear"></div>
      	<div class="profile-h3 text-center white-div-css second-profile-h32">
          <img src="img/address1.png" class="white-div-img" alt="<?php echo _INDEX_ADDRESS1 ?>" title="<?php echo _INDEX_ADDRESS1 ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_ADDRESS1 ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getAddress();?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
        <div class="profile-h3 text-center white-div-css mid-profile-h3">
          <img src="img/address2.png" class="white-div-img" alt="<?php echo _INDEX_ADDRESS2 ?>" title="<?php echo _INDEX_ADDRESS2 ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_ADDRESS2 ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getAddressB();?></p>
          </div>
        </div>
      	<div class="profile-h3 text-center white-div-css second-profile-h32">
          <img src="img/post.png" class="white-div-img" alt="<?php echo _INDEX_ZIPCODE ?>" title="<?php echo _INDEX_ZIPCODE ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _INDEX_ZIPCODE ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getZipcode();?></p>
          </div>
        </div>
        <div class="mobile-two-clear"></div>
		<div class="tempo-three-clear"></div>
      	<div class="profile-h3 text-center white-div-css">
          <img src="img/flag.png" class="white-div-img" alt="<?php echo _JS_COUNTRY ?>" title="<?php echo _JS_COUNTRY ?>">
          <div class="inner-white-div">
            <p class="white-div-p"><?php echo _JS_COUNTRY ?></p>
            <p class="white-div-p white-div-amount"><?php echo $userData->getCountry();?></p>
          </div>
        </div>
		<div class="clear"></div>
		<div class="width100 text-center margin-top-bottom30">
        	<a href="editProfile.php" class="clean blue-button one-button-width pill-button margin-auto" ><?php echo _JS_EDIT_PROFILE ?></a>
        </div>

	</div>

    <!-- <div class="width100 profile-img-div">
    	<img src="img/profile2.png" class="profile-img" alt="<?php //echo _USERHEADER_PROFILE ?>" title="<?php //echo _USERHEADER_PROFILE ?>">
    </div> -->
</div>

<?php include 'js.php'; ?>
<!-- <?php //include 'rankIdentifySolo.php' ?> -->

</body>
</html>
