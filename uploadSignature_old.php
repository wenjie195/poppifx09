<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://poppifx4u.com/uploadSignature.php" />
    <link rel="canonical" href="https://poppifx4u.com/uploadSignature.php" />-->
    <meta property="og:title" content="Upload Signature  | Victory 5" />
    <title>Upload Signature  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

  <div class="width100 same-padding menu-distance darkbg min-height big-black-text">
  	<div class="document-div">
  		<img src="img/multibank-white.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">
        <h2 class="white-text document-h2">Power of Attorney & Discretionary Account Terms</h2>
        <p class="document-p white-text">
        	Name of Attorney:<br><br>
            Address of Attorney:<br><br>
        	The below under signed here in after referred to as the Customer, hereby authorises the Attorney‐ in-fact as its agent Attorney-in-fact to buy, sell (including short sales), exchange, assign or transfer and trade for it at any price (my) (our) attorney deems fair in contracts as defined in the Customer Agreement of MultiBank FX International Corporation (“MEX”). The Customer hereby indemnifies MEX and its directors, officers, employees and agents from and against all liability arising directly or indirectly, from following Attorney‐ in‐ fact's instruction and will pay MEX promptly, on demand, any losses arising from such trades and any debit balance resulting there from.<br><br>
            In all such purchases, sales or trades, MEX is authorised to follow Attorney-in-fact's instructions in every respect and Attorney-in-fact is authorised to act for the Customer with the same force and effect as Customer might do with respect to such purchase, sales or trades and all things necessary or incidental to the furtherance of such purchases, sales or trades. MEX is directed to make available to Attorney-in-fact a copy of all statements that MEX makes available to the Customer concerning Customer's account, including, but not limited to, monthly statements, confirmations and purchase and sale agreements. The Customer hereby ratifies and confirms any and all transactions with MEX heretofore and hereafter made by the Attorney‐ in‐ fact for Customer's account.<br><br>
The Attorney‐ in‐ fact is not authorised to withdraw from Customer's account any monies, securities or any property either in the Customer's name or otherwise unless such withdrawal or payment is specifically authorised in writing by the Customer.<br><br>
This Power of Attorney shall remain in full force and effect until MEX receives from the Customer written notification of Customer's revocation thereof.
<br><br>
The Customer understands that MEX is in no way responsible for any loss to the Customer occasioned by actions of the Attorney-in-fact and that MEX does not, by implication or otherwise, endorse the operation or methods of the Attorney- in-fact.<br><br>
Fee Acknowledgement<br><br>
The Client agrees that MEX has been authorised to compensate The Attorney for the Asset Managers services.<br><br>
MEX accepts no responsibility for ratifying the performance or management fees instructed by the Attorney in relation to activity on this account, and this information is provided below to MEX purely for informational purposes but MEX accepts no responsibility as calculation agent to verify that the fees instructed by the Attorney are correct and is exempt from any liability from acting upon such an instruction.<br><br>
Management fee: 0% Per Month<br><br>
Incentive fee: 40% Per Month<br><br>
Note: It is the assumed to be the responsibility of the Asset Manager or Trading Agent to request any fee payment. MultiBank FX International Corporation is not responsible for any losses which may result from a failure of them to do so or from any incorrect fee amount notified to MEX by this party.<br><br>
*Please add the applicable fee type % amount above to the relevant section and nominatefrequency of payment by ticking the box.
        </p>
    </div>
    <!-- Upload  -->
    <h1 class="upload-h1 white-text text-center"><?php echo _UPLOAD_SIGNATURE_MAA ?></h1>
      <form action="utilities/uploadSignatureFunction.php" method="POST" enctype="multipart/form-data" id="file-upload-form" class="uploader">
        <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
        <input id="file-upload" type="file" name="file" accept="image/*" />
          <label for="file-upload" id="file-drag">
            <img id="file-image" src="#" alt="<?php echo _UPLOAD_PREVIEW ?>" title="<?php echo _UPLOAD_PREVIEW ?>" class="hidden">
            <div id="start">
              <img src="img/upload.png" alt="<?php echo _UPLOAD ?>" title="<?php echo _UPLOAD ?>" class="upload-icon">
              <div><?php echo _UPLOAD_SELECT_DRAG ?></div>
              <div id="notimage" class="hidden"><?php echo _UPLOAD_PLS_SELECT_IMG ?></div>
              <span id="file-upload-btn" class="btn btn-primary"><?php echo _UPLOAD_SELECT_A_FILE ?></span>
            </div>
            <div id="response" class="hidden">
              <div id="messages"></div>
              <progress class="progress" id="file-progress" value="0">
              <span>0</span>%
              </progress>
            </div>
          </label>
          <div class="width100 text-center">
            <button class="clean blue-button one-button-width"  type="submit" name="submit"><?php echo _UPLOAD ?></button>
          </div>
      </form>
    <div class="clear"></div>
  </div>

<?php include 'js.php'; ?>
</body>
</html>
