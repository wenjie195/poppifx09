<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn);
// $userDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
   
    <meta property="og:url" content="https://victory5.co/adminViewIDDoc.php" />
    <link rel="canonical" href="https://victory5.co/adminViewIDDoc.php" />
    <meta property="og:title" content="ID Document  | Victory 5" />
    <title>ID Document  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<!-- <?php //include 'adminHeader.php'; ?> -->
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text print-big-div darkbg">
<h1 class="doc-h1 opacity-hover white-text" onclick="goBack()"><img src="img/back3.png" class="back-png" alt="<?php echo _MULTIBANK_GO_BACK ?>" title="<?php echo _MULTIBANK_GO_BACK ?>" > <?php echo _MULTIBANK_ID_DOCUMENT ?></h1>
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");

    $userDoc = getStatus($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <div class="top-profile-div overflow">
		
            <h3 class="profile-h3 ow-black-text"><p class="input-top-text"><?php echo _JS_USERNAME ?>:</p> <b><?php echo $userDetails[0]->getUsername();?></b></h3>
            <h3 class="profile-h3 mid-profile-h3 ow-black-text"><p class="input-top-text"><?php echo _INDEX_IC_NO ?>:</p> <b><?php echo $userDetails[0]->getIcno();?></b></h3>
            <h3 class="profile-h3 ow-black-text"><p class="input-top-text">MT4 ID:</p> <b><?php echo $userDetails[0]->getMpId();?></b></h3>

            <div class="profile-h3 ow-black-text">
            	<h3 class="ow-black-text"><p class="input-top-text"><?php echo _MULTIBANK_IC_FRONT ?>:</p></h3>
                <img src="uploads/<?php echo $userDoc[0]->getICfront();?>" class="id-img" >
            </div>
			
            <div class="profile-h3 mid-profile-h3 ow-black-text">
            	<h3 class="ow-black-text"><p class="input-top-text"><?php echo _MULTIBANK_IC_BACK ?>:</p></h3>
                <img src="uploads/<?php echo $userDoc[0]->getICback();?>" class="id-img" >
            </div>

        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<div class="invite-btn blue-button text-center print-button pill-button" onclick="window.print()"><?php echo _MULTIBANK_PRINT ?></div>
        </div>
    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>

</body>
</html>