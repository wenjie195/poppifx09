<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/editProfile.php" />
    <link rel="canonical" href="https://victory5.co/editProfile.php" />
    <meta property="og:title" content="Edit Profile | Victory 5" />
    <title>Edit Profile | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php
$userIC = $userData->getIcno();
if($userIC == '')
{
?>
    <?php include 'header.php'; ?>
<?php
}
else
{
?>
    <?php include 'userHeader.php'; ?>
<?php
}
?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">

    <form action="utilities/editProfileFunction.php" method="POST">
    <div class="width100 overflow text-center">
    	<img src="img/edit-profile2.png" class="middle-title-icon" alt="<?php echo _JS_EDIT_PROFILE ?>" title="<?php echo _JS_EDIT_PROFILE ?>">
    </div>
    <div class="width100 overflow">   
    	<h1 class="pop-h1 h1-title text-center"><?php echo _JS_EDIT_PROFILE ?></h1>
    </div>
        <div class="dual-input">
            <p class="input-top-text min-height-p"><?php echo _JS_USERNAME ?></p>
            <p class="bottom-input-text"><?php echo $userData->getUsername();?></p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text min-height-p"><?php echo _INDEX_IC_NO ?><br><span class="small-note"><?php echo _INDEX_NO_REPEAT_IC ?></span></p>
            <?php
            $userIC = $userData->getIcno();
            if($userIC == '')
            {
            ?>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_IC_NO ?>" id="update_icno" name="update_icno" required>
            <?php
            }
            else
            {
            ?>
            		<p class="bottom-input-text"><?php echo $userData->getIcno();?></p>
            <?php
            }
            ?>

        </div>
        
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <!-- <p class="bottom-input-text"><?php //echo $userData->getFirstname();?></p> -->

            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" value="<?php echo $userData->getFirstname();?>" id="update_firstname" name="update_firstname" required>

        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
            <!-- <p class="bottom-input-text"><?php //echo $userData->getLastname();?></p> -->

            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" value="<?php echo $userData->getLastname();?>" id="update_lastname" name="update_lastname" required>

        </div>

        <div class="clear"></div>

        <div class="dual-input">   
            <p class="input-top-text"><?php echo _JS_FULLNAME ?></p>
            <p class="bottom-input-text"><?php echo $userData->getLastname();?> <?php echo $userData->getFirstname();?></p>
            <!-- <input class="clean pop-input" type="hidden" value="<?php //echo $userData->getLastname();?> <?php //echo $userData->getFirstname();?>" id="update_fullname" name="update_fullname" required> -->
        </div>
        
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_MOBILE_NO ?>" value="<?php echo $userData->getPhoneNo();?>" id="update_mobileno" name="update_mobileno" required>
        </div>

        <div class="clear"></div>

        <!-- <div class="dual-input second-dual-input"> -->
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
        </div>

        <!-- <div class="dual-input"> -->
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _INDEX_DOB ?></p>
            <input class="clean pop-input" type="date" placeholder="<?php echo _INDEX_DOB ?>" value="<?php echo $userData->getBirthDate();?>" id="update_dob" name="update_dob">      
        </div>

        <div class="clear"></div>

		<div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?>" value="<?php echo $userData->getAddress();?>" id="update_address" name="update_address" required>
        </div>

        <div class="clear"></div>

        <div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?> 2</p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?> 2" value="<?php echo $userData->getAddressB();?>" id="update_address_two" name="update_address_two">
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_ZIPCODE ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ZIPCODE ?>" value="<?php echo $userData->getZipcode();?>" id="update_zipcode" name="update_zipcode" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <select class="clean pop-input" id="update_country" name="update_country" required>

                <!-- <option>Please Select A Country</option> -->
                <option><?php echo $userData->getCountry();?></option>
                <?php
                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                {
                ?>
                <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                ?>"> 
                <?php 
                echo $countryList[$cntPro]->getEnName(); //take in display the options
                ?>
                </option>
                <?php
                }
                ?>

            </select>
        </div>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>