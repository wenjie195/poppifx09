<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$dateCreated = date('Y-m-d');

$dailyBonusDetails = getDailyBonus($conn, "ORDER BY date_created DESC");
$dailyBonusDetailsDaily = getDailyBonus($conn, "WHERE display = 1");
$dailyBonusDetailsDailyUnreleased = getDailyBonus($conn, "WHERE display = 0");
// $dailyBonusDetails = getDailyBonus($conn, "ORDER BY date_created DESC");
// $dailyBonusDetailsDaily = getDailyBonus($conn, "WHERE date_created >= ? and display = 1", array("date_created"), array($dateCreated), "s");
// $dailyBonusDetailsDailyUnreleased = getDailyBonus($conn, "WHERE date_created >= ? and display = 0", array("date_created"), array($dateCreated), "s");
$totalBon = 0;
$totalBonReleased = 0;
$totalBonUnreleased = 0;

$a = 1;

if ($dailyBonusDetails) {
  for ($m=0; $m <count($dailyBonusDetails) ; $m++) {
    $bonus = $dailyBonusDetails[$m]->getBonus();
    $totalBon += $bonus;
  }
}else {
  $totalBon = 0;
}
if ($dailyBonusDetailsDaily) {
  for ($m=0; $m <count($dailyBonusDetailsDaily) ; $m++) {
    $bonusReleased = $dailyBonusDetailsDaily[$m]->getBonus();
    $totalBonReleased += $bonusReleased;
  }
}else {
  $totalBonReleased = 0;
}
if ($dailyBonusDetailsDailyUnreleased) {
  for ($m=0; $m <count($dailyBonusDetailsDailyUnreleased) ; $m++) {
    $bonusUnreleased = $dailyBonusDetailsDailyUnreleased[$m]->getBonus();
    $totalBonUnreleased += $bonusUnreleased;
  }
}else {
  $totalBonUnreleased = 0;
}
$totalBonusReleased = number_format($totalBonReleased,4);
$totalBonusUnreleased = number_format($totalBonUnreleased,4);
$totalBonus = number_format($totalBon,4);



$monthlyProBon = getMonProBon($conn);

$cntAA = 1;

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminViewDaily.php" />
    <link rel="canonical" href="https://victory5.co/adminViewDaily.php" />
    <meta property="og:title" content="Daily Spread Report  | Victory 5" />
    <title>Daily Spread Report  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .total-payout{
    float: right;
    outline-style: dashed;
    outline-offset: 10px;
    outline-color: grey;
    margin-right: 10px;
  }


</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/daily.png" class="middle-title-icon" alt="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>" title="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>">
    </div> 

	<div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></h1>
    </div>
    <div class="width100 shipping-div2">

      <div class="search-big-div">
          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _ADMINVIEWBALANCE_NAME ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="fromInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _DAILY_FROM ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateStart" placeholder="<?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateEnd" placeholder="<?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input">
          </div>
      </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _DAILY_FROM ?></th>
                        <th><?php echo _DAILY_RELEASED_BONUS ?></th>
                        <th><?php echo _DAILY_UNRELEASED_BONUS ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_EDIT ?></th> -->
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_VIEW ?></th> -->
                    </tr>
                </thead>
                <tbody id="myTable">

                    <?php
                    if($dailyBonusDetails)
                    {
                      for ($k=0; $k <count($dailyBonusDetails) ; $k++) {
                        ?>
                            <tr>
                                <td><?php echo $a++; ?></td>
                                <td><?php echo $dailyBonusDetails[$k]->getUsername();?></td>
                                <td><?php echo $dailyBonusDetails[$k]->getFromWho();?></td>
                                <?php if ($dailyBonusDetails[$k]->getDisplay() == 1) {
                                  ?><td><?php echo "$ ".number_format($dailyBonusDetails[$k]->getBonus(),4);?></td><?php
                                }else {
                                  ?><td class="unreleased"></td> <?php
                                }
                                if ($dailyBonusDetails[$k]->getDisplay() == 0) {
                                  ?><td><?php echo "$ ".number_format($dailyBonusDetails[$k]->getBonus(),4);?></td><?php
                                }else {
                                  ?><td class="unreleased"></td> <?php
                                } ?>
                                <td><?php echo date('d/m/Y',strtotime($dailyBonusDetails[$k]->getDateCreated())) ?></td>
                                <td><?php echo date('h:i a',strtotime($dailyBonusDetails[$k]->getDateCreated())) ?></td>
                                <!-- <td><a href="#" class="blue-link"><?php// echo _ADMINVIEWBALANCE_EDIT ?></a></td> -->
                                <!-- <td><a href="#" class="blue-link"><?php //echo _ADMINVIEWBALANCE_VIEW ?></a></td> -->
                            </tr>
                        <?php
                      }
                        ?>
                    <?php
                  }else {
                    ?><td colspan="7" style="text-align: center;font-weight: bold"><?php echo _DAILY_NO_REPORT ?></td> <?php
                  }
                    ?>

                </tbody>
            </table>
        </div><br>
        <br>
        <div class="total-payout">
      <p><?php echo _ADMINVIEWBALANCE_PAYOUT." $ ".$totalBonus ?></p>
      <p><?php echo _ADMINVIEWBALANCE_PAYOUT_RELEASED." $ ".$totalBonusReleased ?></p>
      <p><?php echo _ADMINVIEWBALANCE_PAYOUT_UNRELEASED." $ ".$totalBonusUnreleased ?></p>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
<script>
$(document).ready(function(){
  $("#dateStart,#dateEnd").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );
  $("#usernameInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#fromInput").on("keyup", function() {
    var value2 = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value2) > -1)
    });
  });
  $("#dateStart,#dateEnd").on("change", function() {
    $("#myTable").empty();
    var value3 = $("#dateStart").val();
    var value4 = $("#dateEnd").val();
    // alert(value3);
    // $("#myTable tr").filter(function() {
    //   $(this).toggle($(this).text().toLowerCase().indexOf(value3) > -1)
    // });
    $.ajax({
      url: 'totalPayout.php',
      data: {dateStart:value3,dateEnd:value4},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var len = response.length;
        var releasedBonus;
        var totalPayout = response[0]['totalPayout'];
        var totalPayoutReleased = response[0]['totalPayoutReleased'];
        var totalPayoutUnreleased = response[0]['totalPayoutUnreleased'];
        var date = response[0]['date'];
        $("#myTable").empty();

        for( var m = 0; m<len; m++){
          var no = m + 1;
          var username = response[m]['username'];
          var from = response[m]['from'];
          var bonus = response[m]['bonus'];
          var display = response[m]['display'];
          var dateCreated = response[m]['dateCreated'];
          var timeCreated = response[m]['timeCreated'];

          if (display == 1) {
            releasedBonus = '$ '+bonus+'';
          }else {
            releasedBonus = '';
          }
          if (display == 0) {
            unreleasedBonus = '$ '+bonus+'';
          }else {
            unreleasedBonus = '';
          }
          // alert(username);

          $("#myTable").append('<tr><td>'+no+'</td><td>'+username+'</td><td>'+from+'</td><td>'+releasedBonus+'</td><td>'+unreleasedBonus+'</td><td>'+dateCreated+'</td><td>'+timeCreated+'</td></tr>');
        }

        $(".total-payout").empty();
        // alert(totalPayout);
        // alert(totalPayoutReleased);
        // alert(totalPayoutUnreleased);
        // alert(date);
        $(".total-payout").html('<p><?php echo _ADMINVIEWBALANCE_PAYOUT." $ " ?>'+totalPayout+'</p><p><?php echo _ADMINVIEWBALANCE_PAYOUT_RELEASED." $ " ?>'+totalPayoutReleased+'</p><p><?php echo _ADMINVIEWBALANCE_PAYOUT_UNRELEASED." $ " ?>'+totalPayoutUnreleased+'</p>');
      },
      error:function(response){
        $("#myTable").empty();
        $("#myTable").append('<tr><td style="text-align: center" colspan="7">No Daily Spread Report</td></tr>');
      }
    });
    // $(".total-payout").empty();
    // $(".total-payout").text("sss");
  });
});
</script>
