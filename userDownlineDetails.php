<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userDownlineDetails.php" />
    <link rel="canonical" href="https://victory5.co/userDownlineDetails.php" />
    <meta property="og:title" content="Deep Downline  | Victory 5" />
    <title>Deep Downline  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height">
  <h1 class="pop-h1"><?php echo _USERHEADER_HIERARCHY ?></h1>

  <div class="left-padding-div">
        <?php
        if(isset($_POST['user_uid']))
        {
        $conn = connDB();
        $getWho = getWholeDownlineTree($conn, $_POST['user_uid'],false);
            if($getWho)
            {
                echo '<ul>';
                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    if($thisPerson->getCurrentLevel() == $lowestLevel)
                    {
                        echo '<li id="'.$thisPerson->getReferralId().'"><b>'.$thisTempUser->getUsername().'</b></li>';
                    }
                }
                echo '<ul>';
                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    echo '
                    <script type="text/javascript">
                    var div = document.getElementById("'.$thisPerson->getReferrerId().'");
                    div.innerHTML += "<ul  class=\'tiara-ul2\' name=\'ul-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'><b>'.$thisTempUser->getUsername().'</b></li></ul>";
                    </script>
                    ';
                }
                echo '</ul>';
            }

        }
        ?>
	</div>

<div class="clear"></div>

</div>

<?php //include 'rankIdentifySolo.php' ?>
<?php include 'js.php'; ?>

</body>
</html>
