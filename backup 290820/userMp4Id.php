<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userMp4Id.php" />
    <link rel="canonical" href="https://victory5.co/userMp4Id.php" />
    <meta property="og:title" content="MT4  | Victory 5" />
    <title>MT4  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
<div class="menu-distance-height width100"></div>
    <form action="utilities/uploadMpIdFunction.php" method="POST">
<!-- <form action="#" method="POST"> -->
    <div class="password-width margin-auto overflow text-center">
		<h1 class="h1-title white-text text-center">Please Enter MT4 Account</h1>
        <div class="width100">
            <p class="input-top-text text-center">MT4</p>
            <input class="clean pop-input text-center" type="text" placeholder="MT4" id="register_mpid" name="register_mpid" required>
        </div>
		<div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean blue-button one-button-width" name="submit"><?php echo _INDEX_REGISTER ?></button>
        </div>
		</div>
    </form>

    <div class="clear"></div>

    <div class="width100 overflow text-center">

        <?php $actual_link = "https://$_SERVER[HTTP_HOST]"; ?>
        <input type="hidden" id="linkCopy" value="<?php echo "https://victory5.co/register.php?referrerUID=".$_SESSION['uid']?>">
        <h3 class="invite-h3"><b><?php echo _VICTORY_URL ?>:</b><br> <a id="invest-now-referral-link" href="<?php echo "https://victory5.co/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a black-link-underline"><?php echo "https://victory5.co/register.php?referrerUID=".$_SESSION['uid']?></a></h3>
        <div class="width100 text-center overflow">
            <button class="invite-btn blue-button margin-top15 pill-button margin-auto clean" id="copy-referral-link"><?php echo _VICTORY_Copy ?></button>
        </div>

    </div>

    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>