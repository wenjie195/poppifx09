<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$dateCreated = date('Y-m-d');
// $uid = $_SESSION['uid'];
$uid = $_GET['uid'];
$downlineUsername = [];
$directDownline = 0;

$userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($uid), "s");
$username = $userDetails[0]->getUsername();

// $getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
$getWho = getWholeDownlineTree($conn, $uid,false);

if ($getWho) {
  for ($i=0; $i <count($getWho) ; $i++) {
    $downlineUsername[] = $getWho[$i]->getReferralId();
  }
}

$downlineUsernameImp = implode(",",$downlineUsername);
$downlineUsernameExp = explode(",",$downlineUsernameImp);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userViewGroupMember.php" />
    <link rel="canonical" href="https://victory5.co/userViewGroupMember.php" />
    <meta property="og:title" content="<?php echo $username."'s" ?> Group Member  | Victory 5" />
    <title><?php echo $username."'s" ?> Group Member  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
  <h1 class="text-center">
    <a style="float: left" href="#" onclick="window.history.back()"><img style="vertical-align: middle;width: 35px;" src="img/back2.png" alt=""></a> <?php echo $username."'s Group Member" ?></h1>
    <div class="width100 shipping-div2">

      <div class="search-big-div">
          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _ADMINVIEWBALANCE_NAME ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="datex" placeholder="<?php echo _MULTIBANK_SEARCH." "._DAILY_DATE ?>" value="<?php //echo date('d/m/y') ?>" class="clean pop-input fake-input">
          </div>
      </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _JS_FULLNAME ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_VIEW ?></th> -->
                    </tr>
                </thead>
                <tbody id="myTable">

                    <?php
                    if($downlineUsernameExp)
                    {
                        for($cnt = 0;$cnt < count($downlineUsernameExp) ;$cnt++)
                        {
                          $conn = connDB();
                          $downlineDets = getUser($conn,"WHERE uid=?",array("uid"),array($downlineUsernameExp[$cnt]), "s");
                        ?>
                            <tr>
                                <td><?php echo $cnt+1; ?></td>
                                <td><?php echo $downlineDets[0]->getUsername();?></td>
                                <td>
                                  <?php
                                  if ($downlineDets[0]->getFullName()) {
                                    echo $downlineDets[0]->getFullName();
                                  }else {
                                    echo "-";
                                  }
                                   ?>
                                </td>
                                <td><?php echo date('d/m/Y',strtotime($downlineDets[0]->getDateCreated())) ?></td>
                                <td><?php echo date('h:i a',strtotime($downlineDets[0]->getDateCreated())) ?></td>
                                <!-- <td><a href="#" class="blue-link"><?php //echo _ADMINVIEWBALANCE_VIEW ?></a></td> -->
                            </tr>
                        <?php
                      }
                        ?>
                    <?php
                  }else {
                    ?><td colspan="7" style="text-align: center;font-weight: bold"><?php echo _DAILY_NO_REPORT ?></td> <?php
                  }
                    ?>

                </tbody>
                <td colspan="7" class="ss" style="text-align: center;font-weight: bold;display: none"><?php echo _DAILY_NO_REPORT ?></td>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
<script>
$(document).ready(function(){
  $("#datex").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );
  $("#usernameInput,#fromInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#datex").on("change", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

    if($("#myTable tr").is(":hidden")){
      $(".ss").show();
    }
    if($("#myTable tr").is(":visible")){
      $(".ss").hide();
    }
  });
  var value = $("#datex").val().toLowerCase();
  $("#myTable tr").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
  });
  if($("#myTable tr").is(":hidden")){
    $(".ss").show();
  }
  if($("#myTable tr").is(":visible")){
    $(".ss").hide();
  }
});
</script>
