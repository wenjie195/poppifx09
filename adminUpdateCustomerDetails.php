<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/adminUpdateCustomerDetails.php" />
    <meta property="og:url" content="https://victory5.co/adminUpdateCustomerDetails.php" />
    <meta property="og:title" content="Admin Edit Profile | Victory 5" />
    <title>Admin Edit Profile | Victory 5</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">

    <?php
    if(isset($_POST['customer_id']) || isset($_GET['uid']))
    {
    $conn = connDB();
    if (isset($_POST['customer_id'])) {
    $uid = $_POST['customer_id'];
    $customerDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['customer_id']),"s");
    }elseif (isset($_GET['uid'])) {
    $uid = $_GET['uid'];
    $customerDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_GET['uid']),"s");
    }
    ?>

        <!-- <form action="utilities/editProfileFunction.php" method="POST"> -->
        <form action="utilities/adminEditProfileFunction.php" method="POST">
            <h3 class="text-center small-h1-a"><?php echo _JS_EDIT_USER_PROFILE ?> | <a  href="adminUpdateUserPassword.php?uid=<?php echo $uid ?>"><?php echo _JS_CHANGE_USER_PASSWORD ?></a> | <a  href="adminUpdateBankDetails.php?uid=<?php echo $uid ?>"><?php echo _ADMINVIEWBALANCE_EDIT.""._USERHEADER_BANK_DETAILS ?></a> </h3>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" value="<?php echo $customerDetails[0]->getUsername();?>" id="update_username" name="update_username">
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _INDEX_IC_NO ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_IC_NO ?>" value="<?php echo $customerDetails[0]->getIcno();?>" id="update_icno" name="update_icno">
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text"><?php echo _INDEX_DOB ?></p>
                <input class="clean pop-input" type="date" placeholder="<?php echo _INDEX_DOB ?>" value="<?php echo $customerDetails[0]->getBirthDate();?>" id="update_dob" name="update_dob">
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_MOBILE_NO ?>" value="<?php echo $customerDetails[0]->getPhoneNo();?>" id="update_mobileno" name="update_mobileno">
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text"><?php echo _JS_FULLNAME ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_FULLNAME ?>" value="<?php echo $customerDetails[0]->getFullname();?>" id="update_fullname" name="update_fullname">
            </div>

            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" value="<?php echo $customerDetails[0]->getFirstname();?>" id="update_firstname" name="update_firstname">
            </div>
			<div class="clear"></div>
            <!-- <div class="dual-input second-dual-input"> -->
            <div class="dual-input">
                <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" value="<?php echo $customerDetails[0]->getLastname();?>" id="update_lastname" name="update_lastname">
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">MT4 ID</p>
                <input class="clean pop-input" type="text" placeholder="MT4 ID" value="<?php echo $customerDetails[0]->getMpId();?>" id="update_mpid" name="update_mpid">
            </div>
			<div class="clear"></div>
            <!-- <div class="dual-input second-dual-input"> -->
            <div class="dual-input">
                <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" value="<?php echo $customerDetails[0]->getEmail();?>" id="update_email" name="update_email">
            </div>



            <!-- <div class="width100">
                <p class="input-top-text">MT4 ID</p>
                <input class="clean pop-input" type="text" placeholder="MT4 ID" value="<?php echo $customerDetails[0]->getMpId();?>" id="update_mpid" name="update_mpid">
            </div> -->

            <div class="clear"></div>

            <div class="width100">
                <p class="input-top-text"><?php echo _INDEX_ADDRESS ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?>" value="<?php echo $customerDetails[0]->getAddress();?>" id="update_address" name="update_address">
            </div>

            <div class="width100">
                <p class="input-top-text"><?php echo _INDEX_ADDRESS ?> 2</p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?> 2" value="<?php echo $customerDetails[0]->getAddressB();?>" id="update_address_two" name="update_address_two">
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _INDEX_ZIPCODE ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ZIPCODE ?>" value="<?php echo $customerDetails[0]->getZipcode();?>" id="update_zipcode" name="update_zipcode">
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
                <select class="clean pop-input" id="update_country" name="update_country">

                    <option><?php echo $customerDetails[0]->getCountry();?></option>
                    <?php
                    for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                    {
                    ?>
                    <option value="<?php echo $countryList[$cntPro]->getEnName();
                    ?>">
                    <?php
                    echo $countryList[$cntPro]->getEnName();
                    ?>
                    </option>
                    <?php
                    }
                    ?>

                </select>
            </div>

            <input class="clean pop-input" type="hidden" value="<?php echo $customerDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>
			<div class="clear"></div>
            <div class="width100 text-center">
                <button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>
</body>
</html>
