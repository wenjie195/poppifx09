<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $statusData = $statusDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/uploadDoc.php" />
    <link rel="canonical" href="https://victory5.co/uploadDoc.php" />
    <meta property="og:title" content="Upload Document  | Victory 5" />
    <title>Upload Document  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height text-center" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/upload-doc.png" class="middle-title-icon" alt="<?php echo _USERHEADER_UPLOAD_DOC ?>" title="<?php echo _USERHEADER_UPLOAD_DOC ?>">
    </div>   
    <div class="width100 overflow">  
    	<h1 class="pop-h1"><?php echo _USERHEADER_UPLOAD_DOC ?></h1>
    </div>
    <h3 class="margin-top50"><?php echo _UPLOAD_PROOF_OF_LEGAL_NAME ?></h3>
    <p class="small-note-p"><?php echo _UPLOAD_PROOF_OF_LEGAL_NAME_DESC ?></p>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload1" type="file" name="IcFront" accept="image/*" />
      <label for="file-upload1">
        <?php
        if($userDetails->getIcFront() == 1)
        {
        ?>
            <img class="signature-new-css">
        <?php
        }
        else
        {
        ?>
          <?php
          $statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
          $statusData = $statusDetails[0];
          ?>
          <img id="image1" class="upload" src="<?php echo "uploads/".$statusData->getICFront() ?>" alt="">
        <?php
        }
        ?>
        <div class="text1 file-text"><?php echo _UPLOAD_CHOOSE_FILE_HERE ?></div>
      </label>
      <div class="clear"></div>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width pill-button margin-auto"  type="submit" name="submitIcFront"><?php echo _UPLOAD ?></button>
        </div>
    <!-- <div class="width100">
    	<img src="img/laptop.png" class="doc-css">
    </div> -->
    <h3 class="document2 margin-top50"><?php echo _UPLOAD_PROOF_OF_LEGAL_NAME_BACK ?></h3>
    <p class="small-note-p"><?php echo _UPLOAD_PROOF_OF_LEGAL_NAME_DESC ?></p>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload2" type="file" name="icBack" accept="image/*" />
      <label for="file-upload2">
        <?php
        if($userDetails->getIcBack() == 1)
        {
        ?>
            <img class="signature-new-css">
        <?php
        }
        else
        {
        ?>
          <?php
          $statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
          $statusData = $statusDetails[0];
          ?>
          <img id="image2" class="upload" src="<?php echo "uploads/".$statusData->getICBack() ?>" alt="">
        <?php
        }
        ?>
        <div class="text2 file-text"><?php echo _UPLOAD_CHOOSE_FILE_HERE ?></div>
      </label>
      <div class="clear"></div>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width pill-button margin-auto"  type="submit" name="submitIcBack"><?php echo _UPLOAD ?></button>
        </div>
    </form>

    <h3 class="document2 margin-top50"><?php echo _UPLOAD_SIGNATURE ?></h3>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload3" type="file" name="sign" accept="image/*" />
      <label for="file-upload3">
        <?php
        if($userDetails->getSignature() == 1)
        {
        ?>
            <img class="signature-new-css">
        <?php
        }
        else
        {
        ?>
          <?php
          $statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
          $statusData = $statusDetails[0];
          ?>
          <img id="image3" class="upload" src="<?php echo "uploads/".$statusData->getSignature() ?>" alt="">
        <?php
        }
        ?>
        <div class="text3 file-text"><?php echo _UPLOAD_CHOOSE_FILE_HERE ?></div>
      </label>
      <div class="clear"></div>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width pill-button margin-auto"  type="submit" name="submitSignature"><?php echo _UPLOAD ?></button>
        </div>
    </form>

    <h3 class="document2 margin-top50"><?php echo _UPLOAD_PROOF_OF_ADDRESS ?></h3>
    <p class="small-note-p"><?php echo _UPLOAD_PROOF_OF_ADDRESS_DESC ?></p>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload4" type="file" name="license" accept="image/*" />
      <label for="file-upload4">
        <?php
        if($userDetails->getLicense() == 1)
        {
        ?>
            <img class="signature-new-css">
        <?php
        }
        else
        {
        ?>
          <?php
          $statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
          $statusData = $statusDetails[0];
          ?>
          <img id="image4" class="upload" src="<?php echo "uploads/".$statusData->getLicense() ?>" alt="">
        <?php
        }
        ?>
        <div class="text4 file-text"><?php echo _UPLOAD_CHOOSE_FILE_HERE ?></div>
      </label>
      <div class="clear"></div>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width pill-button margin-auto"  type="submit" name="submitLicense"><?php echo _UPLOAD ?></button>
        </div>
    </form>
    <!-- <div class="width100">
    	<img src="img/laptop.png" class="doc-css">
    </div> -->

</div>
<?php include 'js.php'; ?>
<script>
  $("#file-upload1").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image1").attr("src","");
    $(".text1").html('<p>'+x+'</p>');
    $("button[name='submitIcFront']").removeAttr('disabled');
  });
  $("#file-upload2").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image2").attr("src","");
    $(".text2").html('<p>'+x+'</p>');
    $("button[name='submitIcBack']").removeAttr('disabled');
  });
  $("#file-upload3").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image3").attr("src","");
    $(".text3").html('<p>'+x+'</p>');
    $("button[name='submitSignature']").removeAttr('disabled');
  });
  $("#file-upload4").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image4").attr("src","");
    $(".text4").html('<p>'+x+'</p>');
    $("button[name='submitLicense']").removeAttr('disabled');
  });
</script>
</body>
</html>
