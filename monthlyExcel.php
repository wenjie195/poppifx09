<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$companyBalance = getCompanyBalance($conn);
$totalPoint = 0;
$id = 1;
include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import3"]))
{
    $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if(in_array($_FILES["file"]["type"],$allowedFileType))
    {
        $targetPath = 'uploadExcel/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);

            foreach ($Reader as $Row)
            {

                $userName = "";
                if(isset($Row[0]))
                {
                    $userName = mysqli_real_escape_string($conn,$Row[1]);
                }

                $userID = "";
                if(isset($Row[0]))
                {
                    $userID = mysqli_real_escape_string($conn,$Row[2]);
                }
                // $userName = "";
                // if(isset($Row[0]))
                // {
                //     $userName = mysqli_real_escape_string($conn,$Row[1]);
                // }
                $point = "";
                if(isset($Row[1]))
                {
                    $point = mysqli_real_escape_string($conn,$Row[7]);
                }

                // if (!empty($userName) || !empty($point) || !empty($userID))
                if (!empty($point) || !empty($userID))
                {
                    $query = "UPDATE `equityplrawdata` SET `balance`='".$point."' WHERE  mp_id = '".$userID."' ";
                    $result = mysqli_query($conn, $query);
                    if (! empty($result))
                    {
                        // echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminAddOnCredit.php'</script>";

                        // $query = "INSERT INTO equityplbackupdata (mp_id,balance) VALUES ('".$userID."','".$point."') ";
                        $query = "INSERT INTO equityplbackupdata (name,mp_id,balance) VALUES ('".$userName."','".$userID."','".$point."') ";
                        $result = mysqli_query($conn, $query);
                        if (! empty($result))
                        {
                          $_SESSION['messageType'] = 1;
                          header('location: ./adminDashboard.php?type=10');
                        }
                        else
                        {
                          $_SESSION['messageType'] = 1;
                          header('location: ./adminDashboard.php?type=12');
                        }


                    }
                    else
                    {
                      $_SESSION['messageType'] = 1;
                      header('location: ./adminDashboard.php?type=13');
                    }
                }
                else
                {
                  $_SESSION['messageType'] = 1;
                  header('location: ./adminDashboard.php?type=14');
                }

            }

        }
    }
    else
    {
      $_SESSION['messageType'] = 1;
      header('location: ./adminDashboard.php?type=15');
    }
}
 ?>
