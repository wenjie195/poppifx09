<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
// $userDet = getUser($conn,"WHERE username != 'admin' and username != 'company'");
$userDet = getUser($conn,"WHERE username != 'admin' and username != 'infinox'");

for ($j=0; $j < count($userDet) ; $j++) {
  $uid = $userDet[$j]->getUid();
  echo "<br><br>".$userDet[$j]->getUsername()."<br>";
// $uid = $_SESSION['uid'];
// $uid = "9477a495c932ddfed3374e0b947f6e18";
$rankLoopTot = 0;
$rankLoop = 0;
$groupSales = 0;
$personalSales = 0;
$totalDownline = 0;
$getWho = 0;
$downlinePersonalSales = 0;
$totalIndirectLeader = 0;
$totalIndirectTeamLeader = 0;
$totalIndirectGroupLeader = 0;
$totalDirectLeader = 0;
$totalDirectTeamLeader = 0;
$totalDirectGroupLeader = 0;
$totalDirectIndirectLeader = 0;
$totalDirectIndirectTeamLeader = 0;
$totalDirectIndirectGroupLeader = 0;
$downlineDetailssArray = [];
$rank = "";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s"); // get specific users
$currentRank = $userDetails[0]->getRank();
$oldRank = $userDetails[0]->getOldRank();
$rankLoop = $userDetails[0]->getRankLoop();
$rankLoopTot = $rankLoop + 1;
// $rank = $currentRank;
$dateCreated = date('Ymd',strtotime($userDetails[0]->getDateCreated()));
echo "Date Created :".$dateCreated."<br>";
$mpIdData = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");
if ($mpIdData) {
  for ($a=0; $a <count($mpIdData) ; $a++) {
    $personalSales += $mpIdData[$a]->getBalance();
  }
}else {
  $personalSales = 0;
}
// $personalSales = $userDetails[0]->getPersonalSales(); // get your personal sales
$userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uid),"s");
$currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
$indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level
$getWho = getWholeDownlineTree($conn, $uid,false); // get downline function

for ($i=0; $i <count($getWho) ; $i++) {
  // $totalDownline = count($getWho); // get how many downline he get
  $getWho[$i]->getReferralId();

  $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
  // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
  $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
  // $indirectDownlineCurrentLevel = $currentLevel + 1;
  $downlineReferralId = $downlineReferralHistory[0]->getReferralId(); // get downline current level
  $downlineReferrerId = $downlineReferralHistory[0]->getReferrerId(); // get downline current level
  // echo $downlineCurrentLevel." VS ".$indirectCurrentLevel;
  if ($downlineCurrentLevel == $indirectCurrentLevel) {
    $downlineDetailssII = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
    // echo $downlineDetailssII[0]->getUsername()."<br>";
    // echo $downlineDetailssII[0]->getRank()."<br>";
    $directDownlineRank = $downlineDetailssII[0]->getRank();

    // if (in_array( $downlineDetailss[0]->getUsername(), $downlineDetailssArray)) { // not loop same user again
    //   echo "Got Already ".$downlineDetailss[0]->getUsername()."<br>";
    // }else {
      if ($directDownlineRank == 'Manager') {
        // echo "inDirect C";
        $totalDirectLeader++;
         // $totalIndirectLeader = count($indirectDownlineRank);
      }
      if ($directDownlineRank == 'Senior Manager') {
      // echo  "inDirect B";
      $totalDirectTeamLeader++;
         // $totalIndirectTeamLeader = count($indirectDownlineRank);
      }
      if ($directDownlineRank == 'Director') {
        // echo "inDirect A";
        $totalDirectGroupLeader++;
         // $totalIndirectGroupLeader = count($indirectDownlineRank);
      }
    $totalDownline++;
  }
  if ($downlineCurrentLevel > $indirectCurrentLevel) { //compare downline current level to get indirect level
    $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
    // echo $downlineDetailss[0]->getUsername()."<br>";
    // echo $downlineDetailss[0]->getRank()."<br>";
    $indirectDownlineRank = $downlineDetailss[0]->getRank();

    // if (in_array( $downlineDetailss[0]->getUsername(), $downlineDetailssArray)) { // not loop same user again
    //   echo "Got Already ".$downlineDetailss[0]->getUsername()."<br>";
    // }else {
      if ($indirectDownlineRank == 'Manager') {
        // echo "inDirect C";
        $totalIndirectLeader++;
         // $totalIndirectLeader = count($indirectDownlineRank);
      }
      if ($indirectDownlineRank == 'Senior Manager') {
      // echo  "inDirect B";
      $totalIndirectTeamLeader++;
         // $totalIndirectTeamLeader = count($indirectDownlineRank);
      }
      if ($indirectDownlineRank == 'Director') {
        // echo "inDirect A";
        $totalIndirectGroupLeader++;
         // $totalIndirectGroupLeader = count($indirectDownlineRank);
      }
      // $downlineDetailssArray[] = $downlineDetailss[0]->getUsername();
      // echo implode(",",$downlineDetailssArray);
    // }
  }

  $downlineDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($getWho[$i]->getReferralId()),"s");
  $mpIdDataGroup = getMpIdData($conn,"WHERE uid =?",array("uid"),array($downlineDetails[0]->getUid()),"s");
  if ($mpIdDataGroup) {
    for ($b=0; $b <count($mpIdDataGroup) ; $b++) {
      $downlinePersonalSales += $mpIdDataGroup[$b]->getBalance();
    }
  }

  if (!$downlinePersonalSales) {
    $downlinePersonalSales = 0;
  }
}

// $downlinePersonalSales = $downlineDetails[0]->getPersonalSales(); // get your downline personal sales
$groupSales += $downlinePersonalSales; // get group sales addiction of your downline personal sales
$totalDirectIndirectLeader = $totalDirectLeader + $totalIndirectLeader;
$totalDirectIndirectTeamLeader = $totalDirectTeamLeader + $totalIndirectTeamLeader;
$totalDirectIndirectGroupLeader = $totalDirectGroupLeader + $totalIndirectGroupLeader;
echo "Personal Sales :".$personalSales." ";
echo "Group Sales :".$groupSales."<br>";
echo "Direct Downline :".$totalDownline;
echo " Indirect Manager :".$totalIndirectLeader;
echo " Indirect Senior Manager :".$totalIndirectTeamLeader;
echo " Indirect Director :".$totalIndirectGroupLeader."<br>";
echo " Direct Manager :".$totalDirectLeader;
echo " Direct Senior Manager :".$totalDirectTeamLeader;
echo " Direct Director :".$totalDirectGroupLeader."<br>";

// echo $groupSales;

  if ($personalSales >= 10000 && $totalDownline >= 3 && $totalDirectTeamLeader >= 3 && $groupSales >= 500000) { //  if personal sales >= 5000 , 2 normal member, 2 indirect rank b member and sales group >= 100000

     $rank = "Director";
     echo $rank." ";

  }
  elseif ($personalSales >= 5000 && $totalDownline >= 3 && $totalDirectLeader >= 3 && $groupSales >= 150000) { // if personal sales >= 3000 , 2 normal member, 2 indirect rank c member and sales group >= 30000

     $rank = "Senior Manager";
     echo $rank." ";

  }
  elseif ($personalSales >= 100 && $groupSales >= 50000) { // if personal sales >= 2000 , 2 downline and sales group >= 5000

     $rank = "Manager";
     echo $rank." ";

  }else {
      $rank = "Member";
      echo "else :".$rank." ";
  }

  // if ($dateCreated <= 20200409) {
  //   echo "Old Member ";
  //   if ($personalSales >= 5000 && $totalDownline >= 3 && $totalDirectIndirectTeamLeader >= 2 && $groupSales >= 100000 && $currentRank) { //  if personal sales >= 5000 , 2 normal member, 2 indirect rank b member and sales group >= 100000
  //
  //      $rank = "Group Leader";
  //
  //   }
  //   elseif ($personalSales >= 3000 && $totalDownline >= 3 && $totalDirectIndirectLeader >= 2 && $groupSales >= 30000 && $currentRank != "Group Leader") { // if personal sales >= 3000 , 2 normal member, 2 indirect rank c member and sales group >= 30000
  //
  //      $rank = "Team Leader";
  //
  //   }
  //   elseif ($personalSales >= 1000 && $totalDownline >= 3 && $groupSales >= 5000 && $currentRank != "Team Leader" && $currentRank != "Group Leader") { // if personal sales >= 2000 , 2 downline and sales group >= 5000
  //
  //      $rank = "Leader";
  //
  //   }else {
  //     if ($oldRank) {
  //       $rank = $oldRank;
  //     }else {
  //       $rank = "Member";
  //     }
  //     // $rank = $currentRank;
  //   }
  // }
  // if ($personalSales < 1000 && !$rank) { // if personal sales >= 2000 , 2 downline and sales group >= 5000
  //
  //    $rank = "Members";
  //
  // }
      echo "New Rank :".$rank;
       $tableName = array();
       $tableValue =  array();
       $stringType =  "";
       if($rank)
       {
            array_push($tableName,"rank");
            array_push($tableValue,$rank);
            $stringType .=  "s";
       }
       array_push($tableValue,$uid);
       $stringType .=  "s";
       $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
       if($messageStatusInUser)
       {
          // echo "success";
            // header('Location: userDashboard.php');
       }
      // if ($dateCreated <= 20200409) {
       // if ($rankLoopTot <= 3) {
       //   echo "New Rank :".$rank;
       //    $tableName = array();
       //    $tableValue =  array();
       //    $stringType =  "";
       //    if($rank)
       //    {
       //         array_push($tableName,"old_rank");
       //         array_push($tableValue,$rank);
       //         $stringType .=  "s";
       //    }
       //    if($rankLoopTot)
       //    {
       //         array_push($tableName,"rank_loop");
       //         array_push($tableValue,$rankLoopTot);
       //         $stringType .=  "s";
       //    }
       //    array_push($tableValue,$uid);
       //    $stringType .=  "s";
       //    $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
       //    if($messageStatusInUser)
       //    {
       //       // echo "success";
       //         // header('Location: userDashboard.php');
       //    }
       // }
     // }else {
       $tableName = array();
       $tableValue =  array();
       $stringType =  "";

       if($rank)
       {
            array_push($tableName,"old_rank");
            array_push($tableValue,$rank);
            $stringType .=  "s";
       }
       array_push($tableValue,$uid);
       $stringType .=  "s";
       $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
       if($messageStatusInUser)
       {
          // echo "success";
            // header('Location: userDashboard.php');
       }
     // }
}
 ?>
