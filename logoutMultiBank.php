<?php
    require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
    $conn = connDB();
    //Start the session
    session_start();
    //Check f the session is empty/exist or not
    if(!empty($_SESSION))
    {
        $lang = $_SESSION['lang'];//temporarily store language preference
        //Insert login timestamp
        // remove all session variables
        session_unset();

        // destroy the session 
        session_destroy();

        session_start();
        $_SESSION['lang'] = $lang;//set language preference back again

        // Go back to index page 
        // NOTE : MUST PROMPT ERROR
        
        // header('Location:index.php');
        // exit();
        // header('Location: https://myaccount.infinox.bs/register/index?invitationCode=NGX9C6DK');
        header('Location: https://myaccount.infinox.bs/register/index?invitationCode=XZN9C6D1&language=en');
        exit();
    }
    else
    {
        header('Location:index.php');
    }
    exit;
?>