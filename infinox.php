<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$docRows = getStatus($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$docDetails = $docRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/infinox.php" />
    <link rel="canonical" href="https://victory5.co/infinox.php" />
    <meta property="og:title" content="INFINOX  | Victory 5" />
    <title>INFINOX  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'userHeader.php'; ?>


		<div class="menu-distance-height width100 no-print"></div>
  		<!--<img src="img/multibank-white.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">-->
        <div class="page1-div same-padding">
            <p class="infi-big-title text-center">
                <b><u>INFINOX CAPITAL SUITABILITY ASSESSMENT</u></b>
            </p>
            <p class="infi-small-title text-center"><b><u>FOR DISCRETIONARY MANAGED CLIENT ACCOUNTS UNDER AN LPOA</u></b></p>
            <p class="infi-small-title text-center"><b><u>CLIENT APPLICATION FORM</u></b></p>
            <p class="infi-small-title"><b><u>STEP 1: EXPLANATION OF THE SUITABILITY ASSESSMENT</u></b></p>
            <div class="yellow-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>SUITABILITY ASSESSMENT</u></b></p>
                <p class="red-small-text text-center"><b>All trading involves risk. Losses can exceed deposits.</b></p>
                <ul class="infi-ul">
                	<li>You will now be required to answer some further questions as part of the assessment of your suitability, for a discretionary managed account provided by a Money Manager, under a Limited Power of Attorney Arrangement.</li>
                    <li>You have already been assessed on your knowledge and experience and financial standing, you will now be assessed on your risk appetite, tolerance and investment objectives, as well as your monthly expenditure.</li>
                    <li>Please ensure to read all the questions carefully.</li>
                </ul>
             </div> 
             <p class="infi-small-title"><b><u>STEP 2: DISCRETIONARY INVESTMENT MANAGEMENT SERVICE</u></b></p>
            <div class="blue-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>DISCRETIONARY INVESTMENT MANAGEMENT SERVICE</u></b></p>
                <p class="infi-ul">You are applying for a discretionary investment managed account, this means that all trading decisions are made at the absolute discretion of your Money Manager, in line with the trading mandate agreed with you. The Money Manager will provide you services as your discretionary investment manager, via a limited power of attorney legal agreement.<br><br>
                This discretionary investment management service is not an advisory service and would not be suitable if a client wants to discuss trading decisions.<br><br>I confirm and agree to this <input class="clean checkbox-input" type="checkbox" required>
                
                </p>
             </div>              
             
             
             
             
            
        </div>
         <div class="bottom-page-div">

                       <p class="bottom-page-number text-center">Page 1 of 3</p>
        
        </div>       
        
        
       <div class="page-break"></div>

        
        
        <div class="page1-div same-padding">
             <p class="infi-small-title"><b><u>STEP 3: FINANCIAL COMMITMENTS</u></b></p>
            <div class="lightgrey-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>FINANCIAL COMMITMENTS DECLARATION</u></b></p>
                <p class="infi-ul">Please confirm that the money you intend to invest, is money you can afford to lose and you are still able to afford your existing and future regular financial commitments, including debts, loans, mortgages etc.?<br>
                <label><input type="radio" name="radio" class="checkbox-radio"> Yes</label>
                <br>
                <label><input type="radio" name="radio" class="checkbox-radio"> No</label>
                </p>
             </div> 
             <p class="infi-small-title"><b><u>STEP 4: RISK APPETITE AND INVESTMENT OBJECTIVES ASSESSMENT</u></b></p>
             <div class="green-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>RISK APPETITE AND INVESTMENT OBJECTIVES</u></b></p>
                <p class="red-small-text text-center"><b>Forex and CFDs are HIGH RISK financial products.</b></p>
                <p class="infi-ul">This means that it's not suitable for those looking to achieve a regular income from their investment, or for those looking for a low risk investment.<br><br>
                For each question below, please select one answer that you believe to be most relevant to you.<br><br>
                1. What is the purpose of your investment?
                <ul class="no-listing">
                	<li><label><input type="radio" name="radio2" class="checkbox-radio"> Making profit even at the risk of potentially losing the total capital I invested.</label></li>
                	<li><label><input type="radio" name="radio2" class="checkbox-radio"> To supplement or form part of my regular income.</label></li>
                    <li><label><input type="radio" name="radio2" class="checkbox-radio"> For long term savings.</label></li>
                </ul>
                </p>
                <p class="infi-ul">2. Which statement most closely describes your willingness to take risks?
                	<ul class="no-listing">
                        <li><label><input type="radio" name="radio3" class="checkbox-radio"> LOW risk: I only invest in products that do not jeopardize or significantly erode my capital.</label></li>
                        <li><label><input type="radio" name="radio3" class="checkbox-radio"> MEDIUM risk: I want to preserve short term financial security but also wish to benefit from higher risk investments and I accept the risks involved.</label></li>
                        <li><label><input type="radio" name="radio3" class="checkbox-radio"> HIGH risk: I accept considerable risk to my capital in the hope for potential higher returns. I am not concerned about short term security of my capital.</label></li>                    	
                    </ul>
                </p>
                <p class="infi-ul">3. Which statement is most relevant to you?
                	<ul class="no-listing">
                        <li><label><input type="radio" name="radio4" class="checkbox-radio"> Hoping for a higher than average return, I accept higher than average risk, even at the risk of losing some of my capital.</label></li>
                        <li><label><input type="radio" name="radio4" class="checkbox-radio"> The safety of my capital is the most important to me.</label></li>
                        <li><label><input type="radio" name="radio4" class="checkbox-radio"> Hoping for high returns, I accept high risks even at the risk of losing more than my invested capital.</label></li>                    	
                    </ul>
                </p>                
                <p class="infi-ul">4. Which statement defines your reaction, if your account loses a significant portion of its capital due to adverse market conditions?
                	<ul class="no-listing">
                        <li><label><input type="radio" name="radio5" class="checkbox-radio"> I would be concerned but I understand the risks involved in trading Forex and CFD products.</label></li>
                        <li><label><input type="radio" name="radio5" class="checkbox-radio"> I would not be concerned as I understand the risks involved in trading Forex and CFD products.</label></li>
                        <li><label><input type="radio" name="radio5" class="checkbox-radio"> I would be concerned and consider withdrawing my funds.</label></li>                    	
                    </ul>
                </p>  
                <p class="infi-ul">5. Please confirm the total time would you would like to hold your investments?
                	<ul class="no-listing">
                        <li><label><input type="radio" name="radio6" class="checkbox-radio"> <b>Short term</b> – I can’t invest or hold my investments for long and I would require my capital back quite quickly.</label></li>
                        <li><label><input type="radio" name="radio6" class="checkbox-radio"> <b>Medium term</b> – Although I do not need the money immediately I would not tie my funds up for very long.</label></li>
                        <li><label><input type="radio" name="radio6" class="checkbox-radio"> <b>Long term</b> – I can invest over a longer period and will not be requiring my funds in the near future.</label></li>                    	
                    </ul>
                </p>                 
                
                                
             </div>              
             
        </div>
         <div class="bottom-page-div">
        	<p class="bottom-page-number text-center">Page 2 of 3</p>
        </div>        
        
        
    
        

        
        
        <div class="page-break"></div>          
        
        
        
        <div class="page1-div same-padding">
             <p class="infi-small-title"><b><u>STEP 5: RISK WARNING</u></b></p>
            <div class="orange-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>RISK WARNING</u></b></p>
                <p class="infi-ul">Please read and ensure you understand the following risks of trading leveraged products such as Forex and CFDs:
					<ol class="infi-latin">
                    	<li>Leverage enables an investor to gain a large exposure in a product whilst using a small percentage of capital. Although this type of trading has its benefits, since your initial outlay may be a percentage of your capital, the downside is that losses can be magnified, at times exceeding any funds held. You should consider your investment as being the full value of as opposed to the amount of margin.</li>
                        <li>In fast moving markets, prices can change rapidly and additional margin may be required against your positions.</li>
                        <li>Where stop losses are used to minimise your downside, it is important to know that these are NOT guaranteed. Factors such as illiquid markets, slippage or the market gaps up or down, means your exit price could be significantly away from the intended stop loss price.</li>
                        <li>Gapping occurs when when a market’s price sharply rises or falls but no trading activity has taken place in that time. Gaps tend to occur more in markets with poor liquidity, over news events or when the market is closed.</li>
                        <li>CFD and Forex trading is not a suitable vehicle for long term income growth.</li>
                    </ol>
                </p>
             </div> 
             <p class="infi-small-title"><b><u>STEP 6: FINAL DECLARATION</u></b></p>
            <div class="darkgrey-box same-box-padding">
            	<p class="box-small-title text-center"><b><u>FINAL DECLARATION:</u></b></p>
                <p class="infi-ul">
					<ul class="ul-font-size">
                    	<li>I confirm and agree that the information I have provided is true and correct.</li>
                        <li>Further, I undertake to notify INFINOX CAPITAL if there is any change to my status, such that any of the statements in the declaration becomes untrue.</li>
                        <li>I understand the nature of trading CFDs and Forex, which are high risk products.</li>
                        <li>Please tick if you can confirm ALL of the above? <input class="clean checkbox-input" type="checkbox" required></li>
                    </ul>
                </p>
                <table class="infi-table">
                	<tr>
                    	<td class="first-tdd">Please print your name:</td>
                        <td><p class="underline100-p"></p></td>
                    </tr>
                	<tr>
                    	<td class="first-tdd">Signature:</td>
                        <td><p class="underline100-p"></p></td>
                    </tr>     
                	<tr>
                    	<td class="first-tdd">Date:</td>
                        <td><p class="underline100-p"></p></td>
                    </tr>                                     
                </table>
             </div>
             <div class="whitex-box same-box-padding">
             	<p class="infi-small-p">Infinox Capital is a registered trading name of IX Capital Group Limited, formerly Infinox Capital Bahamas Limited, which is authorised and regulated by the Securities Commission of The Bahamas.</p>
             
             </div>            
        </div>    
         <div class="bottom-page-div">
        	<p class="bottom-page-number text-center">Page 3 of 3</p>
        </div>        
        
        
      
        
</div>       
<img src="img/print.png" class="print-png opacity-hover"  onclick="window.print()" alt="Print" title="Print">
<?php include 'js.php'; ?>

</body>
</html>
