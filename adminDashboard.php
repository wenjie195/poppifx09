<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/PoolFund.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$poolFundDetails = getPoolFundDetails($conn);
$totalPoint = 0;
$id = 1;
include 'selectFilecss.php';
//
// require_once('vendor/php-excel-reader/excel_reader2.php');
// require_once('vendor/SpreadsheetReader.php');

// include 'dailyExcel.php';
// include 'monthlyExcel.php';


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://victory5.co/adminDashboard.php" />
    <link rel="canonical" href="https://victory5.co/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard  | Victory 5" />
    <title>Admin Dashboard  | Victory 5</title>

    <link rel="stylesheet" href="css/font-awesome.min.css">
	<?php include 'css.php'; ?>
</head>
<style media="screen">


td.tdh{
    text-align: center;
  }
  th.thh{
    text-align: center;
  }
  #Reset, #Reset2, #ResetMembers{
  background-color: maroon;
  }
  #Reset:hover, #ResetMembers:hover, #Reset2:hover{
  background-color: #650000;
  }
  .reset-checkbox, #nameCheckMembers, #nameCheck, #nameCheck2, .reset-checkbox2{
	color:white;
    cursor: pointer;
    text-align: center;

  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center" id="firefly">
  	<!-- <h1 class="pop-h1 text-center"><?php //echo _ADMINHEADER_COMPANY_BALANCE ?></h1> -->


<!-- </div> -->


    <!-- <div class="width100 text-center overflow margin-bottom50">
        <button class="clean blue-button one-button-width">
            <a href="addOnBonus.php" class="red-link">Add On Point</a>
        </button>
    </div>  -->
	<!-- <h1 class="pop-h1"><?php // echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></h1> -->

        <div class="width100 overflow margin-top50">
            <div class="profile-h3 text-center">
            <!--
<img src="img/daily-commission.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?>" title="<?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?>">-->
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?></b></p>
                <!-- <p class="smaller-p"><?php //echo _ADMINNEWCREDIT_SECOND_STEP ?></p> -->
                <div class="fake-upoad-spacing"></div>
                <div class="width100 text-center">
                  <button type="submit" id="Daily" name="import"  class="pill-shape clean blue-button one-button-width ow-purple-btn margin-top15 ow-width80">
                    <i id="loadingAnimate2" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName2"><?php echo _ADMINADDON_DAILY_BONUS ?></a>

                  </button>
                </div>
                <div class="clear"></div>
                <div class="width100 text-center">
                     <button style="display: none" type="submit" id="Reset" name="resetButton"  class="clean blue-button one-button-width ow-purple-btn margin-top16 ow-width80" value="<?php echo date('Y-m-d'); ?>">
                       <i id="loadingAnimateDelete" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonNameDelete"><?php echo _ADMINADDON_RESET ?></a>

                     </button>
                </div>
                <p><input class="reset-checkbox" type="checkbox" name="resetCheckbox" value=""><a id="nameCheck"><?php echo " "._ADMINDASHBOARD_RESET ?></a> </p>
            </div>
            <div class="profile-h3 mid-profile-h3 text-center mid-tempo-margin-top">
            	<!--
                <img src="img/monthly-commission.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?>" title="<?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?>">-->
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?></b></p>
                <form action="monthlyExcel.php" method="post" name="frmExcelImport" id="frmExcelImport2" enctype="multipart/form-data">
                    <div class="width80 margin-auto"><label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"></div><div class="clear"></div>
                    <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
                    <button type="submit" id="submit" name="import3"  class="clean blue-button one-button-width ow-width80"><?php echo _JS_SUBMIT ?></button>
                    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
                </form>
                 <div id="monthlyProfitBtn" class="width100 text-center">
                      <button type="submit" id="Monthly" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80">
                        <i id="loadingAnimate3" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName3"><?php echo _ADMINADDON_MONTHLY_BONUS ?></a>

                      </button>
                 </div>
                 <div class="width100 text-center">
                      <button style="display: none" type="submit" id="Reset2" name="resetButton"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80" value="<?php echo date('Y-m-d'); ?>">
                        <i id="loadingAnimateDelete2" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonNameDelete2"><?php echo _ADMINADDON_RESET ?></a>

                      </button>
                 </div>
                 <p><input class="reset-checkbox2" type="checkbox" name="resetCheckbox2" value=""><a id="nameCheck2"><?php echo " "._ADMINDASHBOARD_RESET_MONTH ?></a> </p>









            </div>


            <div class="profile-h3 text-center tempo-margin-top">
            <!--
<img src="img/member-ranking.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?>" title="<?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?>">-->
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?></b></p>
                <!-- <p class="smaller-p"><?php // echo _ADMINNEWCREDIT_FIRST_STEP ?></p> -->
                 <form action="dailyExcel.php" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
                    <div class="width80 margin-auto"><label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input  type="file" name="file" id="file" accept=".xls,.xlsx"></div><div class="clear"></div>
                    <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
                    <button type="submit" id="submit" name="import"  class="clean blue-button one-button-width ow-width80"><?php echo _JS_SUBMIT ?></button>
                    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
                </form>
                <div class="width100 text-center">
                  <button  type="submit" id="Rank" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80">
                    <i id="loadingAnimate" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName"><?php echo _ADMINADDON_RANK_UPDATED ?></a>

                  </button>
                </div>
                <div class="clear"></div>
                <div class="width100 text-center">
                     <button style="display: none" type="submit" id="ResetMembers" name="resetButton"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80" value="<?php echo date('Y-m-d'); ?>">
                       <i id="loadingAnimateDeleteMembers" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonNameDeleteMembers"><?php echo _ADMINADDON_RESET ?></a>

                     </button>
                </div>
                <p><input class="reset-checkboxMembers" type="checkbox" name="resetCheckbox" value=""><a id="nameCheckMembers"><?php echo " "._ADMINDASHBOARD_RESET_MEMBERS ?></a> </p>











            </div>
		</div>
        <div class="clear"></div>
        <div class="width100 overflow text-center">
    		<h1 class="pop-h1"><?php echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></h1>
         </div>
    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th class="thh"><?php echo "POOL FUND" ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_LAST_UPDATED ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($poolFundDetails)
                    {
                        for($cntA = 0;$cntA < count($poolFundDetails) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td class="tdh"><?php echo ($cntA+1)?></td>
                                <td class="tdh"><?php echo "$ ".number_format($poolFundDetails[$cntA]->getPoolFund(),2);?></td>
                                <td class="tdh"><?php echo date('d/m/Y h:i A',strtotime($poolFundDetails[$cntA]->getDateUpdated())); ?></td>
                                <td class="tdh"><form class="" action="editPoolFund.php" method="post">
                                  <input type="hidden" name="id" value="<?php echo $poolFundDetails[$cntA]->getId() ?>">
                                  <a class="blue-link"><button class="blue-btn clean" type="submit" name="editPoolFund"><?php echo _ADMINVIEWBALANCE_EDIT ?></button> </a>
                                </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>


        <?php if(isset($_GET['type']) && isset($_SESSION['messageType'])){
          $valueGet = $_GET['type'];
          $valueSession = $_SESSION['messageType'];
        }else {
          $valueGet = 0;
          $valueSession = 0;
        } ?>
        <input type="hidden" name="getValue" value="<?php echo $valueGet ?>">
        <input type="hidden" name="getValue2" value="<?php echo $valueGet ?>">
        <input type="hidden" name="getValueSession" value="<?php echo $valueSession ?>">






    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>
<?php if(isset($_GET['type']) && isset($_SESSION['messageType']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Update Company Balance.";
        }
        if($_GET['type'] == 10)
        {
            $messageType = "Successfully Upload Excel.";
        }
        if($_GET['type'] == 11)
        {
            $messageType = "Successfully Upload Excel.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>



</body>
</html>
