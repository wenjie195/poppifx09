<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userDetails = getUser($conn, "WHERE username != 'admin'");
$No = 0;

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminCapping.php" />
    <link rel="canonical" href="https://victory5.co/adminCapping.php" />
    <meta property="og:title" content="Capping Report | Victory 5" />
    <title>Capping Report | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: maroon;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
	<h1 class="small-h1-a text-center"><?php echo _MEMBERS_CAPPING_REPORTS ?> | <a href="adminCappingCompleted.php"><?php echo _MEMBERS_CAPPING_REPORTS_COMPLETED ?></a> </h1>
    <div class="width100 shipping-div2">

      <div class="search-big-div">
          <div class="fake-input-div overflow width100">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _ADMINVIEWBALANCE_NAME ?>" class="clean pop-input fake-input">
          </div>

          <!-- <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php //echo _MULTIBANK_SEARCH ?>" title="<?php //echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="fromInput" placeholder="<?php //echo _MULTIBANK_SEARCH ?> <?php //echo _DAILY_FROM ?>" class="clean pop-input fake-input">
          </div> -->

          <!-- <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php //echo _MULTIBANK_SEARCH ?>" title="<?php //echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="datex" placeholder="<?php //echo _MULTIBANK_SEARCH." "._DAILY_DATE ?>" value="<?php //echo date('d/m/y') ?>" class="clean pop-input fake-input">
          </div> -->
      </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <!-- <th><?php //echo _DAILY_FROM ?></th> -->
                        <th><?php echo _DAILY_BONUS ?></th>
                        <!-- <th><?php //echo _DAILY_DATE ?></th> -->
                        <!-- <th><?php //echo _DAILY_TIME ?></th> -->
                        <th><?php echo _MULTIBANK_DETAILS ?></th>
                    </tr>
                </thead>
                <tbody id="myTable">

                  <?php
                  if ($userDetails) {
                    for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
                      $uid = $userDetails[$cnt]->getUid();
                      $username = $userDetails[$cnt]->getUsername();
                      $No++;

                      $totalBonuss = 0;
                      $totalBonusMonthlyy = 0;
                      $capping = 500;
                      // $capping = number_format($cap,4);

                      $dailyBonusDetails = getDailyBonus($conn, "WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");
                      $monthlyBonusDetails = getMonthlyBonus($conn, "WHERE uid = ?", array("uid"), array($uid), "s");

                      if ($dailyBonusDetails) {
                        for ($i=0; $i <count($dailyBonusDetails) ; $i++) {
                          $bonus = $dailyBonusDetails[$i]->getBonus();
                          $bonusDateCreated = date('Y-m',strtotime($dailyBonusDetails[$i]->getDateCreated()));
                          // if (in_array($bonusDateCreated,$dateClaimArray)) {
                            // echo "Claim Already";
                          // }else {
                            $totalBonuss += $bonus;
                          // }
                        }
                      }

                      // $totalBonus = number_format($totalBonuss,4);  // daily spread

                      if ($monthlyBonusDetails) {
                        for ($i=0; $i <count($monthlyBonusDetails) ; $i++) {
                          $bonusMonthly = $monthlyBonusDetails[$i]->getBonus();
                          $bonusMonthlyDateCreated = date('Y-m',strtotime($monthlyBonusDetails[$i]->getDateCreated()));
                          // if (in_array($bonusMonthlyDateCreated,$dateClaimArray)) {
                            // echo "sssss";
                          // }else {
                          $totalBonusMonthlyy += $bonusMonthly;
                          // }
                        }
                      }
                      $totalComm = $totalBonuss + $totalBonusMonthlyy;
                      $totalCommission = number_format($totalComm,4);
                      // $totalBonusMonthly = number_format($totalBonusMonthlyy,4);  // monthly profit
                      if ($totalComm < $capping) {

                      ?>
                      <tr>
                        <td><?php echo $No ?></td>
                        <td><?php echo $username ?></td>
                        <td><?php echo "$ ".$totalCommission." / $ ".$capping ?></td>
                        <td>
                          <form class="" action="adminUserDaily.php" method="post">
                            <button class="clean blue-ow-btn" type="submit" name="viewMore" value="<?php echo $uid ?>"><?php echo _ADMINVIEWBALANCE_VIEW ?></button>
                          </form>
                        </td>
                      </tr>
                      <?php
                    }
                }
              }

                   ?>

                </tbody>
                <td colspan="7" class="ss" style="text-align: center;font-weight: bold;display: none"><?php echo _DAILY_NO_REPORT ?></td>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>
</body>
</html>
<script>
$(document).ready(function(){
  $("#datex").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );
  $("#usernameInput,#fromInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#datex").on("change", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

    if($("#myTable tr").is(":hidden")){
      $(".ss").show();
    }
    if($("#myTable tr").is(":visible")){
      $(".ss").hide();
    }
  });
  var value = $("#datex").val().toLowerCase();
  $("#myTable tr").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
  });
  if($("#myTable tr").is(":hidden")){
    $(".ss").show();
  }
  if($("#myTable tr").is(":visible")){
    $(".ss").hide();
  }
});
</script>
