<div id="wrapper" class="toggled-2">
    <div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
        <li>
            <a href="userDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
        </li>
        <li class="sidebar-li">
            <a href="profile.php" class="overflow">
                <img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _USERHEADER_PROFILE ?></p>
            </a>
        </li>
        <li class="sidebar-li">
            <a href="userBankDetails.php" class="overflow">
                <img src="img/bank-details.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _BANKDETAILS ?></p>
            </a>
        </li>
        <li class="sidebar-li">
            <a href="editPassword.php" class="overflow">
                <img src="img/password2.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _USERHEADER_EDIT_PASSWORD ?></p>
            </a>
        </li>  
        <li class="sidebar-li">
            <a href="uploadDoc.php" class="overflow">
                <img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _VICTORY_DOCUMENTS ?></p>
            </a>
        </li>     
        <li class="sidebar-li">
            <a href="lpoa.php" class="overflow">
                <img src="img/maa.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text">LPOA</p>
            </a>
        </li>       
        <li class="sidebar-li">
            <a href="refer.php" class="overflow">
                <img src="img/link.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _VICTORY_REFERRAL_LINK ?></p>
            </a>
        </li>     
        <li class="sidebar-li">
            <a href="hierarchy.php" class="overflow">
                <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _USERHEADER_HIERARCHY ?></p>
            </a>
        </li>      
        <li class="sidebar-li">
            <a href="userViewDaily.php" class="overflow">
                <img src="img/daily2.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></p>
            </a>
        </li>        
        <li class="sidebar-li">
            <a href="userViewMonthly.php" class="overflow">
                <img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></p>
            </a>
        </li>     
        <li class="sidebar-li">
            <a href="<?php $link ?>?lang=en" class="overflow">
                <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text">EN</p>
            </a>
        </li>    
        <li class="sidebar-li">
            <a href="<?php $link ?>?lang=ch" class="overflow">
                <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text">中文</p>
            </a>
        </li>      
        <li class="sidebar-li">
            <a href="logout.php" class="overflow">
                <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><?php echo _USERDASHBOARD_LOGOUT ?></p>
            </a>
        </li>       
            
    </ul>
    </div>
</div>