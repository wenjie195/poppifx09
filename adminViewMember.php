<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_type = 1");

// $conn->close();

// $userCnt = getUser($conn, "WHERE user_type = 1");

// $userCount = count($userCnt);

// if (isset($_GET['page']))
// {
// 	$page = $_GET['page'];
// 	// $limitMax = 50;
// 	$limitMax = 100;
// 	$startColumn = ($page - 1) * $limitMax;
// 	$userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
// }
// else
// {
// 	// $limitMax = 50;
// 	$limitMax = 100;
// 	$startColumn = 0;
// 	$userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
// }

// $startColumn += 1; // for table no column
// $pagination = $userCount / $limitMax;

// $conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/adminViewMember.php" />
    <link rel="canonical" href="https://victory5.co/adminViewMember.php" />
    <meta property="og:title" content="View Member | Victory 5" />
    <title>View Member | Victory 5</title>

	<?php include 'css.php'; ?>

</head>

<!-- <body> -->

<body class="body">
<?php include 'adminHeader.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

    <div class="width100 shipping-div2 margin-top15">

		<div class="search-big-div">
            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput3" onkeyup="myFunctionC()" placeholder="<?php echo _MULTIBANK_SEARCH ?> MT4" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
            </div>

						<div class="fake-input-div overflow profile-h3">
								<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
								<input autocomplete="off" type="text" id="dateStart" placeholder="<?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input">
						</div>

						<div class="fake-input-div overflow profile-h3">
								<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
								<input autocomplete="off" type="text" id="dateEnd" placeholder="<?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input">
						</div>
        </div>

		<div class="overflow-scroll-div">
			<table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
				<thead>
					<tr>
						<!-- <img src="img/sort.png" class="sort-img"> -->
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _JS_USERNAME ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _JS_FULLNAME ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th">MT4 ID <img src="img/sort.png" class="sort-img"></th>
						<th class="th">MT4 ID 2nd<img src="img/sort.png" class="sort-img"></th>
						<th class="th">MT4 ID 3rd<img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _JS_PHONE ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _JS_EMAIL ?> <img src="img/sort.png" class="sort-img"></th>
            <!-- <th class="th"><?php //echo _USERDASHBOARD_PERSONAL_SALES ?> ($) </th> -->
            <!-- <th class="th"><?php //echo _USERDASHBOARD_GROUP_SALES ?> ($) </th> -->
            <!-- <th class="th"><?php //echo _USERDASHBOARD_DIRECT_DOWNLINE ?> </th> -->
            <!-- <th class="th"><?php //echo _USERDASHBOARD_GROUP_MEMBER ?> </th> -->
            <th class="th"><?php echo _MULTIBANK_ID_DOCUMENT ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _MULTIBANK_UTILITY_BILL ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th">LPOA <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _ADMINVIEWBALANCE_ACTION ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _MULTIBANK_DETAILS ?> <img src="img/sort.png" class="sort-img"></th>

					</tr>
				</thead>
				<tbody id="myFilter">

					<?php
					if($userDetails)
					{
						for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
						{
							//
							// $mpIdData = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
							// $directDownline = 0;
							// $groupSales = 0;
							// $personalSales = 0;
							// $personalSalesUser = 0;
							// $groupMember = 0;
							// $referrerDetails = getWholeDownlineTree($conn, $userDetails[$cnt]->getUid(), false);
							// if ($mpIdData) {
							  // $personalSalesUser = $mpIdData[0]->getBalance();
							// }
							// $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($userDetails[$cnt]->getUid()), "s");
							// $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
							// $directDownlineLevel = $referralCurrentLevel + 1;
							// if ($referrerDetails)
							// {
							//   $groupMember = count($referrerDetails);
							//   for ($i=0; $i <count($referrerDetails) ; $i++)
							//   {
							// 	$currentLevel = $referrerDetails[$i]->getCurrentLevel();
							// 	if ($currentLevel == $directDownlineLevel)
							// 	{
							// 	  $directDownline++;
							// 	}
							// 	$referralId = $referrerDetails[$i]->getReferralId();
							// 	$downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
							// 	if ($downlineDetails)
							// 	{
							// 	  $personalSales = $downlineDetails[0]->getBalance();
							// 	  $groupSales += $personalSales;
							// 	  $groupSalesFormat = number_format($groupSales,4);
							// 	}
							//   }
							// }

						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $userDetails[$cnt]->getUsername();?></td>
								<td><?php echo $userDetails[$cnt]->getLastname()." ".$userDetails[$cnt]->getFirstname();?></td>
								<?php $mpIdData = getMpIdData($conn, "WHERE uid =?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
									if (isset($mpIdData[0])) {
										?>
										<td><?php echo $mpIdData[0]->getMpId(); ?></td>
										<?php
									}else {
										?>
										<td>-</td>
										<?php
									}
									if (isset($mpIdData[1])) {
										?>
										<td><?php echo $mpIdData[1]->getMpId(); ?></td>
										<?php
									}else {
										?>
										<td>-</td>
										<?php
									}
									if (isset($mpIdData[2])) {
										?>
										<td><?php echo $mpIdData[2]->getMpId(); ?></td>
										<?php
									}else {
										?>
										<td>-</td>
										<?php
									}
								 ?>
								<td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
								<td><?php echo $userDetails[$cnt]->getEmail();?></td>
                <!-- <td><?php //echo number_format($personalSalesUser,2) ?></td>
                <td><?php //echo number_format($groupSales,2) ?></td> -->

                <!-- <td><?php //echo $personalSalesUser ;?></td> -->
                <!-- <td><?php //echo $groupSales ;?></td> -->

                <!-- <td><?php //echo  $directDownline ?></td> -->
								<!-- <td><?php //echo  $groupMember ?></td> -->

								<td>
                                    <?php $idDoc = $userDetails[$cnt]->getIcBack();
                                    if($idDoc == '2')
                                    {
                                    ?>
                                        <form action="adminViewIDDoc.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($idDoc == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $utiBill = $userDetails[$cnt]->getLicense();
                                    if($utiBill == '2')
                                    {
                                    ?>
                                        <form action="adminViewBills.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($utiBill == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $sform = $userDetails[$cnt]->getSignature();
                                    if($sform == '2')
                                    {
                                    ?>
                                        <form action="adminViewSignature.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($sform == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <form action="adminUpdateCustomerDetails.php" method="POST">
                                        <button class="clean blue-ow-btn" type="submit" name="customer_id" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <?php echo _MULTIBANK_UPDATE ?>
                                        </button>
                                    </form>
                                </td>
																<td>
								                  <form action="adminViewMemberDetails.php" method="POST">
								                      <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
								                          <?php echo _MULTIBANK_VIEW ?>
								                      </button>
								                  </form>
								                </td>

							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- <div class="width100 same-padding footer-div">
	<p class="footer-p white-text"><?php //echo _JS_FOOTER ?></p>
</div> -->
<?php include 'js.php'; ?>
<!-- </div> -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
<script src="js/headroom.js"></script>
<script>
	$("#dateStart,#dateEnd").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );

	$("#dateStart,#dateEnd").on("change", function() {
		$("#myFilter").empty();
		$("#myFilter").append('<tr><td style="text-align: center" colspan="14"><i id="loadingAnimate" class="fa fa-spinner fa-spin margin-right12"></i>Loading...</td></tr>');
		var value3 = $("#dateStart").val();
		var value4 = $("#dateEnd").val();
		$.ajax({
			url: 'totalMember.php',
			data: {dateStart:value3,dateEnd:value4},
			type: 'post',
			dataType: 'json',
			success:function(response){
				var len = response.length;
				$("#myFilter").empty();

				for( var m = 0; m<len; m++){
					var no = m + 1;
					var uid = response[m]['uid'];
					var username = response[m]['username'];
					var fullname = response[m]['fullname'];
					var mt4id = response[m]['mt4id'];
					var phoneNo = response[m]['phoneNo'];
					var email = response[m]['email'];
					// var personalSalesUser = response[m]['personalSalesUser'];
					// var groupSales = response[m]['groupSales'];
					// var directDownline = response[m]['directDownline'];
					// var groupMember = response[m]['groupMember'];
					var idDoc = response[m]['idDoc'];
					var utiBill = response[m]['utiBill'];
					var sForm = response[m]['sForm'];
					var action = '<form action="adminUpdateCustomerDetails.php" method="POST"><button class="clean blue-ow-btn" type="submit" name="customer_id" value="'+uid+'"><?php echo _MULTIBANK_UPDATE ?></button></form>';

					if (idDoc == 2) {
						idDoc = '<form action="adminViewIDDoc.php" method="POST"><button class="clean blue-ow-btn" type="submit" name="user_uid" value="'+uid+'"><?php echo _MULTIBANK_VIEW ?></button></form>';
					}else if(idDoc == 1){
						idDoc = 'NO';
					}

					if (utiBill == 2) {
						utiBill = '<form action="adminViewBills.php" method="POST"><button class="clean blue-ow-btn" type="submit" name="user_uid" value="'+uid+'"><?php echo _MULTIBANK_VIEW ?></button></form>';
					}else if (utiBill == 1) {
						utiBill = 'NO';
					}

					if (sForm == 2) {
						sForm = '<form action="adminViewSignature.php" method="POST"><button class="clean blue-ow-btn" type="submit" name="user_uid" value="'+uid+'"><?php echo _MULTIBANK_VIEW ?></button></form>';
					}else if (sForm == 1) {
						sForm = 'NO';
					}
$("#myFilter").append('<tr><td>'+no+'</td><td>'+username+'</td><td>'+fullname+'</td><td>'+mt4id+'</td><td>'+phoneNo+'</td><td>'+email+'</td><td>'+idDoc+'</td><td>'+utiBill+'</td><td>'+sForm+'</td><td>'+action+'</td></tr>');
					// $("#myFilter").append('<tr><td>'+no+'</td><td>'+username+'</td><td>'+fullname+'</td><td>'+mt4id+'</td><td>'+phoneNo+'</td><td>'+email+'</td><td>'+personalSalesUser+'</td><td>'+groupSales+'</td><td>'+directDownline+'</td><td>'+groupMember+'</td><td>'+idDoc+'</td><td>'+utiBill+'</td><td>'+sForm+'</td><td>'+action+'</td></tr>');
				}
			},
			error:function(response){
				$("#myFilter").empty();
				$("#myFilter").append('<tr><td style="text-align: center" colspan="14">No Members Found </td></tr>');
			}
		});
	});
</script>

<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Update User Profile.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Successfully Change User Password.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Successfully Update User Bank Details.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>
</body>
</html>
