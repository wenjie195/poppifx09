<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--
    <meta property="og:url" content="https://poppifx4u.com/refer.php" />
    <link rel="canonical" href="https://poppifx4u.com/refer.php" />-->
    <meta property="og:title" content="Refer  | Victory 5" />
    <title>Refer  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height text-center">
	<h1 class="pop-h1"><?php echo _USERHEADER_REFER ?></h1>

        <div class="spacing-div"></div>
            <div class="width100 text-center">
               <img src="img/invitation.png" class="invitation-img" alt="<?php echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php echo _USERDASHBOARD_INVITATION_LINK ?>">
            </div>
            <div class="clear"></div>
            <?php $actual_link = "https://$_SERVER[HTTP_HOST]"; ?>
            <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?>">
            <h3 class="invite-h3"><b><?php echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="<?php echo "/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a black-link-underline"><?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?></a></h3>
            <button class="invite-btn blue-button" id="copy-referral-link"><?php echo _USERDASHBOARD_COPY ?></button>


        <div class="clear"></div>

</div>
<?php include 'js.php'; ?>
</body>
</html>
