<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$dateCreated = date('Y-m-d');
$historyUidReport = '';
$totalBonus = 0;

$userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array("$uid"),"s");
$oldMembers = $userDetails[0]->getRankLoop();
$oldMembersUsername = $userDetails[0]->getUsername();

// $dailyBonusDetails = getDailyBonus($conn, " WHERE uid = ? AND display = 1 AND date_created >= ?", array("uid,date_created"), array($uid,$dateCreated), "ss");
$dailyBonusDetails = getDailyBonus($conn, " WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");

$monthlyProBon = getMonProBon($conn);

$cntAA = 1;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userViewDaily.php" />
    <link rel="canonical" href="https://victory5.co/userViewDaily.php" />
    <meta property="og:title" content="Daily Spread Report  | Victory 5" />
    <title>Daily Spread Report  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/daily.png" class="middle-title-icon" alt="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>" title="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>">
    </div>  
    <div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></h1>
    </div>
    <div class="width100 shipping-div2">

      <div class="search-big-div">
          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _ADMINVIEWBALANCE_NAME ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="fromInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _DAILY_FROM ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateStart" placeholder="<?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateEnd" placeholder="<?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input">
          </div>
      </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _DAILY_FROM ?></th>
                        <th><?php echo _DAILY_BONUS ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_VIEW ?></th> -->
                    </tr>
                </thead>
                <tbody id="myTable">

                    <?php
                    if($dailyBonusDetails)
                    {
                        for($cnt = 0;$cnt < count($dailyBonusDetails) ;$cnt++)
                        {
                          if ($dailyBonusDetails[$cnt]->getBonus()) {
                        ?>
                            <tr>
                                <td><?php echo $cntAA++; ?></td>
                                <td><?php echo $dailyBonusDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $dailyBonusDetails[$cnt]->getFromWho();?></td>
                                <td><?php echo "$ ".number_format($dailyBonusDetails[$cnt]->getBonus(),2);?></td>
                                <td><?php echo date('d/m/Y',strtotime($dailyBonusDetails[$cnt]->getDateCreated())) ?></td>
                                <td><?php echo date('h:i a',strtotime($dailyBonusDetails[$cnt]->getDateCreated())) ?></td>
                                <!-- <td><a href="#" class="blue-link"><?php //echo _ADMINVIEWBALANCE_VIEW ?></a></td> -->
                            </tr>
                        <?php
                      }}
                        ?>
                    <?php
                  }else {
                    ?><td colspan="7" style="text-align: center;font-weight: bold"><?php echo _DAILY_NO_REPORT ?></td> <?php
                  }
                    ?>

                </tbody>
                <td colspan="7" class="ss" style="text-align: center;font-weight: bold;display: none"><?php echo _DAILY_NO_REPORT ?></td>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
<script>
$(document).ready(function(){
  $("#dateStart,#dateEnd").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );
  $("#usernameInput,#fromInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#dateStart,#dateEnd").on("change", function() {
    $("#myTable").empty();
    var value3 = $("#dateStart").val();
    var value4 = $("#dateEnd").val();
    // alert(value3);
    // $("#myTable tr").filter(function() {
    //   $(this).toggle($(this).text().toLowerCase().indexOf(value3) > -1)
    // });
    $.ajax({
      url: 'totalSpread.php',
      data: {dateStart:value3,dateEnd:value4},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var len = response.length;
        $("#myTable").empty();

        for( var m = 0; m<len; m++){
          var no = m + 1;
          var date = response[m]['date'];
          var username = response[m]['username'];
          var from = response[m]['from'];
          var bonus = response[m]['bonus'];
          var dateCreated = response[m]['dateCreated'];
          var timeCreated = response[m]['timeCreated'];

          // alert(username);

          $("#myTable").append('<tr><td>'+no+'</td><td>'+username+'</td><td>'+from+'</td><td>$ '+bonus+'</td><td>'+dateCreated+'</td><td>'+timeCreated+'</td></tr>');
        }
      },
      error:function(response){
        $("#myTable").empty();
        $("#myTable").append('<tr><td style="text-align: center" colspan="7"><?php echo _DAILY_NO_REPORT ?></td></tr>');
      }
    });
    // $(".total-payout").empty();
    // $(".total-payout").text("sss");
  });
});
</script>
