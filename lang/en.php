<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_MAINJS_INDEX_CONFIRM_PASSWORD", "Confirm Password");
define("_INDEX_NO_YET", "Don't have an account? Sign up here.");
define("_INDEX_LOGIN_NOW", "Already have an account? Log in here.");
define("_INDEX_IC_NO", "IC Number");
define("_INDEX_DOB", "Date of Birth");
define("_INDEX_MOBILE_NO", "Contact No.");
define("_INDEX_ADDRESS", "Address");
define("_INDEX_ADDRESS1", "Address 1");
define("_INDEX_ADDRESS2", "Address 2");
define("_INDEX_ZIPCODE", "Zip Code");
define("_INDEX_STATE", "State");
define("_INDEX_REGISTER", "Register");
define("_INDEX_NO_REPEAT_IC", "*Do Not Use REPEATED IC");
//JS
define("_JS_FOOTER", " Victory 5, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_NEW_PASSWORD", "Retype New Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Confirm");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_JS_SUCCESS", "Success");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
define("_JS_EDIT_USER_PROFILE", "Change Member Details");
define("_JS_CHANGE_USER_PASSWORD", "Change Member Password");
define("_JS_EDIT_PROFILE", "Change Personal Details");
define("_JS_RESET_PASSWORD", "Reset Password");
define("_JS_VERIFY_CODE", "Verify Code");
define("_JS_SELECT_COUNTRY", "Please Select a Country");
//UPLOAD
define("_UPLOAD_IC_FRONT", "Upload Front View of IC/Passport");
define("_UPLOAD_PROOF_OF_LEGAL_NAME", "Proof of Legal Name (Front)");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_DESC", "*ID document or Passport to show your full name, date of birth, ID number and expiry date");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_BACK", "Proof of Legal Name (Back)");
define("_UPLOAD_PROOF_OF_ADDRESS", "Proof of Address");
define("_UPLOAD_PROOF_OF_ADDRESS_DESC", "*Utility bill, Driving License or other documents to show your name and address");
define("_UPLOAD", "Upload");
define("_UPLOAD_PREVIEW", "Preview");
define("_UPLOAD_SELECT_DRAG", "Select a file or drag here");
define("_UPLOAD_PLS_SELECT_IMG", "Please select an image");
define("_UPLOAD_SELECT_A_FILE", "Select a file");
define("_UPLOAD_IC_BACK", "Upload Back View of IC/Passport");
define("_UPLOAD_UTILITY_BILL_DRIVING_LICENSE", "Upload Utility Bill or Driving License");
define("_UPLOAD_SIGNATURE_MAA", "Attach your signature for Limited Power of Attorney (LPOA)");
define("_UPLOAD_SIGNATURE", "Upload Signature");
define("_UPLOAD_CHOOSE_FILE_HERE", "Click Here to Choose File");
//USERDASHBOARD
define("_USERDASHBOARD_INVITATION_LINK", "Invitation Link");
define("_USERDASHBOARD_COPY", "Copy");
define("_USERDASHBOARD_LOGOUT", "Logout");
define("_USERDASHBOARD_INVITED_BY_ME", "Members Invited by Me");
define("_USERHEADER_PROFILE", "Personal Details");
define("_USERHEADER_BANK_ACC", "Bank Account");
define("_USERHEADER_UPLOAD_DOC", "Upload Doc");
define("_USERHEADER_REFER", "Refer");
define("_USERHEADER_HIERARCHY", "Organization");
define("RESET_HIERARCHY", "Reset");
define("_USERDASHBOARD_TOTAL_COMMISSION", "Total Commission");
define("_USERDASHBOARD_DAILY_COMMISSION", "Daily Profit");
define("_USERDASHBOARD_MONTHLY_COMMISSION", "Monthly Profit");
define("_USERDASHBOARD_PERSONAL_SALES", "My Sales");
define("_USERDASHBOARD_GROUP_SALES", "Team Sales");
define("_USERDASHBOARD_DIRECT_DOWNLINE", "Direct Downline");
define("_USERDASHBOARD_DIRECT_DOWNLINE2", "'s Direct Downline");
define("_USERDASHBOARD_GROUP_MEMBER", "Team Member");
define("_USERDASHBOARD_RANK", "Rank");
define("_USERHEADER_BANK_DETAILS", "Bank Details");
define("_USERDASHBOARD_DETAILS", "Details");
//BANKDETAILS
define("_BANKDETAILS_ACC_NAME", "Bank Acc. Holder Name");
define("_BANKDETAILS_ACC_NO", "Bank Account Number");
define("_BANKDETAILS_ACC_TYPE", "Bank Account Type");
define("_BANKDETAILS_BANK", "Bank");
define("_BANKDETAILS_BANK_SWIFT_CODE", "Bank SWIFT Code");
define("_BANKDETAILS", "Bank Details");
//ADMINDASHBOARD
define("_ADMINDASHBOARD_ADD_ONS_CREDIT", "Add-ons Credit");
define("_ADMINDASHBOARD_UPLOAD_EQUITY_PL", "Upload Equity PL");
define("_ADMINDASHBOARD_PROFIT_SHARING", "Profit Sharing");
define("_ADMINDASHBOARD_RESET_MEMBERS", "Reset Members Balance?");
define("_ADMINDASHBOARD_RESET", "Reset Daily Profit?");
define("_ADMINDASHBOARD_RESET_MONTH", "Reset Monthly profit?");
//ADMINHEADER
define("_ADMINHEADER_DASHBOARD", "Dashboard");
define("_ADMINHEADER_INFO", "Statements");
define("_ADMINHEADER_MARKETING_PLAN", "Marketing Plan");
define("_ADMINHEADER_DAILY_SPREAD", "Daily Profit");
define("_ADMINHEADER_MONTHLY_PROFITS", "Monthly Profits");
define("_ADMINHEADER_MEMBERS", "Members");
define("_ADMINHEADER_REPORTS", "Statements");
define("_ADMINHEADER_MEMBER_BALANCE", "Member's Balance");
define("_ADMINHEADER_MEMBER_WITHDRAW", "Member's Withdrawal");
define("_ADMINHEADER_MEMBER_WITHDRAW_HISTORY", "Member's Withdrawal History");
define("_ADMINHEADER_MEMBER_PROFIT", "Member's Profit Sharing");
define("_ADMINHEADER_COMPANY_BALANCE", "Company's Balance");
define("_ADMINHEADER_ADD_USER", "Add Member");
define("_ADMINHEADER_VIEW_MEMBER", "View Member");
define("_ADMINHEADER_DAILY_BONUS", "Daily Profit");
define("_ADMINHEADER_MONTHLY_BONUS", "Monthly Profits");
define("_ADMINHEADER_VIEW_LEVEL", "View Level");
define("_ADMINHEADER_EDIT_PASSWORD", "Change Password");
define("_ADMINHEADER_MEMBER_PROFIT2", "Member's Profit");
//ADMINNEWCREDIT
define("_ADMINNEWCREDIT_SELECT_FILE", "Select File");
define("_ADMINNEWCREDIT_MEMBERS_RANKING", "Members Ranking");
define("_ADMINNEWCREDIT_FIRST_STEP", "1st Step");
define("_ADMINNEWCREDIT_DAILY_SPREAD", "Rank & Same Level Reward");
define("_ADMINNEWCREDIT_SECOND_STEP", "2nd Step");
define("_ADMINNEWCREDIT_MONTHLY_PROFITS", "Tier Reward");
//ADMINVIEWBALANCE
define("_ADMINVIEWBALANCE_NO", "No.");
define("_ADMINVIEWBALANCE_NAME", "Name");
define("_ADMINVIEWBALANCE_BALANCE", "Balance");
define("_ADMINVIEWBALANCE_EDIT", "Edit");
define("_ADMINVIEWBALANCE_VIEW", "View");
define("_ADMINVIEWBALANCE_COMPANY_TOTAL", "Company Total Balance (CTB)");
define("_ADMINVIEWBALANCE_TOTAL_LOT_SIZE", "Total Lot Size (TLS)");
define("_ADMINVIEWBALANCE_LAST_UPDATED", "Last Updated");
define("_ADMINVIEWBALANCE_ACTION", "Action");
define("_ADMINVIEWBALANCE_ISSUE", "Issue");
define("_ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE", "Edit Company Balance");
define("_ADMINVIEWBALANCE_PAYOUT", "Total Payout :");
define("_ADMINVIEWBALANCE_PAYOUT_RELEASED", "Total Payout Released :");
define("_ADMINVIEWBALANCE_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_ADMINVIEWBALANCE_PAYOUT_UNRELEASED", "Total Payout Unreleased :");
define("_ADMINVIEWBALANCE_APPROVE", "Approve");
define("_ADMINVIEWBALANCE_REJECT", "Reject");
//MULTIBANK
define("_MULTIBANK_PRINT", "Print");
define("_MULTIBANK_VIEW", "View");
define("_MULTIBANK_ID_DOCUMENT", "ID");
define("_MULTIBANK_UTILITY_BILL", "Utility Bill");
define("_MULTIBANK_ACTION", "Action");
define("_MULTIBANK_SEARCH", "Search");
define("_MULTIBANK_UPDATE", "Update");
define("_MULTIBANK_GO_BACK", "Go Back");
define("_MULTIBANK_IC_FRONT", "Front View of IC");
define("_MULTIBANK_IC_BACK", "Back View of IC");
define("_MULTIBANK_DETAILS", "Details");
define("_MULTIBANK_UPLINE", "Upline");
define("_MULTIBANK_PERFORMANCE_FEE", "Performance Fee");
define("_MULTIBANK_START_DATE", "Start Date");
define("_MULTIBANK_END_DATE", "End Date");
//USERHEADER
define("_USERHEADER_EDIT_PROFILE", "Change Personal Details");
define("_USERHEADER_EDIT_PASSWORD", "Change Password");
define("USERHEADER_DAILY_BONUS", "Daily Profit Statement");
define("USERHEADER_MONTHLY_BONUS", "Monthly Bonus Statement");
define("_USERHEADER_ADD_NEW", "Add New MT4ID");
//HIERARCHY
define("_HIERARCHY_GENERATION", "Generation");
define("_HIERARCHY_NAME", "Name");
define("_HIERARCHY_SPONSOR", "Sponsor");
define("_HIERARCHY_PERSONAL_SALES", "My Sales");
define("_HIERARCHY_GROUP_SALES", "Team Sales");
define("_HIERARCHY_GROUP_MEMBER", "Team Member");
define("_HIERARCHY_DIRECT_DOWNLINE", "Direct Downline");
define("_HIERARCHY_TOTAL_DOWNLINE", "Total Downline");
define("_HIERARCHY_RANK", "Rank");
define("_HIERARCHY_TREE_VIEW", "Tree View");
//REFER
define("_REFER_INVITATION_LINK", "Copy Invitation Link");
//DAILY BONUS
define("_DAILY_MEMBER_DAILY_BONUS", "Rank & Same Level Reward Statement");
define("_DAILY_FROM", "From");
define("_DAILY_BONUS", "Bonus");
define("_DAILY_DATE", "Date");
define("_DAILY_TIME", "Time");
define("_DAILY_NO_REPORT", "No Daily Profit Statement");
define("_DAILY_RELEASED_BONUS", "Released Bonus");
define("_DAILY_UNRELEASED_BONUS", "Unreleased Bonus");
//MONTHLY BONUS
define("_MONTHLY_MEMBER_DAILY_BONUS", "Tier Reward Statement");
define("_MONTHLY_LEVEL", "Level");
define("_MONTHLY_NO_REPORT", "No Monthly Profit Statement");
//LEVEL DETAILS
define("_VIEWLEVEL_LEVEL_DETAILS", "Level Details");
define("_VIEWLEVEL_AMOUNT", "Amount");
define("_VIEWLEVEL_REQUIREMENTS", "Requirements");
define("_VIEWLEVEL_DIRECT_SPONSOR", "Direct Sponsor");
define("_VIEWLEVEL_SELF_INVEST", "Self-Invest");
define("_VIEWLEVEL_PROFIT_SHARING", "Profit-Sharing");
//ADMIN ADD ON CREDIT
define("_ADMINADDON_RANK_UPDATED", "Rank Updated");
define("_ADMINADDON_DAILY_BONUS", "Calculate Reward");
define("_ADMINADDON_MONTHLY_BONUS", "Calculate Reward");
define("_ADMINADDON_RESET", "Reset");
//ADMIN DAILY SPREAD
define("_ADMINDAILY_RANKING", "Ranking");
define("_ADMINDAILY_TEAM_PERFORMANCE", "Team Performance");
define("_ADMINDAILY_SPREAD_SHARING", "Spread Sharing");
define("_ADMINDAILY_MEMBERS", "Members");
define("_ADMINDAILY_LEADER", "Leader");
define("_ADMINDAILY_DIRECT", "Direct Sponsor 3 Members");
define("_ADMINDAILY_TEAM_LEADER", "Team Leader");
define("_ADMINDAILY_INDIRECT_DIRECT", "Indirect + Direct Sponsor 2 Leaders");
define("_ADMINDAILY_GROUP_LEADER", "Team Leader");
define("_ADMINDAILY_INDIRECT_DIRECT_TEAM", "Indirect + Direct Sponsor 2 Team Leaders");
define("_ADMINDAILY_GROUP_LEADER_OVERRIDING", "Team Leader Overriding");
define("_MULTIBANK_SEARCH_START", "Start");
define("_MULTIBANK_SEARCH_END", "End");
// CAPPING
define("_MEMBERS_CAPPING_REPORTS", "Capping Statement");
define("_MEMBERS_CAPPING_REPORTS_COMPLETED", "Completed Capping Statement");
define("_NO_CAPPING_REPORT", "No Capping Statement");
define("_BONUS_TYPE", "Bonus Type");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_NO_MEMBER", "No New Member's Withdrawal");
//VICTORY
define("_VICTORY_DOCUMENTS", "Documents");
define("_VICTORY_URL", "Referral Link");
define("_VICTORY_Copy", "Copy");
define("_VICTORY_REFER", "Refer a Friend");
define("_VICTORY_MY_REFER", "My Referral");
define("_VICTORY_REFERRAL_LINK", "Referral Link");
//broker link
define("_BROKER_LINK", "Broker Link");