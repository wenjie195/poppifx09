<!--
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
               
                <a href="userDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right admin-right-menu">
            	
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <?php echo _USERHEADER_PROFILE ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>">

                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_PROFILE ?></a></p>
                        <p class="dropdown-p"><a href="userBankDetails.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _BANKDETAILS ?></a></p>

                   		<p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_EDIT_PASSWORD ?></a></p>

                        <p class="dropdown-p"><a href="uploadDoc.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _VICTORY_DOCUMENTS ?></a></p>
                        <p class="dropdown-p"><a href="maa.php"  class="menu-padding dropdown-a black-menu-item menu-a">MAA</a></p>
                	</div>
                </div>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <?php echo _VICTORY_MY_REFER ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _VICTORY_MY_REFER ?>" title="<?php echo _VICTORY_MY_REFER ?>">

                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="refer.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _VICTORY_REFERRAL_LINK ?></a></p>
                        <p class="dropdown-p"><a href="hierarchy.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_HIERARCHY ?></a></p>
                	</div>
                </div>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <?php echo _ADMINHEADER_INFO ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_INFO ?>" title="<?php echo _ADMINHEADER_INFO ?>">

                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="userViewDaily.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></a></p>
                        <p class="dropdown-p"><a href="userViewMonthly.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></a></p>
                        <p class="dropdown-p"><a href="userCapping.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></p>

                	</div>
                </div>

                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言">
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="Language / 语言" title="Language / 语言">


                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">EN</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>

                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                 
                                  <li class="title-li unclickable-li"><?php echo _USERHEADER_PROFILE ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>"></li>
                                  <li><a href="profile.php" class="mini-li"><?php echo _USERHEADER_PROFILE ?></a></li>
                                  <li><a href="userBankDetails.php" class="mini-li"><?php echo _USERHEADER_BANK_ACC ?></a></li>
                                  <li><a href="editPassword.php" class="mini-li"><?php echo _USERHEADER_EDIT_PASSWORD ?></a></li>
                                  
                                  <li><a href="uploadDoc.php" class="mini-li"><?php echo _VICTORY_DOCUMENTS ?></a></li>
                                  <li><a href="maa.php" class="mini-li">MAA</a></li>
								  <li class="title-li unclickable-li"><?php echo _VICTORY_MY_REFER ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _VICTORY_MY_REFER ?>" title="<?php echo _VICTORY_MY_REFER ?>"></li>
                                  <li><a href="refer.php" class="mini-li"><?php echo _VICTORY_REFERRAL_LINK ?></a></li>
                                  <li><a href="hierarchy.php" class="mini-li"><?php echo _USERHEADER_HIERARCHY ?></a></li>

                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_INFO ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_INFO ?>" title="<?php echo _ADMINHEADER_INFO ?>"></li>
                                                  <li><a href="userViewDaily.php" class="mini-li"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></a></li>
                                                  <li><a href="userViewMonthly.php" class="mini-li"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></a></li>
                                                  <li><a href="userCapping.php" class="mini-li"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></li>

                                 
                                  <li class="title-li unclickable-li"><img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言"> <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">EN</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div>
            </div>







        </div>

</header>
-->
<div id="wrapper" class="toggled-2">

<div id="sidebar-wrapper">
<ul class="sidebar-nav nav-pills nav-stacked" id="menu">
	<li>
    	 <a href="userDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
    </li>
    <li class="sidebar-li">
        <a href="profile.php" class="overflow">
        	<img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _USERHEADER_PROFILE ?></p>
        </a>
    </li>
    <li class="sidebar-li">
        <a href="userBankDetails.php" class="overflow">
        	<img src="img/bank-details.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _BANKDETAILS ?></p>
        </a>
    </li>
    <li class="sidebar-li">
        <a href="editPassword.php" class="overflow">
        	<img src="img/password2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _USERHEADER_EDIT_PASSWORD ?></p>
        </a>
    </li>  
    <li class="sidebar-li">
        <a href="uploadDoc.php" class="overflow">
        	<img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _VICTORY_DOCUMENTS ?></p>
        </a>
    </li>     
    <li class="sidebar-li">
        <a href="maa.php" class="overflow">
        	<img src="img/maa.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text">LPOA</p>
        </a>
    </li>       
    <li class="sidebar-li">
        <a href="refer.php" class="overflow">
        	<img src="img/link.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _VICTORY_REFERRAL_LINK ?></p>
        </a>
    </li>     
    <li class="sidebar-li">
        <a href="hierarchy.php" class="overflow">
        	<img src="img/group.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _USERHEADER_HIERARCHY ?></p>
        </a>
    </li>      
    <li class="sidebar-li">
        <a href="userViewDaily.php" class="overflow">
        	<img src="img/daily2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></p>
        </a>
    </li>        
    <li class="sidebar-li">
        <a href="userViewMonthly.php" class="overflow">
        	<img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></p>
        </a>
    </li>     
    <li class="sidebar-li">
        <a href="userCapping.php" class="overflow">
        	<img src="img/capping2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _MEMBERS_CAPPING_REPORTS ?></p>
        </a>
    </li>
    <li class="sidebar-li">
        <a href="<?php $link ?>?lang=en" class="overflow">
        	<img src="img/english.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text">EN</p>
        </a>
    </li>    
    <li class="sidebar-li">
        <a href="<?php $link ?>?lang=ch" class="overflow">
        	<img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text">中文</p>
        </a>
    </li>      
    <li class="sidebar-li">
        <a href="logout.php" class="overflow">
        	<img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _USERDASHBOARD_LOGOUT ?></p>
        </a>
    </li>       
        
</ul>
</div>



</div>



<!--
<div class="menu-distance sidebar1">
	<div class="sidebar-icon-div">
		<a><img src="img/dashboard.png" alt="" title=""></a>
    </div>
    <div class="sidebar-text-div">
    	<a>Dashboard</a>
    </div>



</div>-->
