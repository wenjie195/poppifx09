<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$companyBalance = getCompanyBalance($conn);
$totalPoint = 0;
$id = 1;
include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

include 'dailyExcel.php';
include 'monthlyExcel.php';


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminAddOnCreditOld.php" />
    <link rel="canonical" href="https://victory5.co/adminAddOnCreditOld.php" />
    <meta property="og:title" content="Admin Add-ons Credit  | Victory 5" />
    <title>Admin Add-ons Credit  | Victory 5</title>
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<?php include 'css.php'; ?>
</head>
<style media="screen">
.blue-btn {
    border: 0;
    background-color: #003c80;
    font-size: 15px;
    padding: 5px;
    border-radius: 3px;
}
.blue-btn:hover{
  cursor:pointer;
  background-color:#002b5d;
  transition:.15s ease-in-out;
}

td.tdh{
    text-align: center;
  }
  th.thh{
    text-align: center;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center">
  	<!-- <h1 class="pop-h1 text-center"><?php //echo _ADMINHEADER_COMPANY_BALANCE ?></h1> -->
    <h1 class="pop-h1"><?php echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></h1>
    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_COMPANY_TOTAL ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_TOTAL_LOT_SIZE ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_LAST_UPDATED ?></th>
                        <th class="thh"><?php echo _ADMINVIEWBALANCE_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($companyBalance)
                    {
                        for($cntA = 0;$cntA < count($companyBalance) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td class="tdh"><?php echo ($cntA+1)?></td>
                                <td class="tdh"><?php echo "$ ".number_format($companyBalance[$cntA]->getCtb(),2);?></td>
                                <td class="tdh"><?php echo number_format($companyBalance[$cntA]->getTls());?></td>
                                <td class="tdh"><?php echo date('d/m/Y h:i A',strtotime($companyBalance[$cntA]->getDateUpdated())); ?></td>
                                <td class="tdh"><form class="" action="editCompanyBalance.php" method="post">
                                  <input type="hidden" name="id" value="<?php echo $companyBalance[$cntA]->getId() ?>">
                                  <a class="blue-link"><button class="blue-btn clean" type="submit" name="editBalance"><?php echo _ADMINVIEWBALANCE_EDIT ?></button> </a>
                                </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

<!-- </div> -->


    <!-- <div class="width100 text-center overflow margin-bottom50">
        <button class="clean blue-button one-button-width">
            <a href="addOnBonus.php" class="red-link">Add On Point</a>
        </button>
    </div>  -->
	<!-- <h1 class="pop-h1"><?php// echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></h1> -->
 		<div class="clear"></div>
        <div class="width100 overflow margin-top50">
            <div class="profile-h3 text-center">
                <img src="img/member-ranking.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?>" title="<?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?>">
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_MEMBERS_RANKING ?></b></p>
                <!-- <p class="smaller-p"><?php// echo _ADMINNEWCREDIT_FIRST_STEP ?></p> -->
                 <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
                    <div class="width80 margin-auto"><label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input  type="file" name="file" id="file" accept=".xls,.xlsx"></div><div class="clear"></div>
                    <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
                    <button type="submit" id="submit" name="import"  class="clean blue-button one-button-width ow-width80"><?php echo _JS_SUBMIT ?></button>
                    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
                </form>
                <div class="width100 text-center">
                  <button  type="submit" id="Rank" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80">
                    <i id="loadingAnimate" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName"><?php echo _ADMINADDON_RANK_UPDATED ?></a>

                  </button>
                </div>
            </div>
            <div class="profile-h3 mid-profile-h3 text-center mid-tempo-margin-top">
                <img src="img/daily-commission.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?>" title="<?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?>">
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_DAILY_SPREAD ?></b></p>
                <!-- <p class="smaller-p"><?php //echo _ADMINNEWCREDIT_SECOND_STEP ?></p> -->
                <div class="width100 text-center">
                  <button type="submit" id="Daily" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top15 ow-width80">
                    <i id="loadingAnimate2" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName2"><?php echo _ADMINADDON_DAILY_BONUS ?></a>

                  </button>
                </div>

            </div>
            <div class="tempo-three-clear"></div>

            <div class="profile-h3 text-center tempo-margin-top">
                <img src="img/monthly-commission.png" class="three-small-img" alt="<?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?>" title="<?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?>">
                <p class="white-text p-title"><b><?php echo _ADMINNEWCREDIT_MONTHLY_PROFITS ?></b></p>
                <form action="" method="post" name="frmExcelImport" id="frmExcelImport2" enctype="multipart/form-data">
                    <div class="width80 margin-auto"><label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"></div><div class="clear"></div>
                    <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
                    <button type="submit" id="submit" name="import3"  class="clean blue-button one-button-width ow-width80"><?php echo _JS_SUBMIT ?></button>
                    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
                </form>
                 <div class="width100 text-center">
                      <button type="submit" id="Monthly" name="import"  class="clean blue-button one-button-width ow-purple-btn margin-top0 ow-width80">
                        <i id="loadingAnimate3" style="display: none" class="fa fa-spinner fa-spin margin-right12"></i><a id="buttonName3"><?php echo _ADMINADDON_MONTHLY_BONUS ?></a>

                      </button>
                 </div>
            </div>
		</div>
        <div class="clear"></div>



        <?php if(isset($_GET['type'])){
          $valueGet = $_GET['type'];
        }else {
          $valueGet = 0;
        } ?>
        <input type="hidden" name="getValue" value="<?php echo $valueGet ?>">
        <input type="hidden" name="getValue2" value="<?php echo $valueGet ?>">






    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>



</body>
</html>
