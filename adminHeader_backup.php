<!--
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="adminDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right admin-right-menu">
            	
              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_MARKETING_PLAN ?>
                            <img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>">

                <div class="dropdown-content yellow-dropdown-content">

                      <p class="dropdown-p"><a href="adminDailySpread.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_DAILY_SPREAD ?></a></p>
                      <p class="dropdown-p"><a href="viewLevel.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></a></p>

                </div>
              </div>

             

              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_MEMBERS ?>
                            <img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MEMBERS ?>" title="<?php echo _ADMINHEADER_MEMBERS ?>">

                <div class="dropdown-content yellow-dropdown-content">

                     
                      <p class="dropdown-p"><a href="adminAddUser.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_ADD_USER ?></a></p>
                      <p class="dropdown-p"><a href="adminViewMember.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>
                      <p class="dropdown-p"><a href="adminWithdrawal.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></p>

                </div>
              </div>


              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_REPORTS ?>
                          	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_REPORTS ?>" title="<?php echo _ADMINHEADER_REPORTS ?>">

                              

              	<div class="dropdown-content yellow-dropdown-content">
                      


                      <p class="dropdown-p"><a href="adminViewDaily.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_DAILY_BONUS ?></a></p>
                      <p class="dropdown-p"><a href="adminViewMonthly.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MONTHLY_BONUS ?></a></p>
                      <p class="dropdown-p"><a href="adminCapping.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></p>


                     
              	</div>

              </div>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言">
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="Language/语言" title="Language/语言">

                               

                	<div class="dropdown-content yellow-dropdown-content">
                        

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>

             
                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_MARKETING_PLAN ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MARKETING_PLAN ?>" title="<?php echo _ADMINHEADER_MARKETING_PLAN ?>"></li>
                                  <li><a href="adminDailySpread.php" class="mini-li"><?php echo _ADMINHEADER_DAILY_SPREAD ?></a></li>
                                  <li><a href="viewLevel.php" class="mini-li"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></a></li>
                                 
                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_MEMBERS ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MEMBERS ?>" title="<?php echo _ADMINHEADER_MEMBERS ?>"></li>
                                  <li><a href="adminAddUser.php" class="mini-li"><?php echo _ADMINHEADER_ADD_USER ?></a></li>
                                  <li><a href="adminViewMember.php" class="mini-li"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></li>
                                  <li><a href="adminWithdrawal.php" class="mini-li"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></li>

                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_REPORTS ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_REPORTS ?>" title="<?php echo _ADMINHEADER_REPORTS ?>"></li>
                                  <li><a href="adminViewDaily.php" class="mini-li"><?php echo _ADMINHEADER_DAILY_BONUS ?></a></li>
                                  <li><a href="adminViewMonthly.php" class="mini-li"><?php echo _ADMINHEADER_MONTHLY_BONUS ?></a></li>
                                  <li><a href="adminCapping.php" class="mini-li"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></li>

                                
                                  <li class="title-li unclickable-li"><img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言"> <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div>



            </div>
        </div>

</header>
-->
<div id="wrapper" class="toggled-2">

<div id="sidebar-wrapper">
<ul class="sidebar-nav nav-pills nav-stacked" id="menu">
	<li>
    	 <a href="adminDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
    </li>
    <li class="sidebar-li">
        <a href="adminDailySpread.php" class="overflow">
        	<img src="img/daily2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _ADMINHEADER_DAILY_SPREAD ?></p>
        </a>
    </li>
    <li class="sidebar-li">
        <a href="viewLevel.php" class="overflow">
        	<img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></p>
        </a>
    </li>
    <li class="sidebar-li">
        <a href="adminAddUser.php" class="overflow">
        	<img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _ADMINHEADER_ADD_USER ?></p>
        </a>
    </li>  
    <li class="sidebar-li">
        <a href="adminViewMember.php" class="overflow">
        	<img src="img/group.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _ADMINHEADER_VIEW_MEMBER ?></p>
        </a>
    </li>     
    <li class="sidebar-li">
        <a href="adminWithdrawal.php" class="overflow">
        	<img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></p>
        </a>
    </li>       
    <li class="sidebar-li">
        <a href="adminViewDaily.php" class="overflow">
        	<img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></p>
        </a>
    </li>     
    <li class="sidebar-li">
        <a href="adminViewMonthly.php" class="overflow">
        	<img src="img/report.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></p>
        </a>
    </li>      
    <!-- <li class="sidebar-li">
        <a href="adminCapping.php" class="overflow">
        	<img src="img/capping2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php //echo _MEMBERS_CAPPING_REPORTS ?></p>
        </a>
    </li>         -->
    <li class="sidebar-li">
        <a href="<?php $link ?>?lang=en" class="overflow">
        	<img src="img/english.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text">EN</p>
        </a>
    </li>    
    <li class="sidebar-li">
        <a href="<?php $link ?>?lang=ch" class="overflow">
        	<img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text">中文</p>
        </a>
    </li>      
    <li class="sidebar-li">
        <a href="logout.php" class="overflow">
        	<img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><?php echo _USERDASHBOARD_LOGOUT ?></p>
        </a>
    </li>       
        
</ul>
</div>



</div>
