<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$withdrawalDetails = getWithdrawal($conn, "WHERE status = 'PENDING'");
$totalPayout = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminWithdrawal.php" />
    <link rel="canonical" href="https://victory5.co/adminWithdrawal.php" />
    <meta property="og:title" content="View Member Withdrawal  | Victory 5" />
    <title><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?>  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .red{
    background-color: red;
  }
  .red:hover{
    background-color: maroon;
  }
  h1 .h1{
    color: maroon;
  }
  h1 .h1:hover{
    color: maroon;
  }
    .total-payout{
      float: right;
      outline-style: dashed;
      outline-offset: 10px;
      outline-color: grey;
      margin-right: 10px;
    }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
	<h1 class="small-h1-a text-center white-text"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?> | <a class="blue-link" href="adminWithdrawalHistory.php"><?php echo _ADMINHEADER_MEMBER_WITHDRAW_HISTORY ?></a> </h1>
    <div class="width100 shipping-div2">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_WITHDRAW_AMOUNT ?></th>
                        <th><?php echo _BANKDETAILS_ACC_NAME ?></th>
                        <th><?php echo _BANKDETAILS_ACC_NO ?></th>
                        <th><?php echo _BANKDETAILS_ACC_TYPE ?></th>
                        <th><?php echo _BANKDETAILS_BANK ?></th>
                        <th><?php echo _BANKDETAILS_BANK_SWIFT_CODE ?></th>
                        <th><?php echo _JS_COUNTRY ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <th><?php echo _MULTIBANK_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($withdrawalDetails)
                    {
                        for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                        {
                          $conn = connDB();
                          $userDetails = getUser($conn,"WHERE uid=?",array("uid"),array($withdrawalDetails[$cnt]->getUid()), "s");
                          $bank = $userDetails[0]->getBankAccName();
                          $bankNo = $userDetails[0]->getBankAccNumber();
                          $bankAccType = $userDetails[0]->getBankAccType();
                          $bankName = $userDetails[0]->getBankName();
                          $bankSwiftCode = $userDetails[0]->getBankSwiftCode();
                          $country = $userDetails[0]->getCountry();
                          $totalPayout += $withdrawalDetails[$cnt]->getAmount();
                        ?>
                            <tr>
                                <td style="vertical-align: middle " ><?php echo ($cnt+1)?></td>
                                <td style="vertical-align: middle " ><?php echo $withdrawalDetails[$cnt]->getUsername();?></td>
                                <td style="vertical-align: middle " ><?php echo "$ ".$withdrawalDetails[$cnt]->getAmount();?></td>
                                <td style="vertical-align: middle " >
                                  <?php if ($bank) {
                                  echo $bank;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " >
                                  <?php if ($bankNo) {
                                  echo $bankNo;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " >
                                  <?php if ($bankAccType) {
                                  echo $bankAccType;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " >
                                  <?php if ($bankName) {
                                  echo $bankName;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " >
                                  <?php if ($bankSwiftCode) {
                                  echo $bankSwiftCode;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " >
                                  <?php if ($country) {
                                  echo $country;
                                }else {
                                  echo "-";
                                }?>
                                </td>
                                <td style="vertical-align: middle " ><?php echo date('d/m/Y',strtotime($withdrawalDetails[$cnt]->getDateCreated()));?></td>
                                <td style="vertical-align: middle " ><?php echo date('h:i a',strtotime($withdrawalDetails[$cnt]->getDateCreated()));?></td>
                                <td style="vertical-align: middle " >
                                  <button id="approve" name="approve" class="clean blue-ow-btn" type="submit" value="<?php echo $withdrawalDetails[$cnt]->getId() ?>"><?php echo _ADMINVIEWBALANCE_APPROVE ?></button><br>
                                  <button id="reject" style="margin-top: 10px;width: 72.5px" name="reject" class="clean blue-ow-btn red" type="submit" value="<?php echo $withdrawalDetails[$cnt]->getId() ?>"><?php echo _ADMINVIEWBALANCE_REJECT ?></button>
                              </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                  }else {
                    ?><tr>
                      <td colspan="12"style="text-align: center;font-weight: bold"><?php echo _ADMINWITHDRAWAL_NO_MEMBER ?></td>
                    </tr> <?php
                  }
                    ?>

                </tbody>
            </table>
        </div>
        <br><br>
        <div class="total-payout">
      <p><?php echo _ADMINVIEWBALANCE_PAYOUT." $ ".$totalPayout ?></p>
        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
<script>
  $("#approve,#reject").click(function(){
    var id = $(this).val();
    var name = $(this).attr("name");

    $.ajax({
      url: 'utilities/adminWithdrawalFunction.php',
      data: {id:id,name:name},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var success = response[0]['success'];
        putNoticeJavascriptReload("Notice !! ",""+success+"");
      }
    });
  });
</script>
</html>
