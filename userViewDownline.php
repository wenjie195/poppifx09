<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
$uid = $_GET['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];
$username = $userData->getUsername();
$directDownlineUid = [];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$getWho = getWholeDownlineTree($conn,$uid,false);

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$getWho = getWholeDownlineTree($conn, $uid, false);
if ($getWho)
{
  for ($i=0; $i <count($getWho) ; $i++)
  {
    $currentLevel = $getWho[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
      $directDownlineUid[] = $getWho[$i]->getReferralId();
    }
    // $referralId = $getWho[$i]->getReferralId();
    // $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    // if ($downlineDetails)
    // {
    //   $personalSales = $downlineDetails[0]->getBalance();
    //   $groupSales += $personalSales;
    //   $groupSalesFormat = number_format($groupSales,4);
    // }
  }
}

$directDownlineUidImp = implode(",",$directDownlineUid);
$directDownlineUidExp = explode(",",$directDownlineUidImp);

$PersonalSales = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userViewDownline.php" />
    <link rel="canonical" href="https://victory5.co/userViewDownline.php" />
    <meta property="og:title" content="<?php echo $username."'s Direct Downline" ?>  | Victory 5" />
    <title><?php echo $username."'s Direct Downline" ?>  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height">

  <h1 class="text-center white-text opacity-hover">
    <a style="float: left" href="#" onclick="window.history.back()" ><img style="vertical-align: middle;width: 35px;" src="img/back3.png" alt=""></a> <?php echo $username ?><?php echo _USERDASHBOARD_DIRECT_DOWNLINE2 ?></h1>
  <?php $level = $userLevel->getCurrentLevel();?>
<div class="overflow-scroll-div">
  <table class="table-css fix-th">
    <thead>
      <tr>
        <th><?php echo _HIERARCHY_GENERATION ?></th>
        <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
        <!-- <th>MT4 ID</th> -->
        <th><?php echo _HIERARCHY_SPONSOR ?></th>
        <!-- <th>Last Order</th> -->
        <!-- <th><?php //echo _HIERARCHY_PERSONAL_SALES ?></th> -->
        <th><?php echo _HIERARCHY_DIRECT_DOWNLINE ?></th>
        <th><?php echo _MONTHLY_LEVEL ?></th>
        <!-- <th><?php //echo _HIERARCHY_GROUP_SALES ?></th> -->
        <th><?php echo _HIERARCHY_GROUP_MEMBER ?></th>
        <!-- <th><?php //echo _HIERARCHY_DIRECT_DOWNLINE ?></th> -->
        <!-- <th>Hierarchy</th> -->
      </tr>
    </thead>
    <tbody>
    <?php
    $conn = connDB();
    if($directDownlineUidExp)
    {
    for($cnt = 0;$cnt < count($directDownlineUidExp) ;$cnt++)
    {
      $totalGroupSales = 0;
      $groupSales = 0;
      $PersonalSales = 0;
      $totalDownline = 0;
      $downline = 0;
      $directDownline = 0;
      $downlineCurrentLvl = 0;
      $totalDirectDownline = 0;
      $downline = $getWho[$cnt]->getCurrentLevel();
      $directDownline = $downline + 1;

      $mpIdrawDataDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"), array($directDownlineUidExp[$cnt]), "s");
      if ($mpIdrawDataDetails) {
        $PersonalSales = $mpIdrawDataDetails[0]->getBalance();
      }
      $downlineUid = $getWho[$cnt]->getReferralId();
      $getWhoII = getWholeDownlineTree($conn,$downlineUid,false);
      if ($getWhoII) {
        for ($i=0; $i <count($getWhoII) ; $i++) {
          $allDownlineUid = $getWhoII[$i]->getReferralId();
          $downlineCurrentLvl = $getWhoII[$i]->getCurrentLevel();
          $mpIdrawDataDetailsII = getMpIdData($conn, "WHERE uid = ?",array("uid"), array($allDownlineUid), "s");
          if ($mpIdrawDataDetailsII) {
            $groupSales = $mpIdrawDataDetailsII[0]->getBalance();
            $totalGroupSales += $groupSales;
          }
          if ($directDownline == $downlineCurrentLvl) {
            $totalDirectDownline++;
          }
        }
        // $totalDownline = count($getWhoII);
      }

    ?>
    <tr>
    <td>
      <?php
        $downlineLvl = $getWho[$cnt]->getCurrentLevel();
        echo $lvl = $downlineLvl - $level;
      ?>
    </td>
    <td>
      <?php
        $userUid = $getWho[$cnt]->getReferralId();
        $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
        echo $username = $thisUserDetails[0]->getUsername();
      ?>
    </td>
    <!-- <td><?php //echo $userMemberID = $thisUserDetails[0]->getMpId();;?></td> -->
    <td>
      <?php
        $uplineUid = $getWho[$cnt]->getReferrerId();
        $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
        // $userData = $uplineDetails[0];
        echo $uplineUsername = $uplineDetails[0]->getUsername();
      ?>
    </td>
    <!-- <td><?php //echo date("d-m-Y",strtotime($thisUserDetails[0]->getDateUpdated()));?></td> -->
    <!-- <td><?php //echo "$ ".number_format($PersonalSales,2);?></td> -->
    <td><?php echo $totalDirectDownline ?></td>
    <td><?php echo $thisUserDetails[0]->getRank() ?></td>
    <!-- <td><?php //echo "$ ".number_format($totalGroupSales,2) ?></td> -->
    <td>
      <?php
        $getDownlineAmount = getWholeDownlineTree($conn, $userUid,false);
        if ($getDownlineAmount)
        {
          echo $groupMember = count($getDownlineAmount);
        }
        else
        {
          echo $groupMember = 0;
        }

      ?>
    </td>

    <!-- <td>
      <form action="userDownlineDetails.php" method="POST">
        <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php //echo $getWho[$cnt]->getReferralId();?>">
          <?php //echo _HIERARCHY_TREE_VIEW ?>
        </button>
      </form>
    </td> -->
    </tr>
    <?php
    }
    ?>
    <?php
    }
    $conn->close();
    ?>
    </tbody>
  </table>
</div>

<div class="clear"></div>

</div>

<?php //include 'rankIdentifySolo.php' ?>
<?php include 'js.php'; ?>

</body>
</html>
