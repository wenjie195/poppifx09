<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_type = 1 ORDER BY email DESC");
// $userDetails = getUser($conn, "WHERE user_type = 1 AND date_created >=  '2020-04-09 12:00:00' ");
// $monthlyProBon = getMonProBon($conn);

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/adminViewMemberEmailD.php" />
    <link rel="canonical" href="https://victory5.co/adminViewMemberEmailD.php" />
    <meta property="og:title" content="View Member  | Victory 5" />
    <title>View Member  | Victory 5</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <div class="width100 shipping-div2">

        <div class="search-big-div">
            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput3" onkeyup="myFunctionC()" placeholder="<?php echo _MULTIBANK_SEARCH ?> MT4" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
            </div>
        </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th" id="myTable">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><a href="adminViewMemberUsername.php"><?php echo _MAINJS_INDEX_USERNAME ?></a></th>
                        <th><a href="adminViewMemberFullname.php"><?php echo _JS_FULLNAME ?></a></th>
                        <th>MT4 ID</th>
                        <th><?php echo _JS_PHONE ?></th>
                        <th><a href="adminViewMemberFullname.php"><?php echo _JS_EMAIL ?></a></th>
                        <th>Personal Sales</th>
                        <th>Group Sales</th>
                        <th>Direct Downline</th>
                        <th>Group Members</th>
                        <!-- <th>UID</th> -->
                        <th><?php echo _MULTIBANK_ID_DOCUMENT ?></th>
                        <th><?php echo _MULTIBANK_UTILITY_BILL ?></th>
                        <th>LPOA</th>
                        <th><?php echo _ADMINVIEWBALANCE_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($userDetails)
                    {
                        for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                        {
                          $mpIdData = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
                          $directDownline = 0;
                          $groupSales = 0;
                          $personalSales = 0;
                          $personalSalesUser = 0;
                          $groupMember = 0;
                          $referrerDetails = getWholeDownlineTree($conn, $userDetails[$cnt]->getUid(), false);
                          if ($mpIdData) {
                            $personalSalesUser = $mpIdData[0]->getBalance();
                          }
                          $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($userDetails[$cnt]->getUid()), "s");
                          $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
                          $directDownlineLevel = $referralCurrentLevel + 1;
                          if ($referrerDetails)
                          {
                            $groupMember = count($referrerDetails);
                            for ($i=0; $i <count($referrerDetails) ; $i++)
                            {
                              $currentLevel = $referrerDetails[$i]->getCurrentLevel();
                              if ($currentLevel == $directDownlineLevel)
                              {
                                $directDownline++;
                              }
                              $referralId = $referrerDetails[$i]->getReferralId();
                              $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
                              if ($downlineDetails)
                              {
                                $personalSales = $downlineDetails[0]->getBalance();
                                $groupSales += $personalSales;
                                $groupSalesFormat = number_format($groupSales,4);
                              }
                            }
                          }
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $userDetails[$cnt]->getFullname();?></td>
                                <td><?php echo $userDetails[$cnt]->getMpId();?></td>
                                <td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userDetails[$cnt]->getEmail();?></td>
                                <td><?php echo "$ ".number_format($personalSalesUser,2) ?></td>
                                <td><?php echo "$ ".number_format($groupSales,2) ?></td>
                                <td><?php echo  $directDownline ?></td>
                                <td><?php echo  $groupMember ?></td>
                                <!-- <td><?php //echo $userDetails[$cnt]->getUid();?></td> -->

                                <td>
                                    <?php $idDoc = $userDetails[$cnt]->getIcBack();
                                    if($idDoc == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewIDDoc.php" method="POST"> -->
                                        <form action="adminViewIDDoc.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($idDoc == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $utiBill = $userDetails[$cnt]->getLicense();
                                    if($utiBill == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewBills.php" method="POST"> -->
                                        <form action="adminViewBills.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($utiBill == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $sform = $userDetails[$cnt]->getSignature();
                                    if($sform == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewSignature.php" method="POST"> -->
                                        <form action="adminViewSignature.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($sform == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>

                                    <form action="adminUpdateCustomerDetails.php" method="POST">
                                        <button class="clean blue-ow-btn" type="submit" name="customer_id" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <?php echo _MULTIBANK_UPDATE ?>
                                        </button>
                                    </form>

                                </td>

                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>

</body>
</html>