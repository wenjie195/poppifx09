<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Requirements.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$monthlyProBon = getMonProBon($conn);

$requirementDetails = getRequirements($conn);

$cntAA = 1;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/viewLevel.php" />
    <link rel="canonical" href="https://victory5.co/viewLevel.php" />
    <meta property="og:title" content="Level Details  | Victory 5" />
    <title>Level Details  | Victory 5</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/levels.png" class="middle-title-icon" alt="<?php echo _VIEWLEVEL_LEVEL_DETAILS ?>" title="<?php echo _VIEWLEVEL_LEVEL_DETAILS ?>">
    </div>
	<div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _VIEWLEVEL_LEVEL_DETAILS ?></h1>
	</div>
    <div class="width100">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _MONTHLY_LEVEL ?></th>
                        <th><?php echo _VIEWLEVEL_AMOUNT ?> (%)</th>
                        <th><?php echo _ADMINVIEWBALANCE_EDIT ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($monthlyProBon)
                    {
                        for($cntA = 0;$cntA < count($monthlyProBon) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cntA+1)?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getLevel();?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getAmount();?></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_EDIT ?></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
	<!-- <div class="width100 overflow">
    	<h1 class="pop-h1 text-center"><?php echo _VIEWLEVEL_REQUIREMENTS ?></h1>
	</div>
    <div class="width100">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _VIEWLEVEL_DIRECT_SPONSOR ?></th>
                        <th><?php echo _VIEWLEVEL_SELF_INVEST ?></th>
                        <th><?php echo _VIEWLEVEL_PROFIT_SHARING ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_EDIT ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($requirementDetails)
                    {
                        for($cntA = 0;$cntA < count($requirementDetails) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cntA+1)?></td>
                                <td><?php echo $requirementDetails[$cntA]->getDirectSponsor();?></td>
                                <td><?php echo "RM ".number_format($requirementDetails[$cntA]->getSelfInvest(),2);?></td>
                                <td><?php echo "Level ".$requirementDetails[$cntA]->getProfitSharing();?></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_EDIT ?></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div> -->

</div>

<?php include 'js.php'; ?>

</body>
</html>
