<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Requirements.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$monthlyProBon = getMonProBon($conn);

$requirementDetails = getRequirements($conn);

$cntAA = 1;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
   
    <meta property="og:url" content="https://victory5.co/marketingPlan.php" />
    <link rel="canonical" href="https://victory5.co/marketingPlan.php" />
    <meta property="og:title" content="Level Details  | Victory 5" />
    <title>Level Details  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
	<h1 class="pop-h1 text-center"><?php echo _VIEWLEVEL_LEVEL_DETAILS ?></h1>

    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _MONTHLY_LEVEL ?></th>
                        <th><?php echo _VIEWLEVEL_AMOUNT ?> (%)</th>
                        <th><?php echo _ADMINVIEWBALANCE_EDIT ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($monthlyProBon)
                    {
                        for($cntA = 0;$cntA < count($monthlyProBon) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cntA+1)?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getLevel();?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getAmount();?></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_EDIT ?></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

    <h1 class="pop-h1 text-center"><?php echo _VIEWLEVEL_REQUIREMENTS ?></h1>

</div>

<?php include 'js.php'; ?>

</body>
</html>
