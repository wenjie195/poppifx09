<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$docRows = getStatus($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$docDetails = $docRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/lpoa.php" />
    <link rel="canonical" href="https://victory5.co/lpoa.php" />
    <meta property="og:title" content="LPOA  | Victory 5" />
    <title>LPOA  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'userHeader.php'; ?>


		<div class="menu-distance-height width100 no-print"></div>
  		<!--<img src="img/multibank-white.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">-->
        <div class="page1-div same-padding ow-padding-lpoa">
			<div class="left-50-div">
            	<div class="width100 grey-box-print">
                	<p class="box-title-p">
                    	Limited Power of Attorney Agreement<br><br>
                        (The "LPOA Agreement")
                    </p>
                </div>
                <table class="print-table-css">
                	<tr>
                    	<td class="number-td"><b>1.</b></td>
                        <td><b>Terms of LPOA Agreement</b></td>
                    </tr>
                	<tr>
                    	<td class="number-td"><b>1.1</b></td>
                        <td>We are pleased to enclose this <b>Limited Power of Attorney Agreement</b> (referred to as "<b>LPOA Agreement</b>") which is part of a wider agreement between <b>you</b>:</td>
                    </tr>                    
                    
                </table>
                <table class="print-table-css">
                	<tr>
                    	<td class="name-td"><b>Full Name</b></td>
                        <td class="underline-td"><?php echo $userDetails->getFullname();?></td>
                    </tr>
                	<tr>
                    	<td class="name-td"><b>Permanent Address</b></td>
                        <td class="underline-td"></td>
                    </tr>                    
                    
                </table>                
                <table class="print-table-css add-margin-top-table">
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td>(Also referred to as the "<b>client</b>", "<b>your</b>" and "<b>yourself</b>");</td>
                    </tr>
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td><b>AND</b></td>
                    </tr>                    
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td>[Cheehaur Tan] (referred to as "Trading Agent"); registered at [40 Lorong Kenanga Jalan Raja Uda 13000 Butterworth Pulau Pinang].</td>
                    </tr>      
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td><b>AND</b></td>
                    </tr>                     
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td>Infinox Capital, (referred to as "Infinox", "<b>we</b>", "<b>us</b>", "<b>our</b>") is a registered trading name of IX Capital Group Limited, a company duly incorporated under the laws of the Commonwealth of The Bahamas and regulated by the Securities Commission of The Bahamas (SCB), registration number SIA-F188. Our registered office is situated at No. 109 Church Street, Sandyport, Marina Village, P.O. Box SP-62756, Nassau, The Bahamas.</td>
                    </tr>   
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td>This LPOA Agreement sets out the terms and conditions (referred to as "<b>Terms</b>") governing your trading Account (referred to as "Account") and all trading carried out in your Account with us.</td>
                    </tr>                       
                	<tr>
                    	<td class="number-td"><b>1.2</b></td>
                        <td>Our agreement with you consists of several documents and also certain key product information that can be accessed through our Website or our Platform, and specifically comprises:
                        <ol class="roman-ol">
                        	<li>The terms and conditions of the Client Agreement signed by you; and</li>
                        	<li>Best Execution Policy</li>
                            <li>Complaints Procedure</li>
                            <li>Conflict of Interest Policy</li>
                            <li>Privacy Policy</li>
                            <li>Risk Warning Notice</li>
                            <li>Trade Copying Terms & Conditions</li>
                            <li>The terms (including the Schedule) of this LPOA Agreement; and</li>
                            <li>Any client application (referred to as "Application") that you submit (whether online through the website, via email or other means) to open an Account</li>
                        </ol>
                        </td>
                    </tr>                       
                	<tr>
                    	<td class="number-td"><b>1.3</b></td>
                        <td>If there is any conflict between this Agreement and the Securities Industry Act, 2011 (the "Act") or applicable Regulations, the Act and applicable regulations will prevail. Nothing in this Agreement will exclude or restrict any duty or liability owed by us to you under the Act or Applicable Regulations under which we are not permitted to exclude or restrict. We assume no greater responsibility or fiduciary duty than that imposed by the Applicable Regulations or the express Terms of the LPOA Agreement.</td>
                    </tr>  
                	<tr>
                    	<td class="number-td"><b>1.4</b></td>
                        <td><b>For your own benefit and protection, you should take sufficient time to read the terms of this LPOA Agreement, as well as the additional documents provided to you as part of the wider Agreement and information available on our Website and through our Platform before you decide to apply for the discretionary account management activity on your trading Account by the Trading Agent, to trade Foreign Exchange and CFD trades and provide brokerage services for Transactions in such other products on your behalf via a Limited Power of Attorney arrangement.</b></td>
                    </tr>                                                                           
                </table>                                
            </div>
			<div class="right-50-div">
                <table class="print-table-css">
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td><b>This LPOA Agreement contains important information about yours and our rights and obligations in relation to the services provided to you. You should contact us as promptly as possible to ask for further information or if there is anything you do not understand. You should seek advice from a professional legal or financial adviser if you are in any doubt or do not understand anything.</b></td>
                    </tr>
                	<tr>
                    	<td class="number-td"><b>1.5</b></td>
                        <td>It is our intention that:
                            <ol class="roman-ol">
                            	<li>The LPOA Agreement contains all the terms and conditions that govern our relationship and your activities carried on with us through our Platform; and</li>
                                <li>The LPOA Agreement supersedes any prior oral or written representations, arrangements, understandings and/or agreements between you and us which relate to your activities carried on through our Platform; and</li>
                                <li>Any acts, omissions or representations (oral or otherwise) made by you or us (including any of our staff and/or client management team who you have dealings with) shall not amend or take priority over the LPOA Agreement; and</li>
                                <li>
                                	All terms, conditions and warranties implied by statute or common law are excluded to the fullest extent permitted by law. No variation of this agreement shall be valid unless it is in writing and signed by or on behalf of both parties;
                                </li>
                                <li>
                                	If any terms of this LPOA Agreement contradict the Client Agreements, the terms of this LPOA shall prevail with regards to the subject matter hereof; and
                                </li>
                                <li>
                                	This LPOA may be executed in counterparts, in original or scanned electronic copy, each of which shall be deemed an original but all of which together shall constitute one and the same instrument.
                                </li>
                            </ol>
                        </td>
                    </tr>                    
               
                	<tr>
                    	<td class="number-td"><b>2.</b></td>
                        <td><b>Services</b></td>
                    </tr>
                	<tr>
                    	<td class="number-td"><b>2.1</b></td>
                        <td><b>[Cheehaur Tan]</b> (referred to as the <b>"Trading Agent"</b>)</td>
                    </tr>                    
                	<tr>
                    	<td class="number-td"><b>2.1.1</b></td>
                        <td>The Client hereby appoints and authorises to give limited power of attorney to the Trading Agent in connection with the Client's Account(s) with full power as Trading Agent of the Client, to do any one or more of the following permitted Investment Management (or Asset Management) services under its authorisation, on the Client's behalf and in the name of the Client:
                        <ol class="latin-ol">
                        	<li>Buy and sell or otherwise acquire or dispose of Contracts for Difference (CFDs), Spot foreign exchange (FX), commodity contracts and any other investments, on margin or otherwise, for and at the risk of the Client;</li>
                            <li>
                            	To act for the Client and on the Client's behalf in the same manner and with the same force and effect as the Client might or could do with respect to such purchases, sales or trades, as well as with respect to all other things necessary or incidental to the furtherance or conduct of such purchases, sales or trades. This authorisation will be applicable to all funds held on the Client's account for which Infinox and the Trading Agent have been designated.
                            </li>
                            <li>
                            	To transfer funds between and among Client Accounts held with Infinox, as long as the Trading Agent is the approved Trading Agent of all relevant Accounts.
                            </li>
                        </ol>
                        
                        </td>
                    </tr>      
                	<tr>
                    	<td class="number-td"><b>2.1.2</b></td>
                        <td>This LPOA does not permit the Trading Agent to any of the following activities:
                        	<ol class="latin-ol">
                            	<li>To deposit funds and assets into the Client's Account(s), to redeem or withdraw funds or assets from the Clients Account(s), to sign or agree to Terms and Conditions on behalf of the Client for services offered by Infinox and/or new or updated Client Agreement in connection with this LPOA.</li>
                                <li>This LPOA does not authorise the Trading Agent to change the Client's username or password or to add, delete or change the Client's Account information.</li>
                            </ol>
                        </td>
                    </tr>        
                    
                    
                                                                                             
                </table>                                
            </div>            
            
            
            
            
            
            
        </div>
         <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Updated: May, 2019</td>
                        <td class="right-page-number">Page 1 of 4</td>
                	</tr>
                </tbody>
            </table>
        </div>       
        
        
       <div class="page-break"></div>

        
        
        <div class="page1-div same-padding ow-padding-lpoa">

			<div class="left-50-div">

                <table class="print-table-css">

                	<tr>
                    	<td class="number-td"><b>3.</b></td>
                        <td><b>Fees</b>
                        </td>
                    </tr>  
                	<tr>
                    	<td class="number-td"><b>3.1</b></td>
                        <td>Performance Fee
                        </td>
                    </tr>  
                	<tr>
                    	<td class="number-td">3.1.1</td>
                        <td>For details of the performance fees, refer to Schedule 1 of this LPOA Agreement.
                        </td>
                    </tr>                      
                	<tr>
                    	<td class="number-td">3.1.2</td>
                        <td>Performance Fee: [40] % of performance, net of fees, on a "High Water Mark" basis. 
                        </td>
                    </tr>   
                	<tr>
                    	<td class="number-td">3.1.3</td>
                        <td>This Performance Fee, calculated by the Trading Agent is due and payable on the last business day of each calendar month, such month being a 'Calculation Period' where the Performance on an Account is an amount equal to:
                        <ul class="lp-ul">
                        	<li>a change in the Equity value of the Account;</li>
                            <li>plus any withdrawals from the Account;</li>
                            <li>less any deposits to the Account.</li>
                        </ul>
                        <br>
                        Since the last time a Fee was payable or the date of this Authority in relation to the first calculated period provided that the Equity in the Account is above the High  Water Mark.<br>The High Water Mark on a date for the Account, means the greater of:
                        <ul class="lp-ul">
                        	<li>the Account Equity as at the date of this Authority;<br>and</li>
                            <li>the Account Equity as at the end of the Calculation Period for which a Fee was last paid or payable and adjusted for any withdrawals from or deposits in the Account in the Calculation Period</li>
                        </ul>
                        </td>
                    </tr>                       
                	<tr>
                    	<td class="number-td">3.1.4</td>
                        <td>All amounts charged above shall be charged to the Client at the currency equivalent of the Client's Account base currency.
                        </td>
                    </tr>   
                	<tr>
                    	<td class="number-td">3.1.5</td>
                        <td>The Trading Agent has sole discretion in relation to the amount of performance fee charged under this LPOA Agreement. Infinox has no responsibility or liability as to the performance fee charged by the Trading Agent to the Client in accordance with this LPOA Agreement. The Trading Agent shall be responsible for calculating the performance fees due to the Trading Agent by the Client each month. 
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td"><b>3.2</b></td>
                        <td><b>Management Fee</b>
                        </td>
                    </tr> 
                	<tr>
                    	<td class="number-td">3.2.1</td>
                        <td>For details of the management fees, refer to Schedule 1. 
                        </td>
                    </tr>                      
                	<tr>
                    	<td class="number-td">3.2.2</td>
                        <td>This Management Fee, calculated by the Trading Agent, is a per annum percentage that is charged on the equity of the account on a monthly basis. The fee is charged on
                        </td>
                    </tr>                     
<tr>
                    	<td class="number-td"> </td>
                        <td>the daily equity going back throughout the Calculation Period. The calculation uses end of day account (GMT) equity.
                        </td>
                    </tr>   
                	<tr>
                    	<td class="number-td"><b>3.3</b></td>
                        <td>Additional fees (fee adjustments)
                        </td>
                    </tr>                      
                	<tr>
                    	<td class="number-td">3.3.1</td>
                        <td>For details of the additional fees (fee adjustments), refer to Schedule 1 of this LPOA Agreement.
                        </td>
                    </tr>  
                	<tr>
                    	<td class="number-td">3.3.2</td>
                        <td>Infinox is not an agent of the Trading Agent, and in regards to the additional fees (fee adjustments) charged by the Trading Agent, Infinox is only acting to facilitate the payment of such fees and commissions from the Client to Trading Agent.
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td">3.3.3</td>
                        <td>Any dispute with regards to any fees charged to the Client by the Trading Agent under this LPOA Agreement shall strictly be between the Trading Agent and Client, and Infinox shall have no liability to either for the facilitation of the fees or the fees.
                        </td>
                    </tr>                       
                	
                	<tr>
                    	<td class="number-td"><b>3.4</b></td>
                        <td><b>Authority to pay fees</b>
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">3.4.1</td>
                        <td>Authority<br>The Client expressly authorises and directs Infinox to withdraw from the Client Account and pay to the Trading Agent the fees and commissions ("Fees") calculated by the Trading Agent and payable at the specified time(s) set out in the Schedule(s) below. In doing so, Infinox is acting as payment agent for the Client.
                        </td>
                    </tr>                                                                                                                   
                </table>
            </div>
            <div class="right-50-div">
                <table class="print-table-css">

                	<tr>
                    	<td class="number-td">3.4.2</td>
                        <td>Authority Revocable<br>This authority may be revoked by notice in writing by the Client to Infinox. 
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">3.4.3</td>
                        <td>Liability<br>It is acknowledged by the Client that Infinox is bound to act in accordance with this Authority and is released from any obligation or liability that would or might otherwise attach to it in relation to this Authority or the calculation of any Fees by the Trading Agent.
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">3.4.4</td>
                        <td>Indemnity<br>The Client continuously indemnifies Infinox in respect of all liabilities and expenses properly incurred or suffered by Infinox by reason of acting in accordance with this Authority or connected with it,or arising out of the calculation of any Fees by the Trading Agent.
                        </td>
                    </tr> 
                	<tr>
                    	<td class="number-td"><b>4.</b></td>
                        <td><b>Client consents to the following</b>
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td"><b>4.1</b></td>
                        <td><b>Risk Warning</b>
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td><b>Trading leveraged derivative products such as Foreign Exchange (Forex), Contracts for Difference (CFDs) or other financial derivative products carries a high level of risk to your capital. All these products, which are leveraged derivative products, may not be appropriate for all investors. The effect of leverage is that both gains and losses are magnified. The prices of leveraged derivative products may change to your disadvantage very quickly, it is possible for you to lose more than your invested capital and you may be required to make further payments.</b>
                        </td>
                    </tr>
                	<tr>
                    	<td class="number-td"><b> </b></td>
                        <td><b>Before deciding to invest in any financial product, you should carefully consider your investment objectives, trading knowledge and experience and affordability. You should only trade in Forex and CFDs if you have sufficient knowledge and experience of the risky nature of the products, the risks involved in trading such products and if you are dealing with money that you can afford to lose. You should seek independent professional financial advice if you are in any doubt or do not understand the risks involved.</b>
                        </td>
                    </tr> 
<tr>
                    	<td class="number-td"><b>4.2</b></td>
                        <td><b>Client's Account</b>	
                        </td>
                    </tr>
                    <tr>
                    	<td class="number-td">4.2.1</td>
                        <td>The Client confirms any and all transactions in respect of their Account are made by the Trading Agent on their behalf.
                        </td>
                    </tr>                    
                    <tr>
                    	<td class="number-td">4.2.2</td>
                        <td>Even though the Client has granted the trading authority to the Trading Agent, it remains the Client's responsibility to closely scrutinise and monitor account activity.
                        </td>
                    </tr>                     
                    <tr>
                    	<td class="number-td">4.2.3</td>
                        <td>The Client consents to Infinox providing and/or allowing the Trading Agent to access the Client's Account data including the Client's personal or other information held by Infinox (including passwords).
                        </td>
                    </tr>                    
                    <tr>
                    	<td class="number-td">4.2.4</td>
                        <td>The Client further consents to Infinox providing copies to the Trading Agent of all notices, statements, information and correspondence relating to Client's Account(s).
                        </td>
                    </tr>                          
					<tr>
                    	<td class="number-td">4.2.5</td>
                        <td>The Client has at all times access to view their trading Account managed by the Trading Agent under this LPOA Agreement, using their login details issued by Infinox. The Client is able to generate statements at any time which show the ledger balance, the exact positions in the account, the net profit or loss in all contracts closed for the given period, and the net unrealised profit and loss in all open contracts figured to the market. The Client agrees to review their account statements on a regular basis, and contact Infinox immediately if he/she has any queries. Any discrepancies on account statements must be reported to Infinox as soon as they are identified.
                        </td>
                    </tr>                          
                    <tr>
                    	<td class="number-td">4.2.6</td>
                        <td>It is the Client's responsibility to request from Infinox statements on account status and details regarding the overall management of the account.
                        </td>
                    </tr>    
                    <tr>
                    	<td class="number-td">4.2.7</td>
                        <td>The Client agrees to receive a monthly account statement from Infinox in relation to the trading activity carried out by the Trading Agent on the Client's Account.
                        </td>
                    </tr>                                                                                                                                                     
            	</table>
            </div>
        </div>
         <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Updated: May, 2019</td>
                        <td class="right-page-number">Page 2 of 4</td>
                	</tr>
                </tbody>
            </table>
        </div>        
        
        
    
        

        
        
        <div class="page-break"></div>          
        
        
        
        <div class="page1-div same-padding ow-padding-lpoa">
			<div class="left-50-div">

                <table class="print-table-css">
                	
                                   
                    
                    <tr>
                    	<td class="number-td">4.2.8</td>
                        <td>The Client understands that a commission or additional fee referred to as "<b>fee adjustments</b>" will be charged to their Account payable each time a trade is affected as specified in Schedule 1.
                        </td>
                    </tr> 
                    <tr>
                    	<td class="number-td">4.2.9</td>
                        <td>The Client is aware that Infinox cannot and will not guarantee the performance of the trading carried out on the Client's Account by the Trading Agent on behalf of the Client. Infinox will reject all claims of the Client that the Trading Agent failed to comply with the Client's recommendations. Infinox does not endorse the services provided by the Trading Agent. Infinox is not aligned to any Trading Agent and is the foreign exchange (Forex) trading platform provider. Infinox does not endorse the operating methods or recommend any Trading Agent and is not liable for any potential negative trading performance of the Trading Agent.
                        </td>
                    </tr>                    
                    <tr>
                    	<td class="number-td">4.2.10</td>
                        <td>The Client agrees to pay Infinox promptly on demand for any and all losses arising therefrom or debit balance due thereon. The undersigned hereby ratifies and confirms any and all transactions with Infinox hereafter made by the Trading Agent for the Client's Account(s).
                        </td>
                    </tr>                    
                    <tr>
                    	<td class="number-td"><b>5.</b></td>
                        <td><b>Indemnifications</b>
                        </td>
                    </tr>   
                    <tr>
                    	<td class="number-td"><b>5.1</b></td>
                        <td><b>Indemnification of Infinox by the Client</b>
                        </td>
                    </tr>                     
                    <tr>
                    	<td class="number-td">5.1.1</td>
                        <td>The Client agrees to indemnify and hold Infinox and its affiliates, employees, agents and its and their successors and its and their directors, officers, employees and agents harmless from and against all claims, actions, costs liabilities, including attorney's or legal fees, arising out of or relating to their reliance on this LPOA Agreement, execution of any of the Trading Agent's instructions, Infinox following the instructions of the Trading Agent; the performance or non-performance of the Trading Agent's services; any trade or action of the Trading Agent in Client's Account(s); and any dispute involving Trading Agent and the Client.	
                        </td>
                    </tr> 
<tr>
                    	<td class="number-td">5.1.2</td>
                        <td>The Client will not attempt to hold Infinox liable for any decision or action taken by the Trading Agent. This Indemnification shall remain in full force and effect until the LPOA Agreement is terminated in accordance with the terms of this LPOA Agreement, such termination shall not affect any liability in any way resulting from transactions initiated prior to such termination. Infinox's rights under this paragraph are in addition to any other rights under other agreements with the Client and/or Trading Agent.
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">5.1.3</td>
                        <td>The Client understands that the Trading Agent may use an electronic trading system to generate trades, which exposes the Client to risks associated with the use of computers, and data feed systems relied on by Infinox. The Client agrees to accept such risks, which may include, but are not limited to, failure of hardware, software or communication lines or systems and/or inaccurate external data feeds provided by third-party vendors. The Client accepts that Infinox is not liable for any resulting losses from the aforementioned risks.
                        </td>
                    </tr>  
                	<tr>
                    	<td class="number-td"><b>5.2</b></td>
                        <td><b>Indemnification of Infinox by Trading Agent</b>
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td">5.2.1</td>
                        <td>The Trading Agent agrees to indemnify and hold Infinox and its affiliates, employees and its and their successors and its and their directors, officers, employees and agents harmless from and against all claims, actions, costs including attorney's fees, arising out of or relating to any breach by the Trading Agent of any provision of this LPOA Agreement; other Agreements with Infinox and/or with the Client; the performance or non-performance of the Trading Agent's trade on the Client's Account(s); and any dispute involving the Trading Agent and the Account Holder. Infinox's rights under this paragraph are in addition to any other rights it has under other agreements with the Client.
                        </td>
                    </tr>                    
                    
                                                                                                             
  			</table>
           </div>
			<div class="right-50-div">

                <table class="print-table-css">
                	<tr>
                    	<td class="number-td">5.2.2</td>
                        <td>Infinox shall not be held responsible or liable for any miscalculation or non-payment of said fees or commissions for any reason whatsoever. This payment authorisation shall remain in effect until terminated in writing by the undersigned.
                        </td>
                    </tr>  
                	
                	<tr>
                    	<td class="number-td"><b>6</b></td>
                        <td><b>Termination</b>
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td">6.1</td>
                        <td>Authorisation and indemnity under the LPOA Agreement is continuing and will remain in full force and effect until revoked. The Client and Trading Agent may revoke or terminate the trading authority over the account at any time.
                        </td>
                    </tr> 
                	<tr>
                    	<td class="number-td">6.2</td>
                        <td>In the event that Infinox receives written notice of revocation from the Client or Trading Agent, Infinox will notify the other party. The process of revocation can take up to 24 hours from Infinox receiving the request to complete and Infinox is not liable for any resulting loss during this period.
                        </td>
                    </tr>                     
                	<tr>
                    	<td class="number-td">6.3</td>
                        <td>Infinox may terminate the Trading Agent's trading authorisation over the account at any time for any reason. In the event Infinox terminates the Trading Agent's trading authorisation over the Client's Account, Infinox will provide written notice to both the Client and the Trading Agent.
                        </td>
                    </tr>
                	<tr>
                    	<td class="number-td">6.4</td>
                        <td>If the Client requests a withdrawal and/or transfer from his/her account, and there are insufficient funds available to pay the Trading Agent's fees as a result of such request or other action taken by the Client, the Client hereby agrees and understands that such request may not be processed for the full amount requested and agree that the Client shall remain obligated to make payment to the Trading Agent for any payments due pursuant to the agreement between the Client and the Trading Agent.
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">6.5</td>
                        <td>Where this LPOA Agreement has been terminated, the client's relationship with Infinox will continue, unless expressly stated otherwise by either party, and default back to an unmanaged trading account under the original Client Agreement, accepted by the client upon completing the Infinox application form.

                        </td>
                    </tr>  
                	<tr>
                    	<td class="number-td"><b>7.</b></td>
                        <td><b>Other</b>
                        </td>
                    </tr>                    
                	<tr>
                    	<td class="number-td">7.1</td>
                        <td>Subject as hereinafter provided, neither party shall without the prior express written consent of the other party assign, novate, transfer, charge, sub-license or deal in any other manner with this LPOA Agreement or any of its rights or obligations hereunder. Notwithstanding the foregoing, Infinox may, without consent assign or transfer all of its rights and/or liabilities under this LPOA Agreement at Infinox's sole discretion.

                        </td>
                    </tr> 
                	<tr>
                    	<td class="number-td">7.2</td>
                        <td>Except for changes made in accordance with this LPOA Agreement, no deviation, whether intentional or unintentional shall constitute a modification hereof, nor constitute waiver by either party or any right hereunder.
                        </td>
                    </tr>                    
                                                                                                                                                   
           		</table>
           </div>
           
        </div>    
         <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Updated: May, 2019</td>
                        <td class="right-page-number">Page 3 of 4</td>
                	</tr>
                </tbody>
            </table>
        </div>        
        
        
        <div class="page-break"></div>         
        
        
        
		<div class="page1-div same-padding ow-padding-lpoa">
        	<p class="print-p"><b><u>Schedule 1: Fees</u></b></p>
            <div class="border-box-css width100">
            	<table class="border-box-table">
                	<tr>
                    	<td class="first-border-td"><b><u>Account Number/s</u></b></td>
                        <td class="acc-td"> </td>
                    </tr>
                </table>
                <p class="print-p"><b><u>Fee Adjustments*</u></b></p>
            	<table class="border-box-table2">
                	<tr>
                    	<td class="box-td"><div class="box-div-css">2</div></td>
                        <td class="text-td">Pips per lot (100,000) round turn (trading fee)</td>
                    </tr>
                	<tr>
                    	<td class="box-td"><div class="box-div-css">0</div></td>
                        <td class="text-td">[Commission per contract]</td>
                    </tr>                    
                </table>                
                <p class="print-p"><i>*Adjustments are increases on the usual trading spreads and/or commissions</i></p>
                <p class="print-p"><b><u>Management fee:</u></b></p>
            	<table class="border-box-table2">
                	<tr>
                    	<td class="box-td"><div class="box-div-css">0</div></td>
                        <td class="text-td">% Per Annum (Changed monthly in arrears)</td>
                    </tr>
                </table>   
                <p class="print-p"><b><u>Performance fee:</u></b></p>
            	<table class="border-box-table2">
                	<tr>
                    	<td class="box-td"><div class="box-div-css">40</div></td>
                        <td class="text-td">% (insert % amount here)</td>
                    </tr>
                </table>                     
                <table class="border-box-table">
                	<tr>
                    	<td><b>Period:</b></td>
                        <td>Monthly</td>
                    </tr>
                	<tr>
                    	<td><b>Method:</b></td>
                        <td>Profit over Period/High Water Mark</td>
                    </tr>                    
                </table>             
            </div>
            <p class="print-p"><b><u>Acknowledgements</u></b></p>
            <p class="print-p">The Client agrees that he/she/they understands and certifies that the Client will have the financial resources to enter into this LPOA Agreement and the knowledge and experience to understand the risks involved. By signing this LPOA Agreement, the Client acknowledges having read, understood and agreed to be bound by all the terms and conditions of this LPOA Agreement and agrees to their Account to be discretionary managed by the Trading Agent under a Limited Power of Attorney arrangement.</p>
            <div class="width100 overflow-auto">
            <table class="three-table">
            	<tr>
                	<td class="border-td1">
                        <p class="box-title-p box-title-p2">
                            1<sup>st</sup> CLIENT'S SIGNATURE
                        </p>  
                        <p class="print-p"><b>I, THE CLIENT, AGREE TO ALL OF THE TERMS SET OUT ABOVE AND EXECUTE AMD DELIVER THIS POWER OF ATTORNEY AS A LEGAL CONTRACT.</b></p>  
                        <p class="signature-center-p">
                            <?php
                            if($userDetails->getSignature() == 1)
                            {
                            ?>
                                <img class="signature-new-css">
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src="uploads/<?php echo $docDetails->getSignature();?>" class="signature-new-css">
                            <?php
                            }
                            ?>
                        </p>
                        <div class="width100 underline-border"></div>  
                        <p class="box-title-p box-title-p3 extra-grey-top">
                            1<sup>st</sup> CLIENT'S NAME
                        </p>                         
                        <p class="print-p-sign-name"><?php echo $userDetails->getFullname();?></p>
                        <div class="width100 underline-border"></div>                          
                        <p class="box-title-p extra-grey-top">
                            DATE
                        </p> 
                        <p class="print-p-sign">
                            <?php echo date("d-m-Y",strtotime($userDetails->getDateCreated()));?>
                        </p>
                        <div class="width100 underline-border ow-margin-bottom0"></div>                                                                  		
                    </td>
              		<td class="no-border-td1">&nbsp;
                    	
                    </td>  
                	<td class="border-td1">
                        <p class="box-title-p box-title-p2">
                            2<sup>nd</sup> CLIENT'S SIGNATURE (JOINT ACCOUNT HOLDER)
                        </p>  
                        <p class="print-p"><b>I, THE CLIENT, AGREE TO ALL OF THE TERMS SET OUT ABOVE AND EXECUTE AMD DELIVER THIS POWER OF ATTORNEY AS A LEGAL CONTRACT.</b></p>  
                        <p class="signature-center-p">
                        	<img class="signature-new-css">
                        </p>
                        <div class="width100 underline-border"></div>  
                        <p class="box-title-p box-title-p3 extra-grey-top">
                            2<sup>nd</sup> CLIENT'S NAME
                        </p>                         
                        <p class="print-p-sign-name"></p>
                        <div class="width100 underline-border"></div>                          
                        <p class="box-title-p extra-grey-top">
                            DATE
                        </p> 
                        <!-- <p class="print-p-sign">22/12/2020</p> -->
                        <p class="print-p-sign"></p>
                        <div class="width100 underline-border ow-margin-bottom0"></div>                       	
                    </td>
              		<td class="no-border-td1">&nbsp;
                    		
                    </td> 
                	<td class="border-td1">
                        <p class="box-title-p box-title-p2">
                            TRADING AGENT'S SIGNATURE
                        </p>  
                        <p class="print-p"><b>I, THE CLIENT, AGREE TO ALL OF THE TERMS SET OUT ABOVE AND EXECUTE AMD DELIVER THIS POWER OF ATTORNEY AS A LEGAL CONTRACT.</b></p>  
                        <p class="signature-center-p">
                            <!-- <img src="img/" class="signature-new-css"> -->
                            <img src="img/logo.png" class="signature-new-css">
                        </p>
                        <div class="width100 underline-border"></div>  
                        <p class="box-title-p box-title-p3 extra-grey-top">
                            TRADING AGENT'S FULL NAME
                        </p>                         
                        <p class="print-p-sign-name">Cheehaur Tan</p>
                        <div class="width100 underline-border"></div>                          
                        <p class="box-title-p extra-grey-top">
                            DATE
                        </p> 
                        <p class="print-p-sign">
                            <?php echo date("d-m-Y",strtotime($userDetails->getDateCreated()));?>
                        </p>
                        <div class="width100 underline-border ow-margin-bottom0"></div>                     	
                    </td>                                                           
                </tr>
            
            </table>
            </div>
            
        </div>       
        <div class="bottom-page-div">
        	<table class="transparent-table bottom-table black-text">
            	<tbody>
            		<tr>
                    	<td>Updated: May, 2019</td>
                        <td class="right-page-number">Page 4 of 4</td>
                	</tr>
                </tbody>
            </table>
        </div>
        
       

<style>
/* Base for label styling */
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
[type="checkbox"]:not(:checked) + label,
[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 1.35em;
  cursor: pointer;
}

/* checkbox aspect */
[type="checkbox"]:not(:checked) + label:before,
[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left: 0;
  top: 3px;
  width: 0.8em;
  height: 0.8em;
  border: 1px solid black;
  background: #fff;


}
/* checked mark aspect */
[type="checkbox"]:not(:checked) + label:after,
[type="checkbox"]:checked + label:after {
  content: '\2713\0020';
  position: absolute;
  top: .3em;
  left: .1em;
  font-size: 1em;
  line-height: 0.8;
  color: black;
  transition: all .2s;
  font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
}
/* checked mark aspect changes */
[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
/* disabled checkbox */
[type="checkbox"]:disabled:not(:checked) + label:before,
[type="checkbox"]:disabled:checked + label:before {
  box-shadow: none;
  border-color: black;
  background-color: white;
}
[type="checkbox"]:disabled:checked + label:after {
  color: black;
}
[type="checkbox"]:disabled + label {
  color: black;
}
/* accessibility */
[type="checkbox"]:checked:focus + label:before,
[type="checkbox"]:not(:checked):focus + label:before {
  border: 1px solid black;
}

/* hover style just for information */
label:hover:before {
  border: 1px solid black !important;
}


</style>
<?php include 'js.php'; ?>
<img src="img/print.png" class="print-png opacity-hover"  onclick="window.print()" alt="Print" title="Print">
</body>
</html>
