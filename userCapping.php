<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$No = 0;
$totalBonuss = 0;
$totalBonusMonthlyy = 0;
$capping = 500;

$dailyBonusDetails = getDailyBonus($conn, "WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");
$monthlyBonusDetails = getMonthlyBonus($conn, "WHERE uid = ?", array("uid"), array($uid), "s");

if ($dailyBonusDetails) {
  for ($i=0; $i <count($dailyBonusDetails) ; $i++) {
    $bonus = $dailyBonusDetails[$i]->getBonus();
    $bonusDateCreated = date('Y-m',strtotime($dailyBonusDetails[$i]->getDateCreated()));
    // if (in_array($bonusDateCreated,$dateClaimArray)) {
      // echo "Claim Already";
    // }else {
      $totalBonuss += $bonus;
    // }
  }
}

// $totalBonus = number_format($totalBonuss,4);  // daily spread

if ($monthlyBonusDetails) {
  for ($i=0; $i <count($monthlyBonusDetails) ; $i++) {
    $bonusMonthly = $monthlyBonusDetails[$i]->getBonus();
    $bonusMonthlyDateCreated = date('Y-m',strtotime($monthlyBonusDetails[$i]->getDateCreated()));
    // if (in_array($bonusMonthlyDateCreated,$dateClaimArray)) {
      // echo "sssss";
    // }else {
    $totalBonusMonthlyy += $bonusMonthly;
    // }
  }
}
$totalComm = $totalBonuss + $totalBonusMonthlyy;
$totalCommission = number_format($totalComm,4);


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/userCapping.php" />
    <link rel="canonical" href="https://victory5.co/userCapping.php" />
    <meta property="og:title" content="Capping Report  | Victory 5" />
    <title>Capping Report  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: maroon;
  }
  #total{
    text-align: center;
    color: white;
  }
</style>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/capping.png" class="middle-title-icon" alt="<?php echo _MEMBERS_CAPPING_REPORTS ?>" title="<?php echo _MEMBERS_CAPPING_REPORTS ?>">
    	<h3 id="total"><?php echo "$ ".$totalCommission." / $ ".$capping ?></h3>
    </div>	
    <div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _MEMBERS_CAPPING_REPORTS ?></h1>
    </div>
    <div class="width100 shipping-div2">
      

      <div class="search-big-div">
          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _BONUS_TYPE ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateStart" placeholder="<?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input">
          </div>

          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
              <input autocomplete="off" type="text" id="dateEnd" placeholder="<?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input">
          </div>
      </div>
      <!-- <div class="search-big-div">
          <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php// echo _MULTIBANK_SEARCH ?>" title="<?php //echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="usernameInput" placeholder="<?php //echo _MULTIBANK_SEARCH ?> <?php //echo _ADMINVIEWBALANCE_NAME ?>" class="clean pop-input fake-input">
          </div> -->

          <!-- <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php //echo _MULTIBANK_SEARCH ?>" title="<?php //echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="fromInput" placeholder="<?php //echo _MULTIBANK_SEARCH ?> <?php //echo _DAILY_FROM ?>" class="clean pop-input fake-input">
          </div> -->

          <!-- <div class="fake-input-div overflow profile-h3">
              <img src="img/search.png" class="search-png" alt="<?php //echo _MULTIBANK_SEARCH ?>" title="<?php //echo _MULTIBANK_SEARCH ?>">
              <input type="text" id="datex" placeholder="<?php //echo _MULTIBANK_SEARCH." "._DAILY_DATE ?>" value="<?php //echo date('d/m/y') ?>" class="clean pop-input fake-input">
          </div> -->
      </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_NAME ?></th> -->
                        <!-- <th><?php //echo _DAILY_FROM ?></th> -->
                        <th><?php echo _DAILY_BONUS ?></th>
                        <th><?php echo _BONUS_TYPE ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                    </tr>
                </thead>
                <tbody id="myTable">

                  <?php

                  $dailyBonusDetailss = getDailyBonus($conn, "WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");
                  $monthlyBonusDetailss = getMonthlyBonus($conn, "WHERE uid = ?", array("uid"), array($uid), "s");
                  if ($dailyBonusDetailss) {
                    for ($cnt=0; $cnt <count($dailyBonusDetailss) ; $cnt++) {
                      $No++;
                      $username = $dailyBonusDetailss[$cnt]->getUsername();
                      $bonus = $dailyBonusDetailss[$cnt]->getBonus();
                      $bonusType = 'Daily Spread';
                      $date = date('d/m/Y',strtotime($dailyBonusDetailss[$cnt]->getDateCreated()));
                      $time = date('h:i a',strtotime($dailyBonusDetailss[$cnt]->getDateCreated()));
                      if ($bonus != '0.0000') {
                      ?>
                      <tr>
                        <td><?php echo $No ?></td>
                        <!-- <td><?php //echo $username ?></td> -->
                        <td><?php echo "$ ".$bonus ?></td>
                        <td><?php echo $bonusType ?></td>
                        <td><?php echo $date ?></td>
                        <td><?php echo $time ?></td>
                      </tr> <?php
                    }
                    }
                  }
                  if ($monthlyBonusDetailss) {
                    for ($cnt=0; $cnt <count($monthlyBonusDetailss) ; $cnt++) {
                      $No++;
                      $usernameMonthly = $monthlyBonusDetailss[$cnt]->getUsername();
                      $bonusMonthly = $monthlyBonusDetailss[$cnt]->getBonus();
                      $bonusTypeMonthly = 'Monthly Profit';
                      $dateMonthly = date('d/m/Y',strtotime($monthlyBonusDetailss[$cnt]->getDateCreated()));
                      $timeMonthly = date('h:i a',strtotime($monthlyBonusDetailss[$cnt]->getDateCreated()));
                      ?>
                      <tr>
                        <td><?php echo $No ?></td>
                        <!-- <td><?php //echo $usernameMonthly ?></td> -->
                        <td><?php echo "$ ".$bonusMonthly ?></td>
                        <td><?php echo $bonusTypeMonthly ?></td>
                        <td><?php echo $dateMonthly ?></td>
                        <td><?php echo $timeMonthly ?></td>
                      </tr> <?php
                    }
                  }
                      ?>
                </tbody>
                <td colspan="7" class="ss" style="text-align: center;font-weight: bold;display: none"><?php echo _NO_CAPPING_REPORT ?></td>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>
</body>
</html>
<script>
$(document).ready(function(){
  $("#dateStart,#dateEnd").datepicker( {dateFormat: 'dd/mm/yy',showAnim: "fade"} );
  $("#usernameInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#dateStart,#dateEnd").on("change", function() {
    $("#myTable").empty();
    var value3 = $("#dateStart").val();
    var value4 = $("#dateEnd").val();
    // alert(value3);
    // $("#myTable tr").filter(function() {
    //   $(this).toggle($(this).text().toLowerCase().indexOf(value3) > -1)
    // });
    $.ajax({
      url: 'totalCapping.php',
      data: {dateStart:value3,dateEnd:value4},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var len = response.length;
        $("#myTable").empty();

        for( var m = 0; m<len; m++){
          var no = m + 1;
          var date = response[m]['date'];
          var bonus = response[m]['bonus'];
          var bonusType = response[m]['bonusType'];
          var dateCreated = response[m]['dateCreated'];
          var timeCreated = response[m]['timeCreated'];

          // alert(username);

          $("#myTable").append('<tr><td>'+no+'</td><td>$ '+bonus+'</td><td>'+bonusType+'</td><td>'+dateCreated+'</td><td>'+timeCreated+'</td></tr>');
        }
      },
      error:function(response){
        $("#myTable").empty();
        $("#myTable").append('<tr><td style="text-align: center" colspan="7"><?php echo _NO_CAPPING_REPORT ?></td></tr>');
      }
    });
    // $(".total-payout").empty();
    // $(".total-payout").text("sss");
  });
});
</script>
