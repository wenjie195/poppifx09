<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();
$dateCreated = date('Y-m-d');
$dailyBonusDetails = getDailyBonus($conn, "WHERE date_created >= ?", array("date_created"), array($dateCreated), "s");
$totalBon = 0;

if ($dailyBonusDetails) {
  for ($m=0; $m <count($dailyBonusDetails) ; $m++) {
    $bonus = $dailyBonusDetails[$m]->getBonus();
    $totalBon += $bonus;
  }
}else {
  $totalBon = 0;
}
$totalBonus = number_format($totalBon,4);

$userDetails = getUser($conn, "WHERE username != 'admin'");

$a = 1;
$historyUidReport = [];
$releasedUsername = [];

if ($userDetails) {
  for ($i=0; $i <count($userDetails) ; $i++) {

    $balance = 0;

    $uid = $userDetails[$i]->getUid();
    $oldMembers = $userDetails[$i]->getRankLoop();
    $oldMembersUsername = $userDetails[$i]->getUsername();
    $mpIdData = getMpIdData($conn,"WHERE uid =?", array("uid"), array($uid),"s");
    if ($mpIdData) {
      $balance = $mpIdData[0]->getBalance();
    }else {
      $balance = 0;
    }
if ($oldMembersUsername == 'company' || $oldMembersUsername == 'hsbc99' || $oldMembersUsername == 'pngsklim@gmail.com') {

  $historyUidReport[] = $uid;
  $releasedUsername[] = $uid;

}else {

  if ($oldMembers == 3 && $balance < 500) {
    $historyUidReport[] = $uid;
  }else {
    $historyUidReport[] = $uid;
    $releasedUsername[] = $uid;
  }

}

  }
}

// $dailyFilter = getDailyBonus($conn, "WHERE uid =?",array("uid"),array($historyUidReport),"s");
$releasedUsernameImp = implode(",",$releasedUsername);
// $releasedUsernameImp = array($releasedUsernameImp);
$releasedUsernameExp = explode(",",$releasedUsernameImp);

$historyUidReportImp = implode(",",$historyUidReport);
$historyUidReportExp = explode(",",$historyUidReportImp);

$monthlyProBon = getMonProBon($conn);
$cntAA = 1;

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
   
    <meta property="og:url" content="https://victory5.co/adminViewDailyNew.php" />
    <link rel="canonical" href="https://victory5.co/adminViewDailyNew.php" />
    <meta property="og:title" content="Member's Daily Bonus  | Victory 5" />
    <title><?php echo _DAILY_MEMBER_DAILY_BONUS ?>  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .total-payout{
    float: right;
    outline-style: dashed;
    outline-offset: 10px;
    outline-color: grey;
    margin-right: 10px;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
	<h1 class="pop-h1 text-center"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></h1>
    <div class="width100 shipping-div2">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _DAILY_FROM ?></th>
                        <th><?php echo _DAILY_RELEASED_BONUS ?></th>
                        <th><?php echo _DAILY_UNRELEASED_BONUS ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_EDIT ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_VIEW ?></th> -->
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($dailyBonusDetails)
                    {
                      for ($j=0; $j <count($historyUidReportExp) ; $j++) {
                        $dailyFilter = getDailyBonus($conn, "WHERE uid =?",array("uid"),array($historyUidReportExp[$j]),"s");

                          if ($dailyFilter) {
                            for ($k=0; $k <count($dailyFilter) ; $k++) {
                              $dailyFilterUid = $dailyFilter[$k]->getUid();
                        ?>
                            <tr>
                                <td><?php echo $a++; ?></td>
                                <td><?php echo $dailyFilter[$k]->getUsername();?></td>
                                <td><?php echo $dailyFilter[$k]->getFromWho();?></td>
                                <?php
                                if (in_array($dailyFilterUid,$releasedUsername)) {
                                  ?><td><?php echo "$ ".number_format($dailyFilter[$k]->getBonus(),4);?></td><?php
                                }else {
                                  ?><td></td> <?php
                                }
                                if (!in_array($dailyFilterUid,$releasedUsername)) {
                                  ?><td><?php echo "$ ".number_format($dailyFilter[$k]->getBonus(),4);?></td><?php
                                }else {
                                  ?><td></td> <?php
                                }
                                 ?>
                                <td><?php echo date('d/m/Y',strtotime($dailyFilter[$k]->getDateCreated())) ?></td>
                                <td><?php echo date('h:i a',strtotime($dailyFilter[$k]->getDateCreated())) ?></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_EDIT ?></a></td>
                                <!-- <td><a href="#" class="blue-link"><?php //echo _ADMINVIEWBALANCE_VIEW ?></a></td> -->
                            </tr>
                        <?php
                      }
                    }
                  }
                        ?>
                    <?php
                  }else {
                    ?><td colspan="7" style="text-align: center;font-weight: bold">No Daily Spread Report</td> <?php
                  }
                    ?>
                </tbody>
            </table>
        </div>
        <br>
      <h4 class="total-payout"><?php echo _ADMINVIEWBALANCE_PAYOUT." $ ".$totalBonus ?></h4>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
