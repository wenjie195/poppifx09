<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_type = 1");

// $conn->close();

$userCnt = getUser($conn, "WHERE user_type = 1");

$userCount = count($userCnt);

if (isset($_GET['page']))
{
	$page = $_GET['page'];
	// $limitMax = 50;
	$limitMax = 80;
	$startColumn = ($page - 1) * $limitMax;
	$userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}
else
{
	// $limitMax = 50;
	$limitMax = 80;
	$startColumn = 0;
	$userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}

$startColumn += 1; // for table no column
$pagination = $userCount / $limitMax;

// $conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminViewMember.php" />
    <meta property="og:title" content="View Member  | Victory 5" />
    <title>View Member  | Victory 5</title>
	<link rel="canonical" href="https://poppifx4u.com/adminViewMember.php" />
	<?php include 'css.php'; ?>

	<!-- <meta charset="utf-8">
	<title>Basic Tablesorter Demo</title> -->

	<!-- Demo styling -->
	<!-- <link href="docs/css/jq.css" rel="stylesheet"> -->

	<!-- jQuery: required (tablesorter works with jQuery 1.2.3+) -->
	<!-- <script src="docs/js/jquery-1.2.6.min.js"></script> -->
	<script src="js/jquery-3.5.1.min.js"></script>

	<!-- Pick a theme, load the plugin & initialize plugin -->
	<!-- <link href="dist/css/theme.default.min.css" rel="stylesheet"> -->
	<script src="dist/js/jquery.tablesorter.min.js"></script>
	<script src="dist/js/jquery.tablesorter.widgets.min.js"></script>
	<script>
	$(function(){
		$('table').tablesorter({
			widgets        : ['zebra', 'columns'],
			usNumberFormat : false,
			sortReset      : true,
			sortRestart    : true
		});
	});
	</script>

</head>

<!-- <body> -->

<body class="body">
<?php include 'adminHeader.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <div class="width100 shipping-div2">

	<!-- <table class="tablesorter">
		<thead>
			<tr>
				<th>AlphaNumeric Sort</th>
				<th>Currency</th>
				<th>Alphabetical</th>
				<th>Sites</th>
			</tr>
		</thead>
		<tbody>
			<tr><td>abc 123</td><td>&#163;10,40</td><td>Koala</td><td>http://www.google.com</td></tr>
			<tr><td>abc 1</td><td>&#163;234,10</td><td>Ox</td><td>http://www.yahoo.com</td></tr>
			<tr><td>zyx 1</td><td>&#163;99,90</td><td>Koala</td><td>http://www.mit.edu/</td></tr>
			<tr><td>zyx 12</td><td>&#163;234,10</td><td>Llama</td><td>http://www.nasa.gov/</td></tr>
		</tbody>
	</table> -->

		<div class="search-big-div">
            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput3" onkeyup="myFunctionC()" placeholder="<?php echo _MULTIBANK_SEARCH ?> MT4" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
            </div>
        </div>

		<div class="overflow-scroll-div">
			<table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
			<!-- <table class="tablesorter"> -->
				<thead>
					<tr>
						<th><?php echo _ADMINVIEWBALANCE_NO ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _JS_USERNAME ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _JS_FULLNAME ?> <img src="img/sort.png" class="sort-img"></th>
            <th>MT4 ID <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _JS_PHONE ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _JS_EMAIL ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _USERDASHBOARD_PERSONAL_SALES ?> ($) <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _USERDASHBOARD_GROUP_SALES ?> ($) <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _USERDASHBOARD_GROUP_MEMBER ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _MULTIBANK_ID_DOCUMENT ?> <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _MULTIBANK_UTILITY_BILL ?> <img src="img/sort.png" class="sort-img"></th>
            <th>LPOA <img src="img/sort.png" class="sort-img"></th>
            <th><?php echo _ADMINVIEWBALANCE_ACTION ?></th>

					</tr>
				</thead>
				<tbody>

					<?php
					if($userDetails)
					{
						for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
						{

							$mpIdData = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
							$directDownline = 0;
							$groupSales = 0;
							$personalSales = 0;
							$personalSalesUser = 0;
							$groupMember = 0;
							$referrerDetails = getWholeDownlineTree($conn, $userDetails[$cnt]->getUid(), false);
							if ($mpIdData) {
							  $personalSalesUser = $mpIdData[0]->getBalance();
							}
							$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($userDetails[$cnt]->getUid()), "s");
							$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
							$directDownlineLevel = $referralCurrentLevel + 1;
							if ($referrerDetails)
							{
							  $groupMember = count($referrerDetails);
							  for ($i=0; $i <count($referrerDetails) ; $i++)
							  {
								$currentLevel = $referrerDetails[$i]->getCurrentLevel();
								if ($currentLevel == $directDownlineLevel)
								{
								  $directDownline++;
								}
								$referralId = $referrerDetails[$i]->getReferralId();
								$downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
								if ($downlineDetails)
								{
								  $personalSales = $downlineDetails[0]->getBalance();
								  $groupSales += $personalSales;
								  $groupSalesFormat = number_format($groupSales,4);
								}
							  }
							}

						?>
							<tr>
								<td><?php echo ($startColumn++)?></td>
								<td><?php echo $userDetails[$cnt]->getUsername();?></td>
								<td><?php echo $userDetails[$cnt]->getFullname();?></td>
								<td><?php echo $userDetails[$cnt]->getMpId();?></td>
								<td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
								<td><?php echo $userDetails[$cnt]->getEmail();?></td>
                <!-- <td><?php echo number_format($personalSalesUser,2) ?></td>
                <td><?php echo number_format($groupSales,2) ?></td> -->

                <td><?php echo $personalSalesUser ;?></td>
                <td><?php echo $groupSales ;?></td>

                <td><?php echo  $directDownline ?></td>
								<td><?php echo  $groupMember ?></td>
								
								<td>
                                    <?php $idDoc = $userDetails[$cnt]->getIcBack();
                                    if($idDoc == '2')
                                    {
                                    ?>
                                        <form action="adminViewIDDoc.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($idDoc == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $utiBill = $userDetails[$cnt]->getLicense();
                                    if($utiBill == '2')
                                    {
                                    ?>
                                        <form action="adminViewBills.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($utiBill == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $sform = $userDetails[$cnt]->getSignature();
                                    if($sform == '2')
                                    {
                                    ?>
                                        <form action="adminViewSignature.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    elseif($sform == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <form action="adminUpdateCustomerDetails.php" method="POST">
                                        <button class="clean blue-ow-btn" type="submit" name="customer_id" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <?php echo _MULTIBANK_UPDATE ?>
                                        </button>
                                    </form>
                                </td>

							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="pagination pagination-pop">
	<?php for ($i=1; $i < $pagination ; $i++)
	{
	?>  
		<?php 
		if (isset($_GET['page']) && $_GET['page'] == $i)
		{
		?>
			<a class="active pagination-pop-a pagination-pop-a-active" href="<?php echo "adminViewMember_New.php?page=".$i ?>"><?php echo $i ?></a>
		<?php
		}
		else
		{
		?>
			<a class="pagination-pop-a" href="<?php echo "adminViewMember_New.php?page=".$i ?>"><?php echo $i ?></a>
		<?php
		}
		?>
	<?php
	}
	?>
	</div>

</div>

<div class="width100 same-padding footer-div">
	<p class="footer-p white-text"><?php echo _JS_FOOTER ?></p>
</div>

<!-- </div> -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

<!-- <script>
	$(function()
	{
	$("#myTable").tablesorter();
	});
</script>

<script>
	$(function()
	{
	$("#myTable").tablesorter({ sortList: [[0,0], [1,0]] });
	});
</script> -->

</body>
</html>