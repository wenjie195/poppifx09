<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $docRows = getStatus($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $docDetails = $docRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/uploadSignature.php" />
    <link rel="canonical" href="https://victory5.co/uploadSignature.php" />
    <meta property="og:title" content="Upload Signature  | Victory 5" />
    <title>Upload Signature  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'userHeader.php'; ?> -->
<?php include 'header.php'; ?>

<!-- <div class="width100 same-padding menu-distance"> -->
<!-- <div class="width100 same-padding menu-distance min-height "> -->
<div class="width100 same-padding menu-distance2 darkbg min-height">
    <div class="document-div2 document-div3 black-text">
    <!-- Upload  -->
    <div class="no-print">  
        <h1 class="upload-h1 black-text text-center menu-distance"><?php echo _UPLOAD_SIGNATURE_MAA ?></h1>
        <form action="utilities/uploadSignatureFunction.php" method="POST" enctype="multipart/form-data" id="file-upload-form" class="uploader">
            <input id="file-upload" type="file" name="file" accept="image/*" />
            <label for="file-upload" id="file-drag">
                <img id="file-image" src="#" alt="<?php echo _UPLOAD_PREVIEW ?>" title="<?php echo _UPLOAD_PREVIEW ?>" class="hidden">
                <div id="start">
                    <img src="img/upload.png" alt="<?php echo _UPLOAD ?>" title="<?php echo _UPLOAD ?>" class="upload-icon">
                    <div><?php echo _UPLOAD_SELECT_DRAG ?></div>
                    <div id="notimage" class="hidden"><?php echo _UPLOAD_PLS_SELECT_IMG ?></div>
                    <span id="file-upload-btn" class="btn btn-primary"><?php echo _UPLOAD_SELECT_A_FILE ?></span>
                </div>
            </label>
            <div class="clear"></div>
            <div class="width100 text-center">
                <button class="clean blue-button one-button-width pill-button margin-top30 margin-auto"  type="submit" name="submit"><?php echo _UPLOAD ?></button>
            </div>
        </form>
    </div>

    <div class="page-break"></div>

    </div>
</div> 

<style>
/* Base for label styling */
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
[type="checkbox"]:not(:checked) + label,
[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 1.35em;
  cursor: pointer;
}

/* checkbox aspect */
[type="checkbox"]:not(:checked) + label:before,
[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left: 0;
  top: 3px;
  width: 0.8em;
  height: 0.8em;
  border: 1px solid black;
  background: #fff;
}
/* checked mark aspect */
[type="checkbox"]:not(:checked) + label:after,
[type="checkbox"]:checked + label:after {
  content: '\2713\0020';
  position: absolute;
  top: .3em;
  left: .1em;
  font-size: 1em;
  line-height: 0.8;
  color: black;
  transition: all .2s;
  font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
}
/* checked mark aspect changes */
[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
/* disabled checkbox */
[type="checkbox"]:disabled:not(:checked) + label:before,
[type="checkbox"]:disabled:checked + label:before {
  box-shadow: none;
  border-color: black;
  background-color: white;
}
[type="checkbox"]:disabled:checked + label:after {
  color: black;
}
[type="checkbox"]:disabled + label {
  color: black;
}
/* accessibility */
[type="checkbox"]:checked:focus + label:before,
[type="checkbox"]:not(:checked):focus + label:before {
  border: 1px solid black;
}

/* hover style just for information */
label:hover:before {
  border: 1px solid black !important;
}
</style>

<?php include 'js.php'; ?>
<!-- <img src="img/print.png" class="print-png opacity-hover"  onclick="window.print()" alt="Print" title="Print"> -->
</body>
</html>