<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$docRows = getStatus($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$docDetails = $docRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/reviewUploadDoc.php" />
    <link rel="canonical" href="https://victory5.co/reviewUploadDoc.php" />
    <meta property="og:title" content="Review Document  | Victory 5" />
    <title>Review Document  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'userHeader.php'; ?> -->
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance">
    <div class="document-div2 document-div3 black-text">
        
        <div class="page1-div">
            <h2 class="white-text document-h2  black-text print-margin-top">Limited Power of Attorney<br>授权委托书</h2>
        </div>
        
        <div class="page-break"></div>  

        <div class="page1-div"> 
        <h2 class="white-text document-h2  black-text print-margin-top2">CONFIRMATION<br>确认</h2>        

        <div class="big-container-div">
            <div class="big1">
                <div class="big1-div">
                    <p class="document-p  black-text four-box-title">
                    <b>Client’s signature:<br>
                    客户签名：</b>
                    </p>
                    <div class="name-p-div">
                    <p class="right-name-p">
                    Name:<br>姓名
                    </p>
                    <p class="left-line-p name-line-p">
                    <?php echo $userDetails->getUsername();?>
                    </p>
                    </div>
                    <div class="clear"></div>
                    <div class="signature-div3">
                    <p class="right-name-p">
                    Signature:<br>签名
                    </p>
                    <p class="left-line-p">
                    <img src="uploads/<?php echo $docDetails->getSignature();?>" class="box-signature">
                    </p>  
                    </div>
                    <p class="right-name-p">
                    Date:<br>日期
                    </p>
                    <p class="left-line-p">
                    <?php echo date("d-m-Y",strtotime($docDetails->getSignatureTimeline()));?>
                    </p>  
                </div>
                <div class="big1-div big2-div">
                    <p class="document-p  black-text four-box-title">
                    <b>Money Manager’s signature:<br>
                    资金经理签：</b>
                    </p>
                    <div class="name-p-div">
                    <p class="right-name-p">
                    Name:<br>姓名
                    </p>
                    <p class="left-line-p name-line-p">
                    <img src="img/logo.png" class="box-signature">
                    </p>
                    </div>
                    <div class="clear"></div>
                    <div class="signature-div3">
                    <p class="right-name-p">
                    Signature:<br>签名
                    </p>
                    <p class="left-line-p">
                    </p>
                    </div>              
                    <div class="clear"></div>
                    <p class="right-name-p">
                    Date:<br>日期
                    </p>
                    <p class="left-line-p">
                    <?php echo date("d-m-Y",strtotime($docDetails->getSignatureTimeline()));?>
                    </p>  
                </div>
            </div>
            <div class="big1">
                <div class="big1-div bottom-div">
                    <p class="document-p  black-text four-box-title">
                    <b>Witness signature:<br>
                    见证人签名</b>
                    </p>
                    <div class="name-p-div">
                    <p class="right-name-p">
                    Name:<br>姓名
                    </p>
                    <p class="left-line-p name-line-p">
                    </p>
                    </div>
                    <div class="clear"></div>
                    <div class="signature-div3">
                    <p class="right-name-p">
                    Signature:<br>签名
                    </p>
                    <p class="left-line-p">
                    </p>  
                    </div>              
                    <div class="clear"></div>
                    <p class="right-name-p">
                    Date:<br>日期
                    </p>
                    <p class="left-line-p">
                    </p>  
                </div>            
                <div class="big1-div big2-div bottom-div">
                    <p class="document-p  black-text four-box-title">
                    <b>Witness signature:<br>
                    见证人签名</b>
                    </p>
                    <div class="name-p-div">
                    <p class="right-name-p">
                    Name:<br>姓名
                    </p>
                    <p class="left-line-p name-line-p">
                    </p>
                    </div>
                    <div class="clear"></div>
                    <div class="signature-div3">
                    <p class="right-name-p">
                    Signature:<br>签名
                    </p>
                    <p class="left-line-p">
                    </p>
                    </div>           
                    <div class="clear"></div>
                    <p class="right-name-p">
                    Date:<br>日期
                    </p>
                    <p class="left-line-p">
                    </p> 
                </div>              
            </div> 
            </div>     
        </div>   

        <div class="page-break"></div>   

        <div class="width100 text-center overflow margin-bottom50">
            <button class="clean blue-button one-button-width"  type="submit" name="submit">
            <a href="logoutMultiBank.php" class="red-link">NEXT</a>
            </button>
        </div> 

    </div>
</div> 

<style>
/* Base for label styling */
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
[type="checkbox"]:not(:checked) + label,
[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 1.35em;
  cursor: pointer;
}

/* checkbox aspect */
[type="checkbox"]:not(:checked) + label:before,
[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left: 0;
  top: 3px;
  width: 0.8em;
  height: 0.8em;
  border: 1px solid black;
  background: #fff;


}
/* checked mark aspect */
[type="checkbox"]:not(:checked) + label:after,
[type="checkbox"]:checked + label:after {
  content: '\2713\0020';
  position: absolute;
  top: .3em;
  left: .1em;
  font-size: 1em;
  line-height: 0.8;
  color: black;
  transition: all .2s;
  font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
}
/* checked mark aspect changes */
[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
/* disabled checkbox */
[type="checkbox"]:disabled:not(:checked) + label:before,
[type="checkbox"]:disabled:checked + label:before {
  box-shadow: none;
  border-color: black;
  background-color: white;
}
[type="checkbox"]:disabled:checked + label:after {
  color: black;
}
[type="checkbox"]:disabled + label {
  color: black;
}
/* accessibility */
[type="checkbox"]:checked:focus + label:before,
[type="checkbox"]:not(:checked):focus + label:before {
  border: 1px solid black;
}

/* hover style just for information */
label:hover:before {
  border: 1px solid black !important;
}


</style>
<?php include 'js.php'; ?>
<img src="img/print.png" class="print-png opacity-hover"  onclick="window.print()" alt="Print" title="Print">
</body>
</html>
