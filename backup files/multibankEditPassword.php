<?php
require_once dirname(__FILE__) . '/multibankAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/editPassword.php" />
    <meta property="og:title" content="Edit Password  | Victory 5" />
    <title>Edit Password  | Victory 5</title>
    <meta property="og:url" content="https://poppifx4u.com/editPassword.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'multibankHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">

    <!-- <form action="utilities/editPasswordFunction.php" method="POST"> -->
    <form action="utilities/multibankEditPasswordFunction.php" method="POST">
        <h1 class="h1-title white-text">Edit Password</h1>
    
        <div class="dual-input">
            <p class="input-top-text">Current Password</p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="Current Password" id="current_password" name="current_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>
        </div>
        
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text">New Password</p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="New Password"  id="new_password" name="new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text">Retype New Password</p>
            <div class="fake-input-bg">
                <input class="clean pop-input no-bg-input" type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
            </div>
        </div>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width" name="submit">Submit</button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>