<?php
require_once dirname(__FILE__) . '/multibankAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn);
// $userDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/multibankViewForm.php" />
    <meta property="og:title" content="MultiBank View  | Victory 5" />
    <title>MultiBank View  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/multibankViewForm.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<!-- <?php //include 'adminHeader.php'; ?> -->
<?php include 'multibankHeader.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text print-big-div">
<h1 class="doc-h1 opacity-hover black-text" onclick="goBack()"><img src="img/back3.png" class="back-png" alt="Go Back" title="Go Back"> Details</h1>
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <div class="top-profile-div overflow">

            <h3 class="profile-h3 ow-black-text"><?php echo _JS_FIRSTNAME ?> : <b><?php echo $userDetails[0]->getFirstname();?></b></h3>
            <h3 class="profile-h3 mid-profile-h3 ow-black-text"><?php echo _JS_LASTNAME ?> : <b><?php echo $userDetails[0]->getLastname();?></b></h3>
            <h3 class="profile-h3 ow-black-text"><?php echo _INDEX_DOB ?> : <b><?php echo $userDetails[0]->getBirthDate();?></b></h3>

            <h3 class="profile-h3 ow-black-text"><?php echo _INDEX_DOB ?> : <b><?php echo $userDetails[0]->getBirthDate();?></b></h3>
            <h3 class="profile-h3 mid-profile-h3 ow-black-text">Mobile No. : <b><?php echo $userDetails[0]->getPhoneNo();?></b></h3>
            <h3 class="profile-h3 ow-black-text no-print"><b>&nbsp;</b></h3>

            <h3 class="profile-h3 ow-black-text">Address 1 : <b><?php echo $userDetails[0]->getAddress();?></b></h3>
            <h3 class="profile-h3 mid-profile-h3 ow-black-text">Address 2 : <b><?php echo $userDetails[0]->getAddressB();?></b></h3>
            <h3 class="profile-h3 ow-black-text">Zip Code : <b><?php echo $userDetails[0]->getZipcode();?></b></h3>
            <h3 class="profile-h3 ow-black-text">Country : <b><?php echo $userDetails[0]->getCountry();?></b></h3>
			<div class="clear"></div>

            <h3 class="profile-h3 ow-black-text">Upline : 
                <b>
                    <?php 
                        $userUid = $userDetails[0]->getUid();
                        // echo "<br>";
                        $uplineDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($userUid), "s");
                        $uplineUid = $uplineDetails[0]->getReferrerId();
                        // echo "<br>";
                        $uplineData = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                        echo $uplineUsername = $uplineData[0]->getUsername();
                    ?>
                </b>
            </h3>

        </div>
		<div class="clear"></div>
        <div class="width100 overflow">
        	<div class="invite-btn blue-button text-center print-button" onclick="window.print()"><?php echo _MULTIBANK_PRINT ?></div>
        </div>    
    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>

</body>
</html>