<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/profile.php" />
    <meta property="og:title" content="Profile  | Victory 5" />
    <title>Profile  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/profile.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height profile-big-div">
	<h1 class="text-center pop-h1"><?php echo _USERHEADER_PROFILE ?></h1>
    <div class="top-profile-div overflow">

        <h3 class="profile-h3">Username : <b><?php echo $userData->getUsername();?></b></h3>
        <h3 class="profile-h3 mid-profile-h3">MT4 : <b><?php echo $userData->getMpId();?></b></h3>
        <h3 class="profile-h3"><b>&nbsp;</b></h3>

        <h3 class="profile-h3">IC Number : <b><?php echo $userData->getIcno();?></b></h3>
        <h3 class="profile-h3 mid-profile-h3"><?php echo _JS_FIRSTNAME ?> : <b><?php echo $userData->getFirstname();?></b></h3>
        <h3 class="profile-h3"><?php echo _JS_LASTNAME ?> : <b><?php echo $userData->getLastname();?></b></h3>

        <h3 class="profile-h3"><?php echo _JS_EMAIL ?> : <b><?php echo $userData->getEmail();?></b></h3>
        <h3 class="profile-h3 mid-profile-h3"><?php echo _INDEX_DOB ?> : <b><?php echo $userData->getBirthDate();?></b></h3>
        <h3 class="profile-h3">Mobile No. : <b><?php echo $userData->getPhoneNo();?></b></h3>

        <h3 class="profile-h3">Address 1 : <b><?php echo $userData->getAddress();?></b></h3>
        <h3 class="profile-h3 mid-profile-h3">Address 2 : <b><?php echo $userData->getAddressB();?></b></h3>
        <h3 class="profile-h3">Zip Code : <b><?php echo $userData->getZipcode();?></b></h3>
        <h3 class="profile-h3">Country : <b><?php echo $userData->getCountry();?></b></h3>

	</div>

    <!-- <div class="width100 profile-img-div">
    	<img src="img/profile2.png" class="profile-img" alt="<?php //echo _USERHEADER_PROFILE ?>" title="<?php //echo _USERHEADER_PROFILE ?>">
    </div> -->
</div>

<?php include 'js.php'; ?>
<!-- <?php //include 'rankIdentifySolo.php' ?> -->

</body>
</html>