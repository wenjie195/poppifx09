<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $mpid = rewrite($_POST['register_mpid']);
     $mpidStatus = "YES";
     $uid = $senderUID;

     $getUserDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $name = $getUserDetails[0]->getUsername();

     $icNumber = $getUserDetails[0]->getIcno();

     $bankAccName = $getUserDetails[0]->getBankAccName();
     $bankAccNumber = $getUserDetails[0]->getBankAccNumber();
     $bankName = $getUserDetails[0]->getBankName();

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($mpidStatus)
          {
               array_push($tableName,"mp");
               array_push($tableValue,$mpidStatus);
               $stringType .=  "s";
          }
          if($mpid)
          {
               array_push($tableName,"mp_id");
               array_push($tableValue,$mpid);
               $stringType .=  "s";
          }

          array_push($tableValue,$uid);
          $stringType .=  "s";
          $updatedMpId = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);

          if($updatedMpId)
          {
               // header('Location: ../userDashboard.php');

               if(mpIdData($conn,$uid,$name,$mpid))
               {

                 if(profitRawSharing($conn,$uid,$name,$mpid))
                 {
                    // echo "done register";
                    // header('Location: ../userDashboard.php');

                    if(equityPlData($conn,$uid,$name,$mpid))
                    {
                         // echo "done register";
                         // header('Location: ../userDashboard.php');

                         // if($user->getIcFront() == 1)

                         if($icNumber == '')
                         {
                             header('Location: ../editProfile.php');
                         }
                         else
                         {
                              // header('Location: ../userDashboard.php');

                              if($bankAccName == '')
                              {
                              header('Location: ../userBankDetails.php');
                              }
                              elseif($bankAccNumber == '')
                              {
                              header('Location: ../userBankDetails.php');
                              }
                              elseif($bankName == '')
                              {
                              header('Location: ../userBankDetails.php');
                              }
                              else
                              {
                                   header('Location: ../userDashboard.php');
                              }

                         }

                         // if($bankAccName == '')
                         // {
                         //     header('Location: ../userBankDetails.php');
                         // }
                         // elseif($bankAccNumber == '')
                         // {
                         //     header('Location: ../userBankDetails.php');
                         // }
                         // elseif($bankName == '')
                         // {
                         //     header('Location: ../userBankDetails.php');
                         // }
                         // else
                         // {
                         //      header('Location: ../userDashboard.php');
                         // }

                    }
                    else
                    {
                         // echo "fail to register via upline";
                         header('Location: ../userMp4Id.php?type=2');
                    }

                  }else {
                    header('Location: ../userMP4ID.php?type=6');
                  }

               }
               else
               {
                    // echo "fail to register via upline";
                    header('Location: ../userMp4Id.php?type=3');
               }

          }
          else
          {
               // echo "fail";
               header('Location: ../userMp4Id.php?type=4');
          }
     }
     else
     {
          header('Location: ../userMP4ID.php?type=5');
     }


}
else
{
     header('Location: ../index.php');
}

function mpIdData($conn,$uid,$name,$mpid)
{
     if(insertDynamicData($conn,"mpidrawdata",array("uid","name","mp_id"),
     array($uid,$name,$mpid),"sss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function profitRawSharing($conn,$uid,$name,$mpid)
{
     if(insertDynamicData($conn,"profitrawsharing",array("uid","name","mp_id"),
     array($uid,$name,$mpid),"sss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function equityPlData($conn,$uid,$name,$mpid)
{
     if(insertDynamicData($conn,"equityplrawdata",array("uid","name","mp_id"),
     array($uid,$name,$mpid),"sss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>
