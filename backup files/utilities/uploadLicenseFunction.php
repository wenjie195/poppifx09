<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Status.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $time = $dt->format('Y-m-d H:i:s');

     $uid = $senderUID;
     // $license = $_FILES['file']['name'];
     $license = $timestamp.$_FILES['file']['name'];
     $update = "2";

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userUsername = $userDetails[0]->getUsername();
     $username = $userUsername;
     $licenseTimeline = $time;

     // $target_dir = "../uploads/";
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file"]["name"]);
     
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");

     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$license);
     }

     // //for debugging
     // echo "<br>";
     // echo $uid."<br>";
     // echo $message_uid."<br>";

     if($license == "")
     {
          header('Location: ../uploadLicense.php');
     }
     else
     {
          if(isset($_POST['submit']))
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($license)
               {
                    array_push($tableName,"license_image");
                    array_push($tableValue,$license);
                    $stringType .=  "s";
               }     
               if($licenseTimeline)
               {
                    array_push($tableName,"license_timeline");
                    array_push($tableValue,$licenseTimeline);
                    $stringType .=  "s";
               } 

               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updatedLicense = updateDynamicData($conn,"status"," WHERE uid = ? ",$tableName,$tableValue,$stringType);

               if($updatedLicense)
               {

                    if(isset($_POST['submit']))
                    {   
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         if($update)
                         {
                              array_push($tableName,"license");
                              array_push($tableValue,$update);
                              $stringType .=  "s";
                         } 
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $imageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($imageStatusInUser)
                         {
                              header('Location: ../uploadSignature.php');
                         }
                         else
                         {
                              header('Location: ../uploadBackIC.php?type=2');
                         }
                    }
                    else
                    {
                         header('Location: ../uploadLicense.php?type=3');
                    }

               }
               else
               {
                    // echo "fail";
                    header('Location: ../uploadLicense.php?type=4');
               }
          }
          else
          {
               header('Location: ../uploadLicense.php?type=5');
          }

     }

}
else
{
     header('Location: ../index.php');
}

?>