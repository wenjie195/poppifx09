<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$uid = $_SESSION['uid'];
$countryList = getCountries($conn);

$statusDetails = getStatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$statusData = $statusDetails[0];
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/uploadDoc.php" />
    <meta property="og:title" content="Upload Document  | Victory 5" />
    <title>Upload Document  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/uploadDoc.php" />
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .upload{
    width: 500px;
    max-width: 500px;
    /* margin-right: 290px; */
  }

</style>
<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height text-center">

    <h1 class="pop-h1"><?php echo _USERHEADER_UPLOAD_DOC ?></h1>
    <h3>IC Front</h3>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload1" type="file" name="IcFront" accept="image/*" />
      <label for="file-upload1">
        <img id="image1" class="upload" src="<?php echo "uploads/".$statusData->getICFront() ?>" alt="">
        <div class="text1"></div>
      </label>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width"  type="submit" name="submitIcFront"><?php echo _UPLOAD ?></button>
        </div>
    <!-- <div class="width100">
    	<img src="img/laptop.png" class="doc-css">
    </div> -->
    <h3 class="document2">IC Back </h3>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload2" type="file" name="icBack" accept="image/*" />
      <label for="file-upload2">
        <img id="image2" class="upload" src="<?php echo "uploads/".$statusData->getICBack() ?>" alt="">
        <div class="text2"></div>
      </label>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width"  type="submit" name="submitIcBack"><?php echo _UPLOAD ?></button>
        </div>
    </form>

    <h3 class="document2">Signature</h3>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload3" type="file" name="sign" accept="image/*" />
      <label for="file-upload3">
        <img id="image3" class="upload" src="<?php echo "uploads/".$statusData->getSignature() ?>" alt="">
        <div class="text3"></div>
      </label>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width"  type="submit" name="submitSignature"><?php echo _UPLOAD ?></button>
        </div>
    </form>

    <h3 class="document2">License</h3>
    <form action="utilities/uploadExistingImageFunction.php" method="POST" enctype="multipart/form-data" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload4" type="file" name="license" accept="image/*" />
      <label for="file-upload4">
        <img id="image4" class="upload" src="<?php echo "uploads/".$statusData->getLicense() ?>" alt="">
        <div class="text4"></div>
      </label>
        <div class="width100 text-center">
          <button disabled class="clean blue-button one-button-width"  type="submit" name="submitLicense"><?php echo _UPLOAD ?></button>
        </div>
    </form>
    <!-- <div class="width100">
    	<img src="img/laptop.png" class="doc-css">
    </div> -->

</div>
<?php include 'js.php'; ?>
<script>
  $("#file-upload1").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image1").attr("src","");
    $(".text1").html('<p>'+x+'</p>');
    $("button[name='submitIcFront']").removeAttr('disabled');
  });
  $("#file-upload2").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image2").attr("src","");
    $(".text2").html('<p>'+x+'</p>');
    $("button[name='submitIcBack']").removeAttr('disabled');
  });
  $("#file-upload3").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image3").attr("src","");
    $(".text3").html('<p>'+x+'</p>');
    $("button[name='submitSignature']").removeAttr('disabled');
  });
  $("#file-upload4").change(function(e){
    var x = e.target.files[0].name;
    // alert(x);
    $("#image4").attr("src","");
    $(".text4").html('<p>'+x+'</p>');
    $("button[name='submitLicense']").removeAttr('disabled');
  });
</script>
</body>
</html>
