<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
//apply in all
define("_MAINJS_ALL_LOGOUT", "登出");
//index
define("_MAINJS_INDEX_LOGIN", "登录");
define("_MAINJS_INDEX_USERNAME", "用户名");
define("_MAINJS_INDEX_PASSWORD", "密码");
define("_INDEX_NO_YET", "还未拥有账号？点击此处注册。");
define("_INDEX_LOGIN_NOW", "已拥有账号？点击此处登入。");
define("_INDEX_IC_NO", "身份证号码");
define("_INDEX_DOB", "出生日期");
define("_INDEX_MOBILE_NO", "手机号码");
define("_INDEX_ADDRESS", "地址");
define("_INDEX_ZIPCODE", "邮政编码");
define("_INDEX_STATE", "州");
define("_INDEX_REGISTER", "注册");

//JS
define("_JS_FOOTER", "©2020 Poppifx4u 版权所有");
define("_JS_LOGIN", "登入");
define("_JS_USERNAME", "用户名");
define("_JS_PASSWORD", "密码");
define("_JS_FULLNAME", "全名");
define("_JS_NEW_PASSWORD", "新密码");
define("_JS_CURRENT_PASSWORD", "现在的密码");
define("_JS_RETYPE_PASSWORD", "再次输入密码");
define("_JS_RETYPE_REFERRER_NAME", "推荐人名字");
define("_JS_REMEMBER_ME", "记住我");
define("_JS_FORGOT_PASSWORD", "忘记密码");
define("_JS_FORGOT_TITLE", "忘记密码");
define("_JS_EMAIL", "邮箱地址");
define("_JS_SIGNUP", "注册");
define("_JS_FIRSTNAME", "名字");
define("_JS_LASTNAME", "姓氏");
define("_JS_GENDER", "性别");
define("_JS_MALE", "男");
define("_JS_FEMALE", "女");
define("_JS_BIRTHDAY", "出生日期");
define("_JS_COUNTRY", "国家");
define("_JS_MALAYSIA", "马来西亚");
define("_JS_SINGAPORE", "新加坡");
define("_JS_PHONE", "电话号码");
define("_JS_REQUEST_TAC", "申请TAC");
define("_JS_TYPE", "类型");
define("_JS_SUBMIT", "提交");
define("_JS_PLACEORDER", "下单");
define("_JS_WITHDRAW_AMOUNT", "提现金额");
define("_JS_SUCCESS", "交易成功！");
define("_JS_CLOSE", "关闭");
define("_JS_ERROR", "错误");

//UPLOAD
define("_UPLOAD_IC_FRONT", "上载身份证/护照正面");
define("_UPLOAD", "上载");
define("_UPLOAD_PREVIEW", "预览");
define("_UPLOAD_SELECT_DRAG", "选择您的文件或将它拖至此处");
define("_UPLOAD_PLS_SELECT_IMG", "请选择一张照片");
define("_UPLOAD_SELECT_A_FILE", "选择一个文件");
define("_UPLOAD_IC_BACK", "上载身份证/护照背面");
define("_UPLOAD_UTILITY_BILL_DRIVING_LICENSE", "上载水电费单或驾照");
define("_UPLOAD_SIGNATURE_MAA", "上载LPOA文件签名");

//USERDASHBOARD
define("_USERDASHBOARD_INVITATION_LINK", "邀请链接");
define("_USERDASHBOARD_COPY", "复制");
define("_USERDASHBOARD_LOGOUT", "登出");
define("_USERDASHBOARD_INVITED_BY_ME", "我所邀请的会员");
define("_USERHEADER_PROFILE", "个人资料");
define("_USERHEADER_BANK_ACC", "银行账号");
define("_USERHEADER_UPLOAD_DOC", "上载资料");
define("_USERHEADER_REFER", "推荐");
define("_USERHEADER_HIERARCHY", "团队");
define("RESET_HIERARCHY", "收回");
define("_USERDASHBOARD_COMMISSION", "佣金");
define("_USERDASHBOARD_PERSONAL_SALES", "个人业绩");
define("_USERDASHBOARD_GROUP_SALES", "团队业绩");
define("_USERDASHBOARD_DIRECT_DOWNLINE", "直属下线");
define("_USERDASHBOARD_GROUP_MEMBER", "团队成员");
define("_USERDASHBOARD_RANK", "等级");
define("_USERHEADER_BANK_DETAILS", "银行资料");

//BANKDETAILS
define("_BANKDETAILS_ACC_NAME", "银行账号名字");
define("_BANKDETAILS_ACC_NO", "银行账号号码");
define("_BANKDETAILS_ACC_TYPE", "账号类别");
define("_BANKDETAILS_BANK", "银行");
define("_BANKDETAILS_BANK_SWIFT_CODE", "银行SWIFT码");
//ADMINDASHBOARD
define("_ADMINDASHBOARD_ADD_ONS_CREDIT", "充值");
define("_ADMINDASHBOARD_UPLOAD_EQUITY_PL", "上载股本(Equity PL)");
//ADMINDASHBOARD
define("_ADMINHEADER_DASHBOARD", "概览");
define("_ADMINHEADER_INFO", "资料");
define("_ADMINHEADER_MEMBER_BALANCE", "会员的余额");

//ADMINNEWCREDIT
define("_ADMINNEWCREDIT_SELECT_FILE", "选择文件");

//ADMINVIEWBALANCE
define("_ADMINVIEWBALANCE_NO", "序");
define("_ADMINVIEWBALANCE_NAME", "名字");
define("_ADMINVIEWBALANCE_BALANCE", "余额");
define("_ADMINVIEWBALANCE_EDIT", "修改");
define("_ADMINVIEWBALANCE_VIEW", "查阅");

//MULTIBANK
define("_MULTIBANK_PRINT", "打印");
define("_MULTIBANK_VIEW", "查询");
define("_MULTIBANK_ID_DOCUMENT", "ID 文件");
define("_MULTIBANK_UTILITY_BILL", "杂费水电单");
define("_MULTIBANK_ACTION", "决策");
define("_MULTIBANK_SEARCH", "搜索");