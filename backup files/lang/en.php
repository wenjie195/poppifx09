<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_INDEX_NO_YET", "Don't have an account? Sign up here.");
define("_INDEX_LOGIN_NOW", "Already have an account? Log in here.");
define("_INDEX_IC_NO", "IC Number");
define("_INDEX_DOB", "Date of Birth");
define("_INDEX_MOBILE_NO", "Mobile No.");
define("_INDEX_ADDRESS", "Address");
define("_INDEX_ZIPCODE", "Zip Code");
define("_INDEX_STATE", "State");
define("_INDEX_REGISTER", "Register");

//JS
define("_JS_FOOTER", "©2020 Poppifx4u, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Submit");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_JS_SUCCESS", "Success");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");

//UPLOAD
define("_UPLOAD_IC_FRONT", "Upload Front View of IC/Passport");
define("_UPLOAD", "Upload");
define("_UPLOAD_PREVIEW", "Preview");
define("_UPLOAD_SELECT_DRAG", "Select a file or drag here");
define("_UPLOAD_PLS_SELECT_IMG", "Please select an image");
define("_UPLOAD_SELECT_A_FILE", "Select a file");
define("_UPLOAD_IC_BACK", "Upload Back View of IC/Passport");
define("_UPLOAD_UTILITY_BILL_DRIVING_LICENSE", "Upload Utility Bill or Driving License");
define("_UPLOAD_SIGNATURE_MAA", "Upload signature for LPOA Document");

//USERDASHBOARD
define("_USERDASHBOARD_INVITATION_LINK", "Invitation Link");
define("_USERDASHBOARD_COPY", "Copy");
define("_USERDASHBOARD_LOGOUT", "Logout");
define("_USERDASHBOARD_INVITED_BY_ME", "Members Invited by Me");
define("_USERHEADER_PROFILE", "Profile");
define("_USERHEADER_BANK_ACC", "Bank Account");
define("_USERHEADER_UPLOAD_DOC", "Upload Doc");
define("_USERHEADER_REFER", "Refer");
define("_USERHEADER_HIERARCHY", "Hierarchy");
define("RESET_HIERARCHY", "Reset");
define("_USERDASHBOARD_COMMISSION", "Commission");
define("_USERDASHBOARD_PERSONAL_SALES", "Personal Sales");
define("_USERDASHBOARD_GROUP_SALES", "Group Sales");
define("_USERDASHBOARD_DIRECT_DOWNLINE", "Direct Downline");
define("_USERDASHBOARD_GROUP_MEMBER", "Group Member");
define("_USERDASHBOARD_RANK", "Rank");
define("_USERHEADER_BANK_DETAILS", "Bank Details");

//BANKDETAILS
define("_BANKDETAILS_ACC_NAME", "Account Name");
define("_BANKDETAILS_ACC_NO", "Account Number");
define("_BANKDETAILS_ACC_TYPE", "Account Type");
define("_BANKDETAILS_BANK", "Bank");
define("_BANKDETAILS_BANK_SWIFT_CODE", "Bank SWIFT Code");
//ADMINDASHBOARD
define("_ADMINDASHBOARD_ADD_ONS_CREDIT", "Add-ons Credit");
define("_ADMINDASHBOARD_UPLOAD_EQUITY_PL", "Upload Equity PL");

//ADMINHEADER
define("_ADMINHEADER_DASHBOARD", "Dashboard");
define("_ADMINHEADER_INFO", "Info");
define("_ADMINHEADER_MEMBER_BALANCE", "Member's Balance");

//ADMINNEWCREDIT
define("_ADMINNEWCREDIT_SELECT_FILE", "Select File");

//ADMINVIEWBALANCE
define("_ADMINVIEWBALANCE_NO", "No.");
define("_ADMINVIEWBALANCE_NAME", "Name");
define("_ADMINVIEWBALANCE_BALANCE", "Balance");
define("_ADMINVIEWBALANCE_EDIT", "Edit");
define("_ADMINVIEWBALANCE_VIEW", "View");

//MULTIBANK
define("_MULTIBANK_PRINT", "Print");
define("_MULTIBANK_VIEW", "View");
define("_MULTIBANK_ID_DOCUMENT", "ID Document");
define("_MULTIBANK_UTILITY_BILL", "Utility Bill");
define("_MULTIBANK_ACTION", "Action");
define("_MULTIBANK_SEARCH", "Search");