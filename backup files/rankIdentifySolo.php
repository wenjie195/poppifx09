<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$userDet = getUser($conn,"WHERE username != 'admin' and username != 'company'");

for ($j=0; $j < count($userDet) ; $j++) {
  $uid = $userDet[$j]->getUid();
// $uid = $_SESSION['uid'];
$groupSales = 0;
$totalIndirectRankC = 0;
$totalIndirectRankB = 0;
$totalIndirectRankA = 0;
$rank = "";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s"); // get specific users
$mpIdData = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");
if ($mpIdData) {
  $personalSales = $mpIdData[0]->getBalance();
}else {
  $personalSales = 0;
}
// $personalSales = $userDetails[0]->getPersonalSales(); // get your personal sales
$userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uid),"s");
$currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
$indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level
$getWho = getWholeDownlineTree($conn, $uid,false); // get downline function

for ($i=0; $i <count($getWho) ; $i++) {

  $totalDownline = count($getWho); // get how many downline he get
  $getWho[$i]->getReferrerId();

  $downlineReferralHistory = getReferralHistory($conn,"WHERE referrer_id =?",array("referrer_id"),array($getWho[$i]->getReferrerId()),"s"); // get referral history
  // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
  $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
  // $indirectDownlineCurrentLevel = $currentLevel + 1;
  $downlineReferralId = $downlineReferralHistory[0]->getReferralId(); // get downline current level
  $downlineReferrerId = $downlineReferralHistory[0]->getReferrerId(); // get downline current level

  if ($downlineCurrentLevel > $indirectCurrentLevel) { //compare downline current level to get indirect level
    $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
    $indirectDownlineRank = $downlineDetailss[0]->getRank();
    if ($indirectDownlineRank == 'C') {
      // echo "inDirect C";
      $totalIndirectRankC++;
       // $totalIndirectRankC = count($indirectDownlineRank);
    }
    if ($indirectDownlineRank == 'B') {
    // echo  "inDirect B";
    $totalIndirectRankB++;
       // $totalIndirectRankB = count($indirectDownlineRank);
    }
    if ($indirectDownlineRank == 'A') {
      // echo "inDirect A";
      $totalIndirectRankA++;
       // $totalIndirectRankA = count($indirectDownlineRank);
    }

  }

  $downlineDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($getWho[$i]->getReferralId()),"s");
  $mpIdDataGroup = getMpIdData($conn,"WHERE uid =?",array("uid"),array($downlineDetails[0]->getUid()),"s");
  if ($mpIdDataGroup) {
    $downlinePersonalSales = $mpIdDataGroup[0]->getBalance();
  }

  if (!$downlinePersonalSales) {
    $downlinePersonalSales = 0;
  }
  // $downlinePersonalSales = $downlineDetails[0]->getPersonalSales(); // get your downline personal sales
  $groupSales += $downlinePersonalSales; // get group sales addiction of your downline personal sales

}
// echo $groupSales;
  if ($personalSales >= 5000 && $totalDownline >= 2 && $totalIndirectRankB >= 2 && $groupSales >= 100000 && !$rank) { //  if personal sales >= 5000 , 2 normal member, 2 indirect rank b member and sales group >= 100000

     $rank = "A";

  }
  if ($personalSales >= 3000 && $totalDownline >= 2 && $totalIndirectRankC >= 2 && $groupSales >= 30000 && !$rank) { // if personal sales >= 3000 , 2 normal member, 2 indirect rank c member and sales group >= 30000

     $rank = "B";

  }
  if ($personalSales >= 1000 && $totalDownline >= 2 && $groupSales >= 5000 && !$rank) { // if personal sales >= 2000 , 2 downline and sales group >= 5000

     $rank = "C";

  }
  if ($personalSales < 1000 && !$rank) { // if personal sales >= 2000 , 2 downline and sales group >= 5000

     $rank = "Normal";

  }

       $tableName = array();
       $tableValue =  array();
       $stringType =  "";
       if($rank)
       {
            array_push($tableName,"rank");
            array_push($tableValue,$rank);
            $stringType .=  "s";
       }
       array_push($tableValue,$uid);
       $stringType .=  "s";
       $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
       if($messageStatusInUser)
       {
          // echo "success";
            // header('Location: userDashboard.php');
       }
}
 ?>
