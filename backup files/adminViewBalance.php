<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$mpIdData = getMpIdData($conn);

$monthlyProBon = getMonProBon($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminViewBalance.php" />
    <meta property="og:title" content="View Member Balance  | Victory 5" />
    <title>View Member Balance  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/adminViewBalance.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <div class="width100 shipping-div2">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th>MP4 ID</th>
                        <th><?php echo _ADMINVIEWBALANCE_BALANCE ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_EDIT ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_VIEW ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($mpIdData)
                    {
                        for($cnt = 0;$cnt < count($mpIdData) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $mpIdData[$cnt]->getName();?></td>
                                <td><?php echo $mpIdData[$cnt]->getMpID();?></td>
                                <td><?php echo $mpIdData[$cnt]->getBalance();?></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_EDIT ?></a></td>
                                <td><a href="#" class="blue-link"><?php echo _ADMINVIEWBALANCE_VIEW ?></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>LEVEL</th>
                        <th>AMOUNT</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($monthlyProBon)
                    {
                        for($cntA = 0;$cntA < count($monthlyProBon) ;$cntA++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cntA+1)?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getLevel();?></td>
                                <td><?php echo $monthlyProBon[$cntA]->getAmount();?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
