<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$groupSales = 0; // initital
$groupSalesFormat = number_format(0,2); // initital
$directDownline = 0; // initital
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$userDataDet = getMpIdData($conn, "WHERE uid =?",array("uid"),array($uid), "s");

if ($userDataDet)
{
  $personal = number_format($userDataDet[0]->getBalance(),2);
}
else
{
  $personal = 0;
}

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = getReferralHistory($conn, "WHERE referrer_id = ?",array("referrer_id"),array($uid), "s");
if ($referrerDetails) 
{
  for ($i=0; $i <count($referrerDetails) ; $i++) 
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel) 
    {
      $directDownline++;
    }
    $referralId = $referrerDetails[$i]->getReferralId();
    $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    if ($downlineDetails) 
    {
      $personalSales = $downlineDetails[0]->getBalance();
      $groupSales += $personalSales;
      $groupSalesFormat = number_format($groupSales,2);
    }
  }
}

if ($getWho)
{
$groupMember = count($getWho);
}
else
{
  $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/userDashboard.php" />
    <meta property="og:title" content="User Dashboard  | Victory 5" />
    <title>User Dashboard  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/userDashboard.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <!-- <div class="spacing-div text-center">
    	<div class="width100 text-center">
    		<img src="img/invitation.png" class="invitation-img" alt="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>">
        </div>
        <div class="clear"></div>
        <input type="hidden" id="linkCopy" value="http://localhost/poppifx/register.php?referrerUID=<?php //echo $_SESSION['uid']?>">
        <h3 class="invite-h3"><b><?php //echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://localhost/poppifx/register.php?referrerUID=<?php //echo $_SESSION['uid']?></a></h3>
        <button class="invite-btn blue-button" id="copy-referral-link"><?php //echo _USERDASHBOARD_COPY ?></button>
    </div>
    <div class="clear"></div> -->

    <div class="invite-div">
		<h3 class="text-center"><?php echo $userData->getUsername();?> (<?php echo _USERDASHBOARD_RANK ?> : <?php echo $userDetails[0]->getRank() ?>)</h3>
	</div>
    <div class="width100">
    	<div class="five-div-width div-css">
        	<img src="img/commission.png" class="five-icon" alt="<?php echo _USERDASHBOARD_COMMISSION ?>" title="<?php echo _USERDASHBOARD_COMMISSION ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_COMMISSION ?></p>
            <p class="five-div-amount">RM 0.00</p>
        </div>
    	<div class="five-div-width div-css second-five-div">
        	<img src="img/personal-sales.png" class="five-icon" alt="<?php echo _USERDASHBOARD_PERSONAL_SALES ?>" title="<?php echo _USERDASHBOARD_PERSONAL_SALES ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_PERSONAL_SALES ?></p>
            <p class="five-div-amount"><?php echo "RM ".$personal ?></p>
            <!-- <p class="five-div-amount">RM 0</p> -->
        </div>
    	<div class="five-div-width div-css">
        	<img src="img/group-sales.png" class="five-icon" alt="<?php echo _USERDASHBOARD_GROUP_SALES ?>" title="<?php echo _USERDASHBOARD_GROUP_SALES ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_GROUP_SALES ?></p>
            <p class="five-div-amount"><?php echo "RM ".$groupSalesFormat ?></p>
            <!-- <p class="five-div-amount">RM 0</p> -->
        </div>
     	<div class="five-div-width div-css second-five-div second-five-div2">
        	<img src="img/direct-downline.png" class="five-icon" alt="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>" title="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?></p>
            <p class="five-div-amount"><?php echo $directDownline ?></p>
            <!-- <p class="five-div-amount">0</p> -->
        </div>
    	<div class="five-div-width div-css">
        	<img src="img/group-member.png" class="five-icon" alt="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>" title="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_GROUP_MEMBER ?></p>
            <p class="five-div-amount"><?php echo $groupMember ?></p>
            <!-- <p class="five-div-amount">0</p> -->
        </div>
    </div>


</div>

<?php include 'js.php'; ?>
<!-- <?php //include 'rankIdentifySolo.php' ?> -->

</body>
</html>