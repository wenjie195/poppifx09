<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/register.php" />
    <meta property="og:title" content="Register  | Victory 5" />
    <title>Register  | Victory 5</title>
    <meta property="og:url" content="https://poppifx4u.com/register.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">

    <form action="utilities/registerFunction.php" method="POST">
    <h1 class="h1-title white-text"><?php echo _JS_SIGNUP ?></h1>


        <?php 
        // Program to display URL of current page. 
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
        $link = "https"; 
        else
        $link = "http"; 

        // Here append the common URL characters. 
        $link .= "://"; 

        // Append the host(domain name, ip) to the URL. 
        $link .= $_SERVER['HTTP_HOST']; 

        // Append the requested resource location to the URL 
        $link .= $_SERVER['REQUEST_URI']; 

        // Print the link 
        // echo $link; 
        ?> 

        <?php
        $str1 = $link;
        // $referrerUidLink = str_replace( 'http://localhost/poppifx/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'https://localhost/poppifx/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'http://vidatechft.com/poppifx/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'https://bossinternational.asia/poppifx/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'https://ichibangame.com/poppifx/register.php?referrerUID=', '', $str1);

        $referrerUidLink = str_replace( 'https://poppifx4u.com/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'https://vidatechft.com/poppifx/register.php?referrerUID=', '', $str1);
        // echo $referrerUidLink;
        ?>

    
	<p class="width100 signup-p">	
		<a href="index.php" class="white-link forgot-a"><?php echo _INDEX_LOGIN_NOW ?></a>
	</p>      
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" id="register_email" name="register_email" required>
        </div>
        
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_DOB ?></p>
            <input class="clean pop-input" type="date" placeholder="<?php echo _INDEX_DOB ?>" id="register_dob" name="register_dob" required>        
        
           
		</div>
        <div class="dual-input second-dual-input">
 			<p class="input-top-text"><?php echo _INDEX_IC_NO ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_IC_NO ?>" id="register_icno" name="register_icno" required>
        </div> 
        <div class="clear"></div>       
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required>
        </div>

        
        <div class="clear"></div>

        <!-- <div class="dual-input second-dual-input"> -->
        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_MOBILE_NO ?>" id="register_mobileno" name="register_mobileno" required>
        </div>
        <div class="clear"></div>
		<div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?>" id="register_address" name="register_address" required>
        </div>
        <div class="clear"></div>
        <div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?> 2</p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?> 2" id="register_addresstwo" name="register_addresstwo" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_ZIPCODE ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ZIPCODE ?>" id="register_zipcode" name="register_zipcode" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <select class="clean pop-input" id="register_country" name="register_country" required>

                <option>Please Select A Country</option>
                <?php
                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                {
                ?>
                <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                ?>"> 
                <?php 
                echo $countryList[$cntPro]->getEnName(); //take in display the options
                ?>
                </option>
                <?php
                }
                ?>

            </select>
        </div>

        <input class="clean pop-input" type="hidden" value="-" id="register_state" name="register_state" readonly>
        <!-- <input class="clean pop-input" type="hidden" value="<?php //echo $referrerUidLink ?>" id="upline_uid" name="upline_uid" readonly> -->

        <input class="clean pop-input" type="text" value="<?php echo $referrerUidLink ?>" id="upline_uid" name="upline_uid">

        <div class="clear"></div>
		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width" name="register"><?php echo _INDEX_REGISTER ?></button>
        </div>
    </form>

</div>
<?php include 'js.php'; ?>
</body>
</html>