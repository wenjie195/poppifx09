<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div class="width100 same-padding footer-div">
	<p class="footer-p white-text">©<?php echo $time;?> <?php echo _JS_FOOTER ?></p>
</div>
<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/headroom.js"></script>
<script src="js/main.js"></script>
<link href='https://code.jquery.com/ui/1.12.1/themes/dot-luv/jquery-ui.css' rel='stylesheet'>
<script src="js/jquery-ui.js"></script>
<?php echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>

<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <div class="clear"></div>
    <h1 class="menu-h1 white-text" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container white-text" id="noticeMessage">Message Here</div>
  </div>
</div>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>
	<script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
<script>
// File Upload
//
function ekUpload(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );

    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };

        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

<script type="text/javascript" src="js/rolldate.min.js"></script>
<script>
		window.onload = function() {



			new Rolldate({
				el: '#register_dob',
				format: 'YYYY-MM-DD',
				beginYear: 1900,
				endYear: 2100,
				init: function() {
					console.log('start');
				},
				moveEnd: function(scroll) {
					console.log(scroll)
					console.log('end');
				},
				confirm: function(date) {
					console.log(date)
					console.log('confirm');
				},
				cancel: function() {
					console.log('cancel');
				}
			})
			new Rolldate({
				el: '#update_dob',
				format: 'YYYY-MM-DD',
				beginYear: 1900,
				endYear: 2100,
				init: function() {
					console.log('start');
				},
				moveEnd: function(scroll) {
					console.log(scroll)
					console.log('end');
				},
				confirm: function(date) {
					console.log(date)
					console.log('confirm');
				},
				cancel: function() {
					console.log('cancel');
				}
			})
}
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionD()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionE()
{
    var x = document.getElementById("confirmPassword");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
  $(function(){
    var click;

		window.onbeforeunload = function() {	// prevent reload alert
 		if(click == 1) {
   return 'Request in progress....are you sure you want to continue?';
 		}
	};

		var type = $("input[name='getValue']").val();
		var session = $("input[name='getValueSession']").val();
		if (type == 11 && session == 1) {
			$("#frmExcelImport").slideUp(function(){
				$("#frmExcelImport").hide();
			});
		}
    $("#Rank").click(function(){
			click = 1;
      $("#loadingAnimate").show();
      $("#buttonName").text("Loading 1 of 3...");
      $.ajax({
        url: 'rankIdentify.php',
        // data : '{}',
        type: 'post',
        // dataType: 'json',

        success:function(response){
          // alert("done");
          $("#buttonName").text("Loading 2 of 3...");
          $.ajax({
            url: 'rankIdentify.php',
            type: 'post',
            success:function(response){
              $("#buttonName").text("Loading 3 of 3...");
              $.ajax({
                url: 'rankIdentify.php',
                type: 'post',
                success:function(response){
                  putNoticeJavascript("Notice !! ","Rank Successfully Updated.");
                  $("#buttonName").text("Done Update");
                  $("#loadingAnimate").hide();
                  $("#Rank").prop("disabled",true);
                  $("#Rank").css("background-color","#2c186f");
									click = 0;
                }
              });
            }
          });
        }
      });
    });

    $("#Daily").click(function(){
      click = 1;
      $("#loadingAnimate2").show();
      $("#buttonName2").text("Loading 1 of 2...");
      $.ajax({
        url: 'dailyBonus.php',
        // data : '{}',
        type: 'post',
        // dataType: 'json',

        success:function(response){
          // alert("done");
					$("#buttonName2").text("Loading 2 of 2...");
					$.ajax({
						url: 'utilities/releaseUnreleaseBonus.php',
						type: 'post',
						success:function(response){
							putNoticeJavascript("Notice !! ","Daily Spread Successfully Updated.");
							$("#buttonName2").text("Done Update");
							$("#loadingAnimate2").hide();
							$("#Daily").prop("disabled",true);
							$("#Daily").css("background-color","#2c186f");
							click = 0;
						}
					});
        }
      });
    });
		var type2 = $("input[name='getValue2']").val();
		if (type2 == 10) {
			$("#frmExcelImport2").slideUp(function(){
				$("#frmExcelImport2").hide();
			});
		}
		$("#Monthly").click(function(){
			click = 1;
			$("#loadingAnimate3").show();
			$("#buttonName3").text("Loading 1 of 1...");
			$.ajax({
				url: 'monthlyProfit.php',
				// data : '{}',
				type: 'post',
				// dataType: 'json',

				success:function(response){
					// alert("done");
					$("#loadingAnimate3").hide();
					$("#buttonName3").text("Done Update");
					$("#Monthly").prop("disabled",true);
					$("#Monthly").css("background-color","#2c186f");
					putNoticeJavascript("Notice !! ","Monthly Profit Successfully Updated.");
					click = 0;
				}
			});
		});
		$("#Reset").click(function(){
			var confirmReset = confirm("The Reset Report Can't be Restored!");
			var resetBtn = $("#Reset").val();
			if (confirmReset == true) {
				click = 1;
				$("#loadingAnimateDelete").show();
				$("#buttonNameDelete").text("Loading...");
				$.ajax({
					url: 'utilities/resetDailySpread.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDelete").hide();
						// setTimeout(function(){
						$("#buttonNameDelete").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Daily Spread.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});
		$("#Reset2").click(function(){
			var confirmReset2 = confirm("The Reset Report Can't be Restored!");
			var resetBtn2 = $("#Reset2").val();
			if (confirmReset2 == true) {
				click = 1;
				$("#loadingAnimateDelete2").show();
				$("#buttonNameDelete2").text("Loading...");
				$.ajax({
					url: 'utilities/resetMonthlyProfit.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDelete2").hide();
						// setTimeout(function(){
						$("#buttonNameDelete2").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Monthly Profit.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});

		$("#ResetMembers").click(function(){
			var confirmResetMember = confirm("The Reset Balance Can't be Restored!");
			var resetBtnMember = $("#ResetMembers").val();
			if (confirmResetMember == true) {
				click = 1;
				$("#loadingAnimateDeleteMembers").show();
				$("#buttonNameDeleteMembers").text("Loading...");
				$.ajax({
					url: 'utilities/resetBalance.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDeleteMembers").hide();
						// setTimeout(function(){
						$("#buttonNameDeleteMembers").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Member's Balance.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});

		$(".reset-checkbox").change(function(){
			if ($(".reset-checkbox").is(':checked')) {
				$("#Reset").fadeToggle();
			}
			if ($(".reset-checkbox").is(':not(:checked)')) {
				$("#Reset").fadeToggle();
			}
		});

		if ($(".reset-checkbox").is(':checked')) {
			$("#Reset").show();
		}
		else if ($(".reset-checkbox").is(':not(:checked)')) {
			$("#Reset").hide();
		}
		$("#nameCheck").click(function(){
			if ($(".reset-checkbox").is(':checked')) {
				$(".reset-checkbox").prop("checked",false);
				$("#Reset").fadeToggle();
			}else if ($(".reset-checkbox").is(':not(:checked)')) {
				$(".reset-checkbox").prop("checked",true);
				$("#Reset").fadeToggle();
			}
		});
		$(".reset-checkbox2").change(function(){
			if ($(".reset-checkbox2").is(':checked')) {
				$("#monthlyProfitBtn").toggle();
				$("#Reset2").fadeToggle();
			}
			if ($(".reset-checkbox2").is(':not(:checked)')) {
				$("#monthlyProfitBtn").fadeToggle();
				$("#Reset2").toggle();
			}
		});
		$("#nameCheck2").click(function(){
			if ($(".reset-checkbox2").is(':checked')) {
				$(".reset-checkbox2").prop("checked",false);
				$("#monthlyProfitBtn").fadeToggle();
				$("#Reset2").toggle();
			}else if ($(".reset-checkbox2").is(':not(:checked)')) {
				$(".reset-checkbox2").prop("checked",true);
				$("#monthlyProfitBtn").toggle();
				$("#Reset2").fadeToggle();
			}
		});

		$(".reset-checkboxMembers").change(function(){
			if ($(".reset-checkboxMembers").is(':checked')) {
				$("#Rank").toggle();
				$("#ResetMembers").fadeToggle();
			}
			if ($(".reset-checkboxMembers").is(':not(:checked)')) {
				$("#Rank").fadeToggle();
				$("#ResetMembers").toggle();
			}
		});

		if ($(".reset-checkbox").is(':checked')) {
			$("#Reset").show();
		}
		else if ($(".reset-checkbox").is(':not(:checked)')) {
			$("#Reset").hide();
		}

		$("#nameCheckMembers").click(function(){
			if ($(".reset-checkboxMembers").is(':checked')) {
				$(".reset-checkboxMembers").prop("checked",false);
				$("#Rank").fadeToggle();
				$("#ResetMembers").toggle();
			}else if ($(".reset-checkboxMembers").is(':not(:checked)')) {
				$(".reset-checkboxMembers").prop("checked",true);
				$("#Rank").toggle();
				$("#ResetMembers").fadeToggle();
			}
		});
  });
</script>

<script id="rendered-js">
$("#menu-toggle").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});
$("#menu-toggle-2").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled-2");
  $('#menu ul').hide();
});

function initMenu() {
  $('#menu ul').hide();
  $('#menu ul').children('.current').parent().show();
  //$('#menu ul:first').show();
  $('#menu li a').click(
  function () {
    var checkElement = $(this).next();
    if (checkElement.is('ul') && checkElement.is(':visible')) {
      return false;
    }
    if (checkElement.is('ul') && !checkElement.is(':visible')) {
      $('#menu ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
      return false;
    }
  });

}
$(document).ready(function () {
  initMenu();
});
//# sourceURL=pen.js
    </script>
  <script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#06f4f4',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>
