<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--
    <meta property="og:url" content="https://poppifx4u.com/hierarchy.php" />
    <link rel="canonical" href="https://poppifx4u.com/hierarchy.php" />-->
    <meta property="og:title" content="Hierarchy  | Victory 5" />
    <title>Hierarchy  | Victory 5</title>
   
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'userHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
	<h1 class="pop-h1"><?php echo _USERHEADER_HIERARCHY ?></h1>

         <div class="left-invite-div">
            <?php
            $conn = connDB();
            if($getWho)
            {
                // echo '<ul name="menu">';
                echo '<ul>';
                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    if($thisPerson->getCurrentLevel() == $lowestLevel)
                    {
                        // echo '<li id="'.$thisPerson->getReferralId().'">'.$thisPerson->getReferralId().'</li>';
                        echo '<li name="top" value="'.$thisPerson->getReferralId().'" id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().'</li>';
                    }
                }

                // echo '<ul>';
                // $lowestLevel = $getWho[0]->getCurrentLevel();
                // foreach($getWho as $thisPerson)
                // {
                //     $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                //     $thisTempUser = $tempUsers[0];
                //     echo '
                //     <script type="text/javascript">
                //     var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                //     div.innerHTML += "<ul style=\'display: none\' class=\'hide\' name=\'ul-'.$thisPerson->getReferrerId().'\'><li name=\'bot\' value=\''.$thisPerson->getReferralId().'\' id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                //     </script>
                //     ';
                // }
                // echo '</ul>';

                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    echo '
                    <script type="text/javascript">
                    var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                    div.innerHTML += "<ul name=\'ul-'.$thisPerson->getReferrerId().'\'><li name=\'bot\' value=\''.$thisPerson->getReferralId().'\' id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                    </script>
                    ';
                }
                echo '</ul>';

            }
            $conn->close();
            ?>
            <!-- <a href="<?php //echo $_SERVER['REQUEST_URI']; ?>"><button type="button" class="invite-btn blue-button" name="button">Reset</button> </a> -->
            <!-- <button type="button" class="invite-btn blue-button" name="button"><?php echo RESET_HIERARCHY ?></button> -->
        </div>

        <!-- <div class="right-invite-div">
        	<img src="img/invited.png" class="invited-img" alt="<?php //echo _USERDASHBOARD_INVITED_BY_ME ?>" title="<?php //echo _USERDASHBOARD_INVITED_BY_ME ?>">
        </div> -->

</div>
<?php //include 'rankIdentifySolo.php' ?>
<?php include 'js.php'; ?>
<!-- <script>
  $(function(){
    $("ul > li").click(function(){
      var uid = $(this).attr("value");
        $("ul[name='ul-"+uid+"']").slideDown(function(){
          $(this).show();
        });
      });
  $("button[name='button']").click(function(){
    $("ul .hide").hide();
  });
    });
</script> -->

</body>
</html>
