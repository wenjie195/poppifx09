<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$newMt4id = rewrite($_POST['add_mt4id']);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"), array($uid), "s");

$addmt4idDB = insertDynamicData($conn, "mpidrawdata", array("uid","name","mp_id"), array($uid,$userDetails[0]->getUsername(),$newMt4id), "sss");
$addmt4idIIDB = insertDynamicData($conn, "equityplrawdata", array("uid","name","mp_id"), array($uid,$userDetails[0]->getUsername(),$newMt4id), "sss");

if ($addmt4idDB && $addmt4idIIDB) {
  $_SESSION['messageType'] = 1;
  header('location: ../mt4idDetails.php?type=1');
}else {
  $_SESSION['messageType'] = 1;
  header('location: ../mt4idDetails.php?type=2');
}
 ?>
