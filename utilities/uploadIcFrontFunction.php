<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Status.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $time = $dt->format('Y-m-d H:i:s');

     $uid = $senderUID;
     $validationIcFront = $_FILES['file']['name'];
     $icfront = $timestamp.$_FILES['file']['name'];
     $update = "2";

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userUsername = $userDetails[0]->getUsername();
     $username = $userUsername;
     $icfrontTimeline = $time;

     // $target_dir = "../uploads/";
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file"]["name"]);
     
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");

     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$icfront);
     }

     // //for debugging
     // echo "<br>";
     // echo $uid."<br>";
     // echo $message_uid."<br>";

     if($validationIcFront != "")
     {
          if($icfront == "")
          {
               header('Location: ../uploadFrontIC.php');
          }
          else
          {
     
               if(submitImage($conn,$uid,$username,$icfront,$icfrontTimeline))
               {
                    // header('Location: ../viewMessage.php?type=1');
                    // echo "uploaded";
     
                    if(isset($_POST['submit']))
                    {   
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         if($update)
                         {
                              array_push($tableName,"icfront");
                              array_push($tableValue,$update);
                              $stringType .=  "s";
                         } 
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($messageStatusInUser)
                         {
                              header('Location: ../uploadBackIC.php');
                         }
                         else
                         {
                              header('Location: ../uploadFrontIC.php?type=2');
                         }
                    }
                    else
                    {
                    header('Location: ../uploadFrontIC.php?type=3');
                    }
     
               }
               else
               {
                    header('Location: ../uploadFrontIC.php?type=4');
                    // echo "fail";
               }
     
          }
     }
     else
     {
          echo "no Front IC";
     }

     // if($icfront == "")
     // {
     //      header('Location: ../uploadFrontIC.php');
     // }
     // else
     // {

     //      if(submitImage($conn,$uid,$username,$icfront,$icfrontTimeline))
     //      {
     //           // header('Location: ../viewMessage.php?type=1');
     //           // echo "uploaded";

     //           if(isset($_POST['submit']))
     //           {   
     //                $tableName = array();
     //                $tableValue =  array();
     //                $stringType =  "";
     //                if($update)
     //                {
     //                     array_push($tableName,"icfront");
     //                     array_push($tableValue,$update);
     //                     $stringType .=  "s";
     //                } 
     //                array_push($tableValue,$uid);
     //                $stringType .=  "s";
     //                $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     //                if($messageStatusInUser)
     //                {
     //                     header('Location: ../uploadBackIC.php');
     //                }
     //                else
     //                {
     //                     header('Location: ../uploadFrontIC.php?type=2');
     //                }
     //           }
     //           else
     //           {
     //           header('Location: ../uploadFrontIC.php?type=3');
     //           }

     //      }
     //      else
     //      {
     //           header('Location: ../uploadFrontIC.php?type=4');
     //           // echo "fail";
     //      }

     // }

}
else
{
     header('Location: ../index.php');
}

function submitImage($conn,$uid,$username,$icfront,$icfrontTimeline)
{
     if(insertDynamicData($conn,"status",array("uid","username","icfront_image","icfront_timeline"),
     array($uid,$username,$icfront,$icfrontTimeline),"ssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

?>