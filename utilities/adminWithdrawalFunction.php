<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$id = rewrite($_POST['id']);
$name = rewrite($_POST['name']);
$success = 0;

// $withdrawalDetails = getWithdrawal($conn, "WHERE")
if ($name == 'approve') {
  $status = 'APPROVED';
}elseif ($name == 'reject') {
  $status = 'REJECTED';
}


if ($name) {

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($status)
  {
      array_push($tableName,"status");
      array_push($tableValue,$status);
      $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "i";
  $balancedUpdated = updateDynamicData($conn,"withdrawal"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($balancedUpdated)
  {
    if ($status == 'APPROVED') {
      $success = "Withdraw Amount Has Been Approve";
    }elseif ($status == 'REJECTED') {
      $success = "Withdraw Amount Has Been Reject";
    }
  }
  if ($success) {
    $successArray[] = array("success" => $success);
  }

}

echo json_encode($successArray);
 ?>
