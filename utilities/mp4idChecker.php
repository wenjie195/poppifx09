<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/MpIdData.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$mt4idInput = $_POST['mt4idVal'];

$mpIdData = getMpIdData($conn, "WHERE mp_id =?",array("mp_id"),array($mt4idInput), "s");

if (!$mpIdData) {
  $success = 1;
}else {
  $success = 2;
}
$sucessArray[] = array("success" => $success);

echo json_encode($sucessArray);
 ?>
