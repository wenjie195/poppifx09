<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/PoolFund.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$id = rewrite($_POST['id']);
$poolFund = rewrite($_POST['pool_fund']);

if (isset($_POST['editPoolFund'])) {

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($poolFund || !$poolFund)
  {
      array_push($tableName,"pool_fund");
      array_push($tableValue,$poolFund);
      $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "i";
  $balancedUpdated = updateDynamicData($conn,"pool_fund"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($balancedUpdated)
  {
      $_SESSION['messageType'] = 1;
      header('Location: ../adminDashboard.php?type=1');
  }

}
 ?>
