<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","firstname","lastname","icno","birth_date","country","phone_no","address","address_two","zipcode","state","user_type","password","salt"),
          array($uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt),"sssssssssssssiss") === null)
     {
          // echo "gg";
          // header('Location: ../index.php?promptError=1');
          echo "<script>alert('Error during registration, please try again later !');window.location='../index.php'</script>";
          // echo "<script>alert('register details has been used by others');window.location='../register.php?referrerUID=.'$sponsorID'.</script>";
          // header('Location: https://poppifx4u.com/register.php?referrerUID=.$sponsorID.');

     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function sendEmailForVerification($uid)
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.victory5.co";
     $verifyUser_usernameThatSendEmail = "noreply@victory5.co";                   // Sender Acc Username
     $verifyUser_password = "8z0xxO548roP";                                              // Sender Acc Password

     $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
     $verifyUser_port = 465;                                                              // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@victory5.co";                    // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@victory5.co";                        // Sender Email


     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Welcome To Victory 5";

     $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>";                      // Body
     $verifyUser_body .="<p>You have successfully registered for Victory 5.</p>";
     $verifyUser_body .="<p>Here are your temporary login details :</p>";
     $verifyUser_body .="<p>https://victory5.co/</p>";
     $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
     $verifyUser_body .="<p>Password : 333666</p>";
     $verifyUser_body .="<p>Thank you.</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}



if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $firstname = rewrite($_POST['register_firstname']);
     $lastname = rewrite($_POST['register_lastname']);
     $icno = rewrite($_POST['register_icno']);

     $dob = rewrite($_POST['register_dob']);
     $country = rewrite($_POST['register_country']);
     $phoneNo = rewrite($_POST['register_mobileno']);
     $address = rewrite($_POST['register_address']);
     $addresstwo = rewrite($_POST['register_addresstwo']);
     $zipcode = rewrite($_POST['register_zipcode']);
     $state = rewrite($_POST['register_state']);
     $userType = "1";

     $sponsorID = rewrite($_POST['upline_uid']);

     // $register_password = rewrite($_POST['register_password']);
     // $register_retype_password = rewrite($_POST['register_retype_password']);

     // $register_password = "123321";
     // // $register_password_validation = strlen($register_password);
     // $register_retype_password = "123321";

     $register_password = "333666";
     // $register_password_validation = strlen($register_password);
     $register_retype_password = "333666";

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $fullName."<br>";
     // echo $nationality."<br>";
     // echo $userType."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
     $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     $usernameDetails = $usernameRows[0];

     // $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icno),"s");
     // $icDetails = $icRows[0];

     // $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
     // $userEmailDetails = $userEmailRows[0];

     // $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_mobileno']),"s");
     // $userPhoneDetails = $userPhoneRows[0];

     // if (!$usernameDetails && !$icDetails)
     if (!$usernameDetails)
     {
          if($sponsorID)
          {
               $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
               if($referrerUserRows)
               {
                    $referrerUid = $referrerUserRows[0]->getUid();
                    $referrerName = $referrerUserRows[0]->getUsername();
                    // $referralUid = $referrerUserRows[0]->getUid();
                    $referralName = $username;
                    $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                    $currentLevel = 1;
                    $getUplineCurrentLevel = 1;

                    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                    if($referralHistoryRows)
                    {
                         $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                         $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                    }
                    $referralNewestRows = getReferralHistory($conn,"WHERE referral_id = ?", array("referral_id"),array($referrerUid), "s");
                    if($referralNewestRows)
                    {
                         $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                         // if (!$userEmailDetails && !$userPhoneDetails && !$usernameDetails)
                         // if (!$userEmailDetails && !$icDetails)
                         // {
                              if(registerNewUser($conn,$uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt))
                              {

                                   if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                   {
                                        // echo "done register";
                                        // echo "<script>alert('Register Success !');window.location='../userDashboard.php'</script>";
                                        sendEmailForVerification($uid);
                                        $referralUid = $uid;
                                        $_SESSION['uid'] = $uid;
                                        $_SESSION['usertype_level'] = 1;
                                        header('Location: ../uploadFrontIC.php');
                                   }
                                   else
                                   {
                                        // echo "fail to register via upline";
                                        echo "<script>alert('fail to register with upline');window.location='../index.php'</script>";
                                   }

                              }
                              else
                              {
                                   // echo "fail to register";
                                   echo "<script>alert('Fail To Register !');window.location='../index.php'</script>";
                              }
                         // }
                         // else
                         // {
                         //      // echo "fail to register register";
                         //      echo "<script>alert('register details has been used by others');window.location='../index.php'</script>";
                         // }

                    }
                    else
                    {
                         // echo "register error with referral ";
                         echo "<script>alert('register error with referral !');window.location='../index.php'</script>";
                    }

               }
               else
               {
                    // echo "unable to find related data sponsor ID!!";
                    echo "<script>alert('unable to find related data sponsor ID!!');window.location='../index.php'</script>";
               }
          }
          else
          {
               // echo "sponsor ID is unavailible !!";
               echo "<script>alert('sponsor ID is unavailible !!');window.location='../index.php'</script>";
          }
     }
     else
     {
          // echo "fail to register register";
          // echo "<script>alert('register details has been used by others');window.location='../index.php'</script>";

          echo "<script>alert('register details has been used by others'); window.history.go(-1);</script>";
     }

}
else
{
     header('Location: ../index.php');
}

?>
