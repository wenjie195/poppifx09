<?php
if (session_id() == "")
{
    session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


$conn = connDB();

$conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        // $email = rewrite($_POST['email']);

        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        // $userRows = getUser($conn," WHERE username = ? ",array("username"),array($email),"s");

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

                if($finalPassword == $user->getPassword())
                {
                    // if(isset($_POST['remember-me']))
                    // {

                    //     // setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else
                    // {
                    //     // setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype_level'] = $user->getUserType();
                    $_SESSION['login_type'] = $user->getLoginType();

                    // $icNumber = $user->getIcno();
                    $bankAccName = $user->getBankAccName();
                    $bankAccNumber = $user->getBankAccNumber();
                    $bankName = $user->getBankName();
 
                    // if($user->getLoginType() == 1)
                    // {

                        if($user->getUserType() == 0)
                        {
                            header('Location: ../adminDashboard.php');
                            // echo "admin page";
                        }
                        elseif($user->getUserType() == 2)
                        {
                            header('Location: ../multibankMember.php');
                        }
                        else
                        {
                            // echo "user page";
                            // header('Location: ../userDashboard.php');

                            if($user->getIcFront() == 1)
                            {
                                // echo "no ic front";
                                header('Location: ../uploadFrontIC.php');
                            }
                            elseif($user->getIcBack() == 1)
                            {
                                // echo "no ic back";
                                header('Location: ../uploadBackIC.php');
                            }
                            elseif($user->getLicense() == 1)
                            {
                                // echo "no signature";
                                header('Location: ../uploadLicense.php');
                            }
                            elseif($user->getSignature() == 1)
                            {
                                // echo "no license";
                                header('Location: ../uploadSignature.php');
                            }

                            // elseif($icNumber == '')
                            // {
                            //     // echo "no license";
                            //     header('Location: ../editProfile.php');
                            // }

                            else
                            {
                                // echo "ic, signature, license uploaded !!";
                                // header('Location: ../userDashboard.php');

                                    if($user->getMp() == "YES")
                                    {
                                        // header('Location: ../userDashboard.php');

                                        // if($icNumber != '')
                                        // {
        
                                        // }
                                        // else
                                        // {
                                        //     header('Location: ../editProfile.php');
                                        // }

                                        if($bankAccName == '')
                                        {
                                            header('Location: ../userBankDetails.php');
                                        }
                                        elseif($bankAccNumber == '')
                                        {
                                            header('Location: ../userBankDetails.php');
                                        }
                                        elseif($bankName == '')
                                        {
                                            header('Location: ../userBankDetails.php');
                                        }
                                        else
                                        {
                                            header('Location: ../userDashboard.php');
                                        }

                                    }
                                    else
                                    {
                                        header('Location: ../userMp4Id.php');
                                    }

                            }

                        }

                    // }
                    // else
                    // {
                    //     header('Location: ../index.php?userDeactivated');
                    // }

                }
                else
                {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../index.php?type=11');
                    // promptError("Incorrect email or password");
                    // echo "incorrect email or password";
                    echo "<script>alert('incorrect username or password');window.location='../index.php'</script>";
                }

        }
        else
        {
        //   $_SESSION['messageType'] = 1;
        //   header('Location: ../index.php?type=7');
        //   echo "no user with this email ";
          echo "<script>alert('no user with this username');window.location='../index.php'</script>";
          //   promptError("This account does not exist");
        }
    }

    $conn->close();
}
?>
