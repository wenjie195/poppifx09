<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $firstname = rewrite($_POST['update_firstname']);
    $lastname = rewrite($_POST["update_lastname"]);

    // $fullname = rewrite($_POST["update_fullname"]);

    $icNumber = rewrite($_POST["update_icno"]);

    $mobileNo = rewrite($_POST["update_mobileno"]);
    $email = rewrite($_POST["update_email"]);
    $dateOfBirth = rewrite($_POST["update_dob"]);
    $address = rewrite($_POST["update_address"]);
    $addressTwo = rewrite($_POST["update_address_two"]);
    $zipcode = rewrite($_POST["update_zipcode"]);
    $country = rewrite($_POST["update_country"]);

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

    $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icNumber),"s");
    $icDetails = $icRows[0];

    $getUserDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $bankAccName = $getUserDetails[0]->getBankAccName();
    $bankAccNumber = $getUserDetails[0]->getBankAccNumber();

    if (!$icDetails)
    {

        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($firstname)
            {
                array_push($tableName,"firstname");
                array_push($tableValue,$firstname);
                $stringType .=  "s";
            }
            if($lastname)
            {
                array_push($tableName,"lastname");
                array_push($tableValue,$lastname);
                $stringType .=  "s";
            }
            // if($fullname)
            // {
            //     array_push($tableName,"fullname");
            //     array_push($tableValue,$fullname);
            //     $stringType .=  "s";
            // }
            if($icNumber)
            {
                array_push($tableName,"icno");
                array_push($tableValue,$icNumber);
                $stringType .=  "s";
            }
            if($mobileNo)
            {
                array_push($tableName,"phone_no");
                array_push($tableValue,$mobileNo);
                $stringType .=  "s";
            }
            if($email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$email);
                $stringType .=  "s";
            }
            if($dateOfBirth)
            {
                array_push($tableName,"birth_date");
                array_push($tableValue,$dateOfBirth);
                $stringType .=  "s";
            }
            if($address)
            {
                array_push($tableName,"address");
                array_push($tableValue,$address);
                $stringType .=  "s";
            }
            if($addressTwo)
            {
                array_push($tableName,"address_two");
                array_push($tableValue,$addressTwo);
                $stringType .=  "s";
            }
            if($zipcode)
            {
                array_push($tableName,"zipcode");
                array_push($tableValue,$zipcode );
                $stringType .=  "s";
            }
            if($country)
            {
                array_push($tableName,"country");
                array_push($tableValue,$country );
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                // header('Location: ../userBankDetails.php');

                if($bankAccName == '')
                {
                header('Location: ../userBankDetails.php');
                }
                elseif($bankAccNumber == '')
                {
                header('Location: ../userBankDetails.php');
                }
                else
                {
                    header('Location: ../userDashboard.php');
                }

                // if($user->getBankAccNumber() != '')
                // {
                //     header('Location: ../userBankDetails.php');
                //     header('Location: ../profile.php');
                // }
                // else
                // {
                //      header('Location: ../userBankDetails.php');
                // }

            }
            else
            {
                // echo "fail";
                echo "<script>alert('fail to update profile !');window.location='../editProfile.php'</script>";
            }
        }
        else
        {
            //echo "";
            // header('Location: ../editProfile.php?type=3');
            echo "<script>alert('ERROR !!');window.location='../editProfile.php'</script>";
        }

    }
    else
    {
        //echo "";
        // header('Location: ../editProfile.php?type=3');
        echo "<script>alert('Your IC is repeated !! Please enter a new !!');window.location='../editProfile.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
