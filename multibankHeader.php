<!--
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>

            <div class="right-menu-div float-right admin-right-menu">
            	<a href="multibankEditPassword.php" class="menu-item menu-a"><?php echo _ADMINHEADER_EDIT_PASSWORD ?></a>

                <div class="dropdown  menu-item menu-a menu-left-margin">
                    <img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言">
                    <img src="img/dropdown.png" class="dropdown-img hover" alt="Language/语言" title="Language/语言">
                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>

               
                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                 
                                  <li><a href="multibankEditPassword.php" class="mini-li"><?php echo _ADMINHEADER_EDIT_PASSWORD ?></a></li>
                                  <li class="title-li unclickable-li"><img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言"> <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div>


            </div>
        </div>

</header>
-->
<div id="wrapper" class="toggled-2">

<div id="sidebar-wrapper">
<ul class="sidebar-nav nav-pills nav-stacked" id="menu">
	<li>
    	 <a href="multibankMember.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
    </li>
 
    <li class="sidebar-li">
        
        	<img src="img/password2.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><a href="multibankEditPassword.php" class="overflow"><?php echo _USERHEADER_EDIT_PASSWORD ?></a></p>
        
    </li>  
 
    <li class="sidebar-li">
        
        	<img src="img/english.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
        
    </li>    
    <li class="sidebar-li">
        
        	<img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
        
    </li>      
    <li class="sidebar-li">
        
        	<img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
            
            <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
        
    </li>       
        
</ul>
</div>



</div>
