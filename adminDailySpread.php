<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Requirements.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$monthlyProBon = getMonProBon($conn);

$requirementDetails = getRequirements($conn);

$cntAA = 1;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://victory5.co/adminDailySpread.php" />
    <link rel="canonical" href="https://victory5.co/adminDailySpread.php" />
    <meta property="og:title" content="Daily Spread  | Victory 5" />
    <title>Daily Spread  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  li{
    list-style-type: lower-roman;
  }
</style>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/daily.png" class="middle-title-icon" alt="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>" title="<?php echo _DAILY_MEMBER_DAILY_BONUS ?>">
    </div>  
    <div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _ADMINHEADER_DAILY_SPREAD ?></h1>
	</div>
    <div class="width100 margin-top50">
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _ADMINDAILY_RANKING ?></th>
                        <th><?php echo _ADMINDAILY_TEAM_PERFORMANCE ?></th>
                        <th><?php echo _VIEWLEVEL_SELF_INVEST ?></th>
                        <th><?php echo _ADMINDAILY_SPREAD_SHARING ?></th>
                    </tr>
                </thead>
                <tbody>
                            <tr>
                                <td><?php echo _ADMINDAILY_MEMBERS ?></td>
                                <td>-</td>
                                <td>-</td>
                                <td>$ 0</td>
                            </tr>
                            <tr>
                                <td><?php echo _ADMINDAILY_LEADER ?></td>
                                <td><li><?php echo _HIERARCHY_GROUP_SALES ?>: $ 5,000</li>
                                    <li><?php echo _ADMINDAILY_DIRECT ?></li>
                                 </td>
                                <td>$ 500</td>
                                <td>$ 2</td>
                            </tr>
                            <tr>
                                <td><?php echo _ADMINDAILY_TEAM_LEADER ?></td>
                                <td><li><?php echo _HIERARCHY_GROUP_SALES ?>: $ 30,000</li>
                                    <li><?php echo _ADMINDAILY_INDIRECT_DIRECT ?></li>
                                 </td>
                                <td>$ 3,000</td>
                                <td>$ 3</td>
                            </tr>
                            <tr>
                                <td><?php echo _ADMINDAILY_GROUP_LEADER ?>Group Leader</td>
                                <td><li><?php echo _HIERARCHY_GROUP_SALES ?> : $ 100,000</li>
                                    <li><?php echo _ADMINDAILY_INDIRECT_DIRECT_TEAM ?></li>
                                 </td>
                                <td>$ 5,000</td>
                                <td>$ 4</td>
                            </tr>
                            <tr style="height: 50px">
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td colspan="4" ><?php echo _ADMINDAILY_GROUP_LEADER_OVERRIDING ?> - 20%</td>
                            </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
