<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/EquityPlRawData.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$skipUpline = 0;
$memberBonus = 0;
$leaderBonus = 0.01;
$teamLeaderBonus = 0.02;
$groupLeaderBonus = 0.03;
$overridingGroupLeader = 0.2; // 20%

function getBonus($conn,$uid,$username,$fromWho,$bonus)
{
     if(insertDynamicData($conn,"daily_bonus",array("uid", "username","from_who","bonus"),
          array($uid,$username,$fromWho,$bonus),"ssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

// $getAllUser = getUser($conn, "WHERE username != 'admin' and username != 'company'");
$getAllUser = getUser($conn, "WHERE username != 'admin' and username != 'infinox'");

for ($k=0; $k < count($getAllUser) ; $k++) {
  $userBalance = 0;
   $uid = $getAllUser[$k]->getUid();
   $fromWho = $getAllUser[$k]->getUsername();
   echo "<br><br>"."Process Start at : ".$fromWho."<br>";

   $userMpIdDataDetails = getEquityPl($conn,"WHERE uid = ?",array("uid"),array($uid), "s");
   if ($userMpIdDataDetails) {
     for ($cntx=0; $cntx <count($userMpIdDataDetails) ; $cntx++) {
       $userBalance += $userMpIdDataDetails[$cntx]->getBalance();
     }
   }else {
     $userBalance = 0;
   }
   echo "Wallet : $ ".$userBalance."<br>";
   // $uid = '37fdbccbaa12ab9ce09f3c81f44be24d';
   // $fromWho = 'super1';
// $uid = $_SESSION['uid'];

$uplineDetails = getTop10ReferrerOfUser($conn,$uid); // get upline
$uplineArray = []; // initial array of uid that are not get bonus yet
$uplineUsernameArray = [];
$fromWhoNewAray = [];
$loopMembers = 0; // initial state of looping based on its own Ranking
$loop = 0; // initial state of looping based on its own Ranking
$loopLeader = 0; // initial state of looping based on its own Ranking
$loopTeamLeader = 0; // initial state of looping based on its own Ranking
$loopGroupLeader = 0; // initial state of looping based on its own Ranking
$bonus = 0;
$indirectDownlineRank = '';
// $balance = 0;
$currentLevel = 0;
$indirectCurrentLevel = 0;
$downlineCurrentLevel = 0;
$endLeaderLoop = 0;
$endTeamLeaderLoop = 0;

// if ($skipUpline >= 5) {
for ($count=5; $count <count($uplineDetails) ; $count++) {
  // foreach ($uplineDetails as $uplineDetail) {
  if (isset($uplineDetails[$count])) {
    $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
    $userUplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineDetails[$count]),"s");

    echo "Upline : ".$userUplineDetails[0]->getUsername()." Rank : ".$userUplineDetails[0]->getRank()."<br>";

    // if ($userDetails[0]->getRank() == 'Members') { // if you rank Members
    if ($userDetails[0]->getRank() == 'Member' && $userBalance) { // if you rank Members
      if ($userUplineDetails[0]->getRank() == 'Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $loopLeader == 0 && $endLeaderLoop == 0) { // if no member c yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }


        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Senior Manager' || $indirectDownlineRank != 'Director' || $indirectDownlineRank != 'Co-Founder' || $indirectDownlineRank != 'Co-Founder') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;
          $bonus = $memberLot * $leaderBonus; // $3 times with lots
          // $bonus = number_format($bonus,4);
          $loopLeader = 1; // means u already loop on Rank C
          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 1<br>";
          }
        }else {
          $endLeaderLoop = 1;
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Senior Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0 || $userUplineDetails[0]->getRank() == 'Co-Founder' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0) { // if no Rank B yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();
          // echo $downlineReferralId."<br>";

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Director') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopLeader == 1) {
          $bonus = $memberLot * ($teamLeaderBonus - $leaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }else {
          $bonus = $memberLot * $teamLeaderBonus; // $5 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopTeamLeader = 1; // means u already loop on Rank B

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 2<br>";
          }
        }else {
          $endTeamLeaderLoop = 1;
        }

      }

      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader == 0) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopTeamLeader == 1 && $loopLeader == 0) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $leaderBonus); // $4 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 1 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 0) {
          $bonus = $memberLot * $groupLeaderBonus; // $7 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopGroupLeader = 1; // means u already loop on Rank A

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3<br>";
          }
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader >= 1) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $oldBonusDetails = getDailyBonus($conn, "WHERE from_who = ? ORDER BY date_created DESC",array("from_who"),array($fromWho),"s");
          // $oldBonus = $oldBonusDetails[0]->getBonus();
          // $bonus = number_format($bonus,4);
          $oldBonus = $bonus;
          // echo $oldBonus;
          $bonus = $oldBonus * $overridingGroupLeader;
          // $bonus = number_format($bonus,4);

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3.1<br>";
          }
          $loopGroupLeader = 1; // means u already loop on Rank A
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Member') { // if no Rank A yet
        // $bonus = 0; // ignore becuz already loop on Rank A
        // $loopGroupLeader = 1; // means u already loop on Rank A

      }
      else{
      $loop++; // to count how many loop that still not get bonus
      $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet, we skip and will loop on below
      $uplineUsernameArray[] = $userUplineDetails[0]->getUsername();
      $fromWhoNewAray[] = $fromWho;
    }
    }
    elseif ($userDetails[0]->getRank() == 'Manager'&& $userBalance) { // if you rank Members
      $loopLeader = 1;
      if ($userUplineDetails[0]->getRank() == 'Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $loopLeader == 0 && $endLeaderLoop == 0) { // if no member c yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Senior Manager' || $indirectDownlineRank != 'Director' || $indirectDownlineRank != 'Co-Founder') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;
          $bonus = $memberLot * $leaderBonus; // $3 times with lots
          // $bonus = number_format($bonus,4);
          $loopLeader = 1; // means u already loop on Rank C
          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 1<br>";
          }
        }else {
          $endLeaderLoop = 1;
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Senior Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0 || $userUplineDetails[0]->getRank() == 'Co-Founder' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0) { // if no Rank B yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();
          // echo $downlineReferralId."<br>";

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Director') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopLeader == 1) {
          $bonus = $memberLot * ($teamLeaderBonus - $leaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }else {
          $bonus = $memberLot * $teamLeaderBonus; // $5 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopTeamLeader = 1; // means u already loop on Rank B

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 2<br>";
          }
        }else {
          $endTeamLeaderLoop = 1;
        }

      }

      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader == 0) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopTeamLeader == 1 && $loopLeader == 0) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $leaderBonus); // $4 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 1 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 0) {
          $bonus = $memberLot * $groupLeaderBonus; // $7 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopGroupLeader = 1; // means u already loop on Rank A

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3<br>";
          }
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader >= 1) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $oldBonusDetails = getDailyBonus($conn, "WHERE from_who = ? ORDER BY date_created DESC",array("from_who"),array($fromWho),"s");
          // $oldBonus = $oldBonusDetails[0]->getBonus();
          // $bonus = number_format($bonus,4);
          $oldBonus = $bonus;
          // echo $oldBonus;
          $bonus = $oldBonus * $overridingGroupLeader;
          // $bonus = number_format($bonus,4);

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3.1<br>";
          }
          $loopGroupLeader = 1; // means u already loop on Rank A
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Member') { // if no Rank A yet
        // $bonus = 0; // ignore becuz already loop on Rank A
        // $loopGroupLeader = 1; // means u already loop on Rank A

      }
      else{
      $loop++; // to count how many loop that still not get bonus
      $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet, we skip and will loop on below
      $uplineUsernameArray[] = $userUplineDetails[0]->getUsername();
      $fromWhoNewAray[] = $fromWho;
    }
    }

    elseif ($userDetails[0]->getRank() == 'Senior Manager' && $userBalance || $userDetails[0]->getRank() == 'Co-Founder' && $userBalance) { // if you rank Members
      $loopTeamLeader = 1;
      if ($userUplineDetails[0]->getRank() == 'Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $loopLeader == 0 && $endLeaderLoop == 0) { // if no member c yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Senior Manager' || $indirectDownlineRank != 'Director' || $indirectDownlineRank != 'Co-Founder') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;
          $bonus = $memberLot * $leaderBonus; // $3 times with lots
          // $bonus = number_format($bonus,4);
          $loopLeader = 1; // means u already loop on Rank C
          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 1<br>";
          }
        }else {
          $endLeaderLoop = 1;
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Senior Manager' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0 || $userUplineDetails[0]->getRank() == 'Co-Founder' && $loopGroupLeader == 0 && $loopTeamLeader == 0 && $endTeamLeaderLoop == 0) { // if no Rank B yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();
          // echo $downlineReferralId."<br>";

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank != 'Director') {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopLeader == 1) {
          $bonus = $memberLot * ($teamLeaderBonus - $leaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }else {
          $bonus = $memberLot * $teamLeaderBonus; // $5 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopTeamLeader = 1; // means u already loop on Rank B

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 2<br>";
          }
        }else {
          $endTeamLeaderLoop = 1;
        }

      }

      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader == 0) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $companyBalance = getCompanyBalance($conn);
          // $ctb = $companyBalance[0]->getCtb();
          // $tls = $companyBalance[0]->getTls();
          $memberLot = $userBalance;

          if ($loopTeamLeader == 1 && $loopLeader == 0) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $leaderBonus); // $4 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 1 && $loopLeader == 1) {
          $bonus = $memberLot * ($groupLeaderBonus - $teamLeaderBonus); // $2 times with lots
          // $bonus = number_format($bonus,4);
        }
        if($loopTeamLeader == 0 && $loopLeader == 0) {
          $bonus = $memberLot * $groupLeaderBonus; // $7 times with lots
          // $bonus = number_format($bonus,4);
        }
          $loopGroupLeader = 1; // means u already loop on Rank A

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3<br>";
          }
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Director' && $loopGroupLeader >= 1) { // if no Rank A yet
        $statusDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
        if ($statusDetails) {
          $uplineName = $statusDetails[0]->getUsername();
          $uplineUid = $statusDetails[0]->getUid();
          // $balance = $statusDetails[0]->getBalance();
        }else {
          $uplineName = null;
          $uplineUid = null;
          // $balance = 0;
        }

        $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uplineUid),"s");
        if ($userReferralHistory) {
        $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
        }
        $indirectCurrentLevel = $currentLevel + 1; // get Indirect Current Level


        $getWho = getWholeDownlineTree($conn, $uplineUid,false); // get downline function

        for ($i=0; $i <count($getWho) ; $i++) {
          $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
          // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
          $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();
          $downlineReferralId = $downlineReferralHistory[0]->getReferralId();

          if ($downlineCurrentLevel == $indirectCurrentLevel) {
            $downlineDetailss = getUser($conn,"WHERE uid = ?",array("uid"),array($downlineReferralId), "s");
            echo " ↳ Downline Name : ".$downlineDetailss[0]->getUsername()." Rank : ".$downlineDetailss[0]->getRank()."<br>";
            $indirectDownlineRank = $downlineDetailss[0]->getRank();

          }
        }
        if ($indirectDownlineRank) {
          // $oldBonusDetails = getDailyBonus($conn, "WHERE from_who = ? ORDER BY date_created DESC",array("from_who"),array($fromWho),"s");
          // $oldBonus = $oldBonusDetails[0]->getBonus();
          // $bonus = number_format($bonus,4);
          $oldBonus = $bonus;
          // echo $oldBonus;
          $bonus = $oldBonus * $overridingGroupLeader;
          // $bonus = number_format($bonus,4);

          if(getBonus($conn,$uplineUid,$uplineName,$fromWho,$bonus))
          {
            echo "success 3.1<br>";
          }
          $loopGroupLeader = 1; // means u already loop on Rank A
        }

      }
      elseif ($userUplineDetails[0]->getRank() == 'Member') { // if no Rank A yet
        // $bonus = 0; // ignore becuz already loop on Rank A
        // $loopGroupLeader = 1; // means u already loop on Rank A

      }
      else{
      $loop++; // to count how many loop that still not get bonus
      $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet, we skip and will loop on below
      $uplineUsernameArray[] = $userUplineDetails[0]->getUsername();
      $fromWhoNewAray[] = $fromWho;
    }
  }
    }
  }
// }else{
//   $skipUpline += 1;
//   echo $skipUpline."<br>";
//   echo " Name : ".$userUplineDetails[0]->getUsername()."<br>";
//   echo "Rank : ".$userUplineDetails[0]->getRank()."<br>";
// }
}

 ?>
