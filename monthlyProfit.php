<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/EquityPlRawData.php';
require_once dirname(__FILE__) . '/classes/Requirements.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$requirementDetails = getRequirements($conn, "ORDER BY profit_sharing DESC");
$monthlyProfitBonus = getMonProBon($conn);

function getMonthlyBonus($conn,$uid,$username,$level,$who,$bonus)
{
     if(insertDynamicData($conn,"monthly_bonus",array("uid", "username","level","who","bonus"),
          array($uid,$username,$level,$who,$bonus),"sssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

$getAllUser = getUser($conn, "WHERE username != 'admin' and username !='infinox'");

for ($k=0; $k < count($getAllUser) ; $k++) {
   $uid = $getAllUser[$k]->getUid();
   $username = $getAllUser[$k]->getUsername();
   // $username = 'AB1';
   echo "<br><br>Name : ".$username."<br>";
   // $uid = '2717531593f862d7c90f043df6e116d0';
   $totalAmount = 0;
   $directSponsor1 = 0;
   $directSponsor2 = 0;
   $directSponsor3 = 0;
   $directSponsor4 = 0;
   $directSponsor5 = 0;
   $directSponsor6 = 0;
   $directSponsor7 = 0;
   $directSponsor8 = 0;
   $directSponsor9 = 0;
   $level = 0;
   $levelLoop = 0;
   $monthlyProfitPercent = 0;
   $equityPlBalance = 0;
   $balance = 0;
   $currentLevel = 0;
   $indirectCurrentLevel = 0;
   $amount = 0;

   // $equityPLDetails = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");
   // if ($equityPLDetails) {
   // $balance = $equityPLDetails[0]->getBalance();
   // }
   $equityPLDetails = getEquityPl($conn,"WHERE uid =?",array("uid"),array($uid),"s");
   if ($equityPLDetails) {
   $balance = $equityPLDetails[0]->getBalance();
 }

   $userReferralHistory = getReferralHistory($conn,"WHERE referral_id = ?",array("referral_id"),array($uid),"s");
   if ($userReferralHistory) {
   $currentLevel = $userReferralHistory[0]->getCurrentLevel(); // get your current level
   }
   $indirectCurrentLevel1 = $currentLevel + 1; // get Indirect Current Level
   $indirectCurrentLevel2 = $currentLevel + 2; // get Indirect Current Level
   $indirectCurrentLevel3 = $currentLevel + 3; // get Indirect Current Level
   $indirectCurrentLevel4 = $currentLevel + 4; // get Indirect Current Level
   $indirectCurrentLevel5 = $currentLevel + 5; // get Indirect Current Level
   // $indirectCurrentLevel6 = $currentLevel + 6; // get Indirect Current Level
   // $indirectCurrentLevel7 = $currentLevel + 7; // get Indirect Current Level
   // $indirectCurrentLevel8 = $currentLevel + 8; // get Indirect Current Level
   // $indirectCurrentLevel9 = $currentLevel + 9; // get Indirect Current Level
   $getWho = getWholeDownlineTree($conn, $uid,false); // get downline function

   for ($i=0; $i <count($getWho) ; $i++) {
     $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($getWho[$i]->getReferralId()),"s"); // get referral history
     // echo $downlineReferralHistory[0]->getCurrentLevel()."<br>"; // get downline current level
     $downlineCurrentLevel = $downlineReferralHistory[0]->getCurrentLevel();

     if ($downlineCurrentLevel == $indirectCurrentLevel1) {
       $level = 1;
       $referralId = $downlineReferralHistory[0]->getReferralId();
       $referralName = $downlineReferralHistory[0]->getReferralName();
       $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
       if ($equityPLDetailsII) {
         $balance = $equityPLDetailsII[0]->getBalance();
         $who = $equityPLDetailsII[0]->getName();
       }else {
         $balance = 0;
       }
       $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
       if ($monthlyProfitBonus) {
         $amount = $monthlyProfitBonus[0]->getAmount() / 100;
         $totalAmount =  $balance * $amount;
         echo "Bonus Level 1 : ".$totalAmount."<br>";
         if ($totalAmount) {
            if(getMonthlyBonus($conn,$uid,$username,$level,$who,$totalAmount))
            {
              echo "success 1<br>";
            }
         }
       }
       $balance = 0; // reset amount back
       // echo $referralName."<br>";
       // echo $totalBalance1."<br>";
       $directSponsor1++;
     }
     if ($downlineCurrentLevel == $indirectCurrentLevel2) {
       $level = 2;
       $referralId = $downlineReferralHistory[0]->getReferralId();
       $referralName = $downlineReferralHistory[0]->getReferralName();
       $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
       if ($equityPLDetailsII) {
         $balance = $equityPLDetailsII[0]->getBalance();
         $who2 = $equityPLDetailsII[0]->getName();
       }
       $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
       if ($monthlyProfitBonus) {
         $amount = $monthlyProfitBonus[0]->getAmount() / 100;
         $totalAmount =  $balance * $amount;
         echo "Bonus Level 2 : ".$totalAmount."<br>";
         if ($totalAmount) {
            if(getMonthlyBonus($conn,$uid,$username,$level,$who2,$totalAmount))
            {
              echo "success 2<br>";
            }
         }
       }
       $balance = 0; // reset amount back
       // echo $referralName."<br>";
       // echo $totalBalance2."<br>";
       $directSponsor2++;
     }
     if ($downlineCurrentLevel == $indirectCurrentLevel3) {
       $level = 3;
       $referralId = $downlineReferralHistory[0]->getReferralId();
       $referralName = $downlineReferralHistory[0]->getReferralName();
       $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
       if ($equityPLDetailsII) {
         $balance = $equityPLDetailsII[0]->getBalance();
         $who3 = $equityPLDetailsII[0]->getName();
       }
       $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
       if ($monthlyProfitBonus) {
         $amount = $monthlyProfitBonus[0]->getAmount() / 100;
         $totalAmount =  $balance * $amount;
         echo "Bonus Level 3 : ".$totalAmount."<br>";
         if ($totalAmount) {
            if(getMonthlyBonus($conn,$uid,$username,$level,$who3,$totalAmount))
            {
              echo "success 3<br>";
            }
         }
       }
       $balance = 0; // reset amount back
       // echo $referralName."<br>";
       // echo $totalBalance3."<br>";
       $directSponsor3++;
     }
     if ($downlineCurrentLevel == $indirectCurrentLevel4) {
       $level = 4;
       $referralId = $downlineReferralHistory[0]->getReferralId();
       $referralName = $downlineReferralHistory[0]->getReferralName();
       $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
       if ($equityPLDetailsII) {
         $balance = $equityPLDetailsII[0]->getBalance();
         $who4 = $equityPLDetailsII[0]->getName();
       }
       $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
       if ($monthlyProfitBonus) {
         $amount = $monthlyProfitBonus[0]->getAmount() / 100;
         $totalAmount =  $balance * $amount;
         echo "Bonus Level 4 : ".$totalAmount."<br>";
         if ($totalAmount) {
            if(getMonthlyBonus($conn,$uid,$username,$level,$who4,$totalAmount))
            {
              echo "success 4<br>";
            }
         }
       }
       $balance = 0; // reset amount back
       // echo $referralName."<br>";
       // echo $totalBalance4."<br>";
       $directSponsor4++;
     }
     if ($downlineCurrentLevel == $indirectCurrentLevel5) {
       $level = 5;
       $referralId = $downlineReferralHistory[0]->getReferralId();
       $referralName = $downlineReferralHistory[0]->getReferralName();
       $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
       if ($equityPLDetailsII) {
         $balance = $equityPLDetailsII[0]->getBalance();
         $who5 = $equityPLDetailsII[0]->getName();
       }
       $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
       if ($monthlyProfitBonus) {
         $amount = $monthlyProfitBonus[0]->getAmount() / 100;
         $totalAmount =  $balance * $amount;
         echo "Bonus Level 5 : ".$totalAmount."<br>";
         if ($totalAmount) {
            if(getMonthlyBonus($conn,$uid,$username,$level,$who5,$totalAmount))
            {
              echo "success 5<br>";
            }
         }
       }
       $balance = 0; // reset amount back
       // echo $referralName."<br>";
       // echo $totalBalance5."<br>";
       $directSponsor5++;
     }
     // if ($downlineCurrentLevel == $indirectCurrentLevel6) {
     //   $level = 6;
     //   $referralId = $downlineReferralHistory[0]->getReferralId();
     //   $referralName = $downlineReferralHistory[0]->getReferralName();
     //   $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
     //   if ($equityPLDetailsII) {
     //     $balance = $equityPLDetailsII[0]->getBalance();
     //     $who6 = $equityPLDetailsII[0]->getName();
     //   }
     //   $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
     //   if ($monthlyProfitBonus) {
     //     $amount = $monthlyProfitBonus[0]->getAmount() / 100;
     //     $totalAmount =  $balance * $amount;
     //     echo "Bonus Level 6 : ".$totalAmount."<br>";
     //     if ($totalAmount) {
     //        if(getMonthlyBonus($conn,$uid,$username,$level,$who6,$totalAmount))
     //        {
     //          echo "success 6<br>";
     //        }
     //     }
     //   }
     //   $balance = 0; // reset amount back
     //   // echo $referralName."<br>";
     //   // echo $totalBalance6."<br>";
     //   $directSponsor6++;
     // }
     // if ($downlineCurrentLevel == $indirectCurrentLevel7) {
     //   $level = 7;
     //   $referralId = $downlineReferralHistory[0]->getReferralId();
     //   $referralName = $downlineReferralHistory[0]->getReferralName();
     //   $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
     //   if ($equityPLDetailsII) {
     //     $balance = $equityPLDetailsII[0]->getBalance();
     //     $who7 = $equityPLDetailsII[0]->getName();
     //   }
     //   $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
     //   if ($monthlyProfitBonus) {
     //     $amount = $monthlyProfitBonus[0]->getAmount() / 100;
     //     $totalAmount =  $balance * $amount;
     //     echo "Bonus Level 7 : ".$totalAmount."<br>";
     //     if ($totalAmount) {
     //        if(getMonthlyBonus($conn,$uid,$username,$level,$who7,$totalAmount))
     //        {
     //          echo "success 7<br>";
     //        }
     //     }
     //   }
     //   $balance = 0; // reset amount back
     //   // echo $referralName."<br>";
     //   // echo $totalBalance7."<br>";
     //   $directSponsor7++;
     // }
     // if ($downlineCurrentLevel == $indirectCurrentLevel8) {
     //   $level = 8;
     //   $referralId = $downlineReferralHistory[0]->getReferralId();
     //   $referralName = $downlineReferralHistory[0]->getReferralName();
     //   $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
     //   if ($equityPLDetailsII) {
     //     $balance = $equityPLDetailsII[0]->getBalance();
     //     $who8 = $equityPLDetailsII[0]->getName();
     //   }
     //   $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
     //   if ($monthlyProfitBonus) {
     //     $amount = $monthlyProfitBonus[0]->getAmount() / 100;
     //     $totalAmount =  $balance * $amount;
     //     echo "Bonus Level 8 : ".$totalAmount."<br>";
     //     if ($totalAmount) {
     //        if(getMonthlyBonus($conn,$uid,$username,$level,$who8,$totalAmount))
     //        {
     //          echo "success 8<br>";
     //        }
     //     }
     //   }
     //   $balance = 0; // reset amount back
     //   // echo $referralName."<br>";
     //   // echo $totalBalance8."<br>";
     //   $directSponsor8++;
     // }
     // if ($downlineCurrentLevel == $indirectCurrentLevel9) {
     //   $level = 9;
     //   $referralId = $downlineReferralHistory[0]->getReferralId();
     //   $referralName = $downlineReferralHistory[0]->getReferralName();
     //   $equityPLDetailsII = getEquityPl($conn,"WHERE uid =?",array("uid"),array($referralId),"s");
     //   if ($equityPLDetailsII) {
     //     $balance = $equityPLDetailsII[0]->getBalance();
     //     $who9 = $equityPLDetailsII[0]->getName();
     //   }
     //   $monthlyProfitBonus = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
     //   if ($monthlyProfitBonus) {
     //     $amount = $monthlyProfitBonus[0]->getAmount() / 100;
     //     $totalAmount =  $balance * $amount;
     //     echo "Bonus Level 9 : ".$totalAmount."<br>";
     //     if ($totalAmount) {
     //        if(getMonthlyBonus($conn,$uid,$username,$level,$who9,$totalAmount))
     //        {
     //          echo "success 9<br>";
     //        }
     //     }
     //   }
     //   $balance = 0; // reset amount back
     //   // echo $referralName."<br>";
     //   // echo $totalBalance9."<br>";
     //   $directSponsor9++;
     // }
   }

   // if ($monthlyProfitBonus) {
   //   for ($b=0; $b < count($monthlyProfitBonus) ; $b++) {
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount =  $totalBalance1 * $amount;
   //      echo "Bonus Level 1 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 1<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance2 * $amount;
   //      echo "Bonus Level 2 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 2<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance3 * $amount;
   //      echo "Bonus Level 3 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 3<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance4 * $amount;
   //      echo "Bonus Level 4 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 4<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance5 * $amount;
   //      echo "Bonus Level 5 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 5<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance6 * $amount;
   //      echo "Bonus Level 6 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 6<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance7 * $amount;
   //      echo "Bonus Level 7 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 7<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance8 * $amount;
   //      echo "Bonus Level 8 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 8<br>";
   //         }
   //      }
   //     }
   //     if ($monthlyProfitBonus[$b]->getLevel() == $level) {
   //      $amount = $monthlyProfitBonus[$b]->getAmount() / 100;
   //      $totalAmount = $totalBalance9 * $amount;
   //      echo "Bonus Level 9 : ".$totalAmount."<br>";
   //      if ($totalAmount) {
   //         if(getMonthlyBonus($conn,$uid,$username,$level,$totalAmount))
   //         {
   //           echo "success 9<br>";
   //         }
   //      }
   //     }
   //   }
   // }
   echo "Direct Sponsor 1 : ".$directSponsor1."<br>";
   echo "Direct Sponsor 2 : ".$directSponsor2."<br>";
   echo "Direct Sponsor 3 : ".$directSponsor3."<br>";
   echo "Direct Sponsor 4 : ".$directSponsor4."<br>";
   echo "Direct Sponsor 5 : ".$directSponsor5."<br>";
   echo "Direct Sponsor 6 : ".$directSponsor6."<br>";
   echo "Direct Sponsor 7 : ".$directSponsor7."<br>";
   echo "Direct Sponsor 8 : ".$directSponsor8."<br>";
   echo "Direct Sponsor 9 : ".$directSponsor9."<br>";
   // if ($requirementDetails) {
   //   for ($l=0; $l <count($requirementDetails) ; $l++) {
   //     if ($directSponsor >= $requirementDetails[$l]->getDirectSponsor() && $balance >= $requirementDetails[$l]->getSelfInvest() && $levelLoop == 0) {
   //       $level = $requirementDetails[$l]->getProfitSharing();
   //       $levelLoop = 1;
   //     }
   //   }
   // }

   // if ($directSponsor >= 4 && $balance >= 3000 && $levelLoop == 0) {
   //   $level = 7;
   //   $levelLoop = 1;
   // }
   // if ($directSponsor >= 3 && $balance >= 500 && $levelLoop == 0) {
   //   $level = 4;
   //   $levelLoop = 1;
   // }
   // if ($directSponsor >= 2 && $balance >= 500 && $levelLoop == 0) {
   //   $level = 2;
   //   $levelLoop = 1;
   // }
   // if ($directSponsor >= 1 && $balance >= 500 && $levelLoop == 0) {
   //   $level = 1;
   //   $levelLoop = 1;
   // }

   // $monthlyProfitDetails = getMonProBon($conn,"WHERE level = ?", array("level"), array($level), "s");
   // $equityPlDetails = getEquityPl($conn,"WHERE uid =?",array("uid"),array($uid),"s");
   //
   // if ($monthlyProfitDetails) {
   //   // echo $monthlyProfitPercent = $monthlyProfitDetails[0]->getAmount();
   //   // echo $level;
   //   $monthlyProfitPercent = $monthlyProfitDetails[0]->getAmount() / 100;
   // }
   // if ($equityPlDetails) {
   //   $equityPlBalance = $equityPlDetails[0]->getBalance();
   // }

   // echo "RM".$equityPlBalance * $monthlyProfitPercent." -----> ".$username." -----> ".$monthlyProfitPercent."%"."<br>";
   // $monthlyProfitBonus = $equityPlBalance * $monthlyProfitPercent;
   //
   // if(getMonthlyBonus($conn,$uid,$username,$level,$monthlyProfitBonus))
   // {
   //   echo "success 1<br>";
   // }





 }
 ?>
