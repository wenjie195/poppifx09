<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right admin-right-menu">
            	<a href="adminDashboard.php" class="menu-item menu-a"><?php echo _ADMINHEADER_DASHBOARD ?></a>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    <?php echo _ADMINHEADER_INFO ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_INFO ?>" title="<?php echo _ADMINHEADER_INFO ?>">

                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="adminViewBalance.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MEMBER_BALANCE ?></a></p>
                        <p class="dropdown-p"><a href="adminCompanyBalance.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_COMPANY_BALANCE ?></a></p>

                        <p class="dropdown-p"><a href="adminAddUser.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_ADD_USER ?></a></p>
                        <p class="dropdown-p"><a href="adminViewMember.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>

                	</div>
                </div>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="Language/语言" title="Language/语言">

                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>

                <!-- -->
                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li class="title-li"><a href="adminDashboard.php" class="no-padding-left"><?php echo _ADMINHEADER_DASHBOARD ?></a></li>

                                  <!-- <li class="title-li">My Team</li>            -->
                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_INFO ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_INFO ?>" title="<?php echo _ADMINHEADER_INFO ?>"></li>
                                  <li><a href="adminViewBalance.php" class="mini-li"><?php echo _ADMINHEADER_MEMBER_BALANCE ?></a></li>
								<li><a href="adminCompanyBalance.php" class="mini-li"><?php echo _ADMINHEADER_COMPANY_BALANCE ?></a></li>
                                  <li><a href="adminAddUser.php" class="mini-li"><?php echo _ADMINHEADER_ADD_USER ?></a></li>
                                  <li><a href="adminViewMember.php" class="mini-li"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></li>

                                  <!-- <li class="title-li">Language</li> -->
                                  <li class="title-li unclickable-li">Language/语言 <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->



            </div>
        </div>

</header>
