<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/uploadBackIC.php" />
    <meta property="og:title" content="Upload Details  | Victory 5" />
    <title>Upload Details  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/uploadBackIC.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

  <div class="width100 same-padding menu-distance darkbg min-height">
    <!-- Upload  -->
    <h1 class="upload-h1 white-text text-center"><?php echo _UPLOAD_IC_BACK ?></h1>
    <form action="utilities/uploadIcBackFunction.php" method="POST" enctype="multipart/form-data" id="file-upload-form" class="uploader">
      <!-- <input id="file-upload" type="file" name="fileUpload" accept="image/*" /> -->
      <input id="file-upload" type="file" name="file" accept="image/*" />
        <label for="file-upload" id="file-drag">
          <img id="file-image" src="#" alt="<?php echo _UPLOAD_PREVIEW ?>" title="<?php echo _UPLOAD_PREVIEW ?>" class="hidden">
          <div id="start">
            <img src="img/upload.png" alt="<?php echo _UPLOAD ?>" title="<?php echo _UPLOAD ?>" class="upload-icon">
            <div><?php echo _UPLOAD_SELECT_DRAG ?></div>
            <div id="notimage" class="hidden"><?php echo _UPLOAD_PLS_SELECT_IMG ?></div>
            <span id="file-upload-btn" class="btn btn-primary"><?php echo _UPLOAD_SELECT_A_FILE ?></span>
          </div>
          <div id="response" class="hidden">
            <div id="messages"></div>
            <progress class="progress" id="file-progress" value="0">
            <span>0</span>%
            </progress>
          </div>
        </label>
      <div class="width100 text-center">
        <button class="clean blue-button one-button-width"  type="submit" name="submit"><?php echo _UPLOAD ?></button>
      </div>  
    </form>
    <div class="clear"></div>
  </div>

<?php include 'js.php'; ?>
</body>
</html>