<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/editProfile.php" />
    <meta property="og:title" content="Edit Profile  | Victory 5" />
    <title>Edit Profile  | Victory 5</title>
    <meta property="og:url" content="https://poppifx4u.com/editProfile.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php
$userIC = $userData->getIcno();
if($userIC == '')
{
?>
    <?php include 'header.php'; ?>
<?php
}
else
{
?>
    <?php include 'userHeader.php'; ?>
<?php
}
?>

<div class="width100 same-padding menu-distance darkbg min-height">

    <form action="utilities/editProfileFunction.php" method="POST">
    <h1 class="h1-title white-text text-center"><?php echo _JS_EDIT_PROFILE ?></h1>
    
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <p class="bottom-input-text"><?php echo $userData->getUsername();?></p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _INDEX_IC_NO ?></p>
			<!-- <p class="bottom-input-text"><?php //echo $userData->getIcno();?></p> -->

            <?php
            $userIC = $userData->getIcno();
            if($userIC == '')
            {
            ?>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_IC_NO ?>" id="update_icno" name="update_icno" required>
            <?php
            }
            else
            {
            ?>
            		<p class="bottom-input-text"><?php echo $userData->getIcno();?></p>
            <?php
            }
            ?>

        </div>
        
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <p class="bottom-input-text"><?php echo $userData->getFirstname();?></p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
            <p class="bottom-input-text"><?php echo $userData->getLastname();?></p>
        </div>

        <!-- <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" value="<?php echo $userData->getFirstname();?>" id="update_firstname" name="update_firstname">
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" value="<?php echo $userData->getLastname();?>" id="update_lastname" name="update_lastname">
        </div> -->

        <div class="clear"></div>

        <div class="dual-input">   
            <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_MOBILE_NO ?>" value="<?php echo $userData->getPhoneNo();?>" id="update_mobileno" name="update_mobileno" required>
        </div>
        
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_DOB ?> : <?php echo $userData->getBirthDate();?></p>
            <input class="clean pop-input" type="date" placeholder="<?php echo _INDEX_DOB ?>" id="update_dob" name="update_dob">      
        </div>

        <div class="clear"></div>

		<div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?>" value="<?php echo $userData->getAddress();?>" id="update_address" name="update_address" required>
        </div>

        <div class="clear"></div>

        <div class="width100">
            <p class="input-top-text"><?php echo _INDEX_ADDRESS ?> 2</p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ADDRESS ?> 2" value="<?php echo $userData->getAddressB();?>" id="update_address_two" name="update_address_two">
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _INDEX_ZIPCODE ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_ZIPCODE ?>" value="<?php echo $userData->getZipcode();?>" id="update_zipcode" name="update_zipcode" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <select class="clean pop-input" id="update_country" name="update_country" required>

                <!-- <option>Please Select A Country</option> -->
                <option><?php echo $userData->getCountry();?></option>
                <?php
                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                {
                ?>
                <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                ?>"> 
                <?php 
                echo $countryList[$cntPro]->getEnName(); //take in display the options
                ?>
                </option>
                <?php
                }
                ?>

            </select>
        </div>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>