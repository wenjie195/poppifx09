<?php
require_once dirname(__FILE__) . '/multibankAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn);
// $userDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/multibankViewBills.php" />
    <meta property="og:title" content="Utility Bill  | Victory 5" />
    <title>Utility Bill  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/multibankViewBills.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<!-- <?php //include 'adminHeader.php'; ?> -->
<?php include 'multibankHeader.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text print-big-div">
<h1 class="doc-h1 opacity-hover black-text" onclick="goBack()"><img src="img/back3.png" class="back-png" alt="Go Back" title="Go Back"> Utility Bill</h1>
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");

    $userDoc = getStatus($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <div class="top-profile-div overflow">

            <h3 class="profile-h3 ow-black-text">Username: <b><?php echo $userDetails[0]->getUsername();?></b></h3>
            <h3 class="profile-h3 mid-profile-h3 ow-black-text">IC : <b><?php echo $userDetails[0]->getIcno();?></b></h3>
            <h3 class="profile-h3 ow-black-text">MT4 ID : <b><?php echo $userDetails[0]->getMpId();?></b></h3>

            <div class="width100 overflow">
                <img src="uploads/<?php echo $userDoc[0]->getLicense();?>" class="signature-css" alt="Signature" title="Signature">
            </div>

        </div>
		<div class="clear"></div>
        <div class="width100 overflow">
        	<div class="invite-btn blue-button text-center print-button" onclick="window.print()"><?php echo _MULTIBANK_PRINT ?></div>
        </div>        
    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>

</body>
</html>