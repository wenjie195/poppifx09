<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/" />
    <meta property="og:title" content="Login  | Victory 5" />
    <title>Login  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
    
    <div class="left-login-div">
    <h1 class="h1-title white-text"><?php echo _JS_LOGIN ?></h1>

    <!-- hide due to register is need referrer-->
	<!-- <p class="width100 signup-p">	
		<a href="register.php" class="white-link forgot-a"><?php //echo _INDEX_NO_YET ?></a>
	</p>       -->
    
        <form action="utilities/loginFunction.php" method="POST">
            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            
            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
            </div>
            <div class="clear"></div>
            <button class="clean width100 blue-button" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <p class="width100 text-center">	
                <a href="forgotPassword.php" class="blue-link forgot-a"><?php echo _JS_FORGOT_TITLE ?></a>
            </p>
          
            <div class="clear"></div>
        </form>
    </div>
    <div class="right-login-div">
    	<img src="img/laptop.png" alt="<?php echo _JS_LOGIN ?>" title="<?php echo _JS_LOGIN ?>"   data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow pulse black-rock absolute animated width100">
    </div>

</div>
<?php include 'js.php'; ?>
</body>
</html>