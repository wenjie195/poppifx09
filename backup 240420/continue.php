<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/continue.php" />
    <meta property="og:title" content="Continue  | Victory 5" />
    <title>Continue  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/continue.php" />
	<?php include 'css.php'; ?>
</head>

<body>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
  	<div class="document-div">
  		<img src="img/multibank-white.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">
        <h2 class="white-text document-h2">Power of Attorney & Discretionary Account Terms</h2>
        <p class="document-p white-text">
        	Name of Attorney: Poppifx<br><br>
            Address of Attorney:<br><br>
        	The below under signed here in after referred to as the Customer, hereby authorises the Attorney‐ in-fact as its agent Attorney-in-fact to buy, sell (including short sales), exchange, assign or transfer and trade for it at any price (my) (our) attorney deems fair in contracts as defined in the Customer Agreement of MultiBank FX International Corporation (“MEX”). The Customer hereby indemnifies MEX and its directors, officers, employees and agents from and against all liability arising directly or indirectly, from following Attorney‐ in‐ fact's instruction and will pay MEX promptly, on demand, any losses arising from such trades and any debit balance resulting there from.<br><br>
            In all such purchases, sales or trades, MEX is authorised to follow Attorney-in-fact's instructions in every respect and Attorney-in-fact is authorised to act for the Customer with the same force and effect as Customer might do with respect to such purchase, sales or trades and all things necessary or incidental to the furtherance of such purchases, sales or trades. MEX is directed to make available to Attorney-in-fact a copy of all statements that MEX makes available to the Customer concerning Customer's account, including, but not limited to, monthly statements, confirmations and purchase and sale agreements. The Customer hereby ratifies and confirms any and all transactions with MEX heretofore and hereafter made by the Attorney‐ in‐ fact for Customer's account.<br><br>
The Attorney‐ in‐ fact is not authorised to withdraw from Customer's account any monies, securities or any property either in the Customer's name or otherwise unless such withdrawal or payment is specifically authorised in writing by the Customer.<br><br>
This Power of Attorney shall remain in full force and effect until MEX receives from the Customer written notification of Customer's revocation thereof.
<br><br>
The Customer understands that MEX is in no way responsible for any loss to the Customer occasioned by actions of the Attorney-in-fact and that MEX does not, by implication or otherwise, endorse the operation or methods of the Attorney- in-fact.<br><br>
Fee Acknowledgement<br><br>
The Client agrees that MEX has been authorised to compensate The Attorney for the Asset Managers services.<br><br>
MEX accepts no responsibility for ratifying the performance or management fees instructed by the Attorney in relation to activity on this account, and this information is provided below to MEX purely for informational purposes but MEX accepts no responsibility as calculation agent to verify that the fees instructed by the Attorney are correct and is exempt from any liability from acting upon such an instruction.<br><br>
Management fee: 0% Per Month<br><br>
Incentive fee: 40% Per Month<br><br>
Note: It is the assumed to be the responsibility of the Asset Manager or Trading Agent to request any fee payment. MultiBank FX International Corporation is not responsible for any losses which may result from a failure of them to do so or from any incorrect fee amount notified to MEX by this party.<br><br>
*Please add the applicable fee type % amount above to the relevant section and nominatefrequency of payment by ticking the box.
        </p>
    </div>
	
    <div class="width100 text-center">
    	<a href="">
        	<div class="blue-button one-button-width margin-auto">Next</div>
    	</a>
    </div>

</div>
<?php include 'js.php'; ?>
</body>
</html>