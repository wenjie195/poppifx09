<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$id = rewrite($_POST['id']);
$companyBalanceDet = getCompanyBalance($conn, "WHERE id = ?",array("id"),array($id), "s");
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/editCompanyBalance.php" />
    <meta property="og:title" content="Edit Company Balance  | Victory 5" />
    <title>Edit Company Balance  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/editCompanyBalance.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">

    <form action="utilities/editCompanyBalanceFunction.php" method="POST">
    <h1 class="h1-title white-text">Edit Company Balance</h1>

	<p class="width100 signup-p">
	</p>
        <div class="dual-input">
            <p class="input-top-text">Company Total Balance (CTB)</p>
            <input class="clean pop-input" type="text" placeholder="CTB" value="<?php echo $companyBalanceDet[0]->getCtb() ?>" name="ctb" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text">Total Lot Size (TLS)</p>
            <input class="clean pop-input" type="text" placeholder="TLS" value="<?php echo $companyBalanceDet[0]->getTls() ?>" name="tls" required>
        </div>
        <div class="clear"></div>

		<div class="width100 text-center">
      <input type="hidden" name="id" value="<?php echo $companyBalanceDet[0]->getId() ?>">
        	<button class="clean blue-button one-button-width" name="editBalance">Submit</button>
        </div>
    </form>

</div>
<?php include 'js.php'; ?>
</body>
</html>
