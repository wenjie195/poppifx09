<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/userMp4Id.php" />
    <meta property="og:title" content="MT4  | Victory 5" />
    <title>MT4  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/userMp4Id.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <form action="utilities/uploadMpIdFunction.php" method="POST">
<!-- <form action="#" method="POST"> -->
    <h1 class="h1-title white-text">Please Enter MT4 Account</h1>
        <div class="dual-input">
            <p class="input-top-text">MT4</p>
            <input class="clean pop-input" type="text" placeholder="MT4" id="register_mpid" name="register_mpid" required>
        </div>

        <div class="width100 text-center">
        	<button class="clean blue-button one-button-width" name="submit"><?php echo _INDEX_REGISTER ?></button>
        </div>

    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>