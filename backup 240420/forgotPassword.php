<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/forgotPassword.php" />
    <meta property="og:title" content="Forgot Password  | Victory 5" />
    <title>Forgot Password  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/forgotPassword.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
    
    <div class="left-login-div">
    <h1 class="h1-title white-text"><?php echo _JS_FORGOT_TITLE ?></h1>     
    
        <form action="" method="POST">
            <input class="clean pop-input" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="" name="" required>
            <button class="clean width100 blue-button" name="loginButton"><?php echo _JS_SUBMIT ?></button>
            <p class="width100 text-center">	
                <a href="index.php" class="blue-link forgot-a"><?php echo _JS_LOGIN ?></a>
            </p>
          
            <div class="clear"></div>
        </form>
    </div>
    <div class="right-login-div">
    	<img src="img/forgot-password.png" alt="<?php echo _JS_LOGIN ?>" title="<?php echo _JS_LOGIN ?>"   data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow pulse black-rock absolute animated width100">
    </div>

</div>
<?php include 'js.php'; ?>
</body>
</html>