<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard  | Victory 5" />
    <title>Admin Dashboard  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/adminDashboard.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <!-- <div class="spacing-div text-center">
    	<div class="width100 text-center">
    		<img src="img/invitation.png" class="invitation-img" alt="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>">
        </div>
        <div class="clear"></div>
        <input type="hidden" id="linkCopy" value="http://localhost/poppifx/register.php?referrerUID=<?php //echo $_SESSION['uid']?>">
        <h3 class="invite-h3"><b><?php //echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://localhost/poppifx/register.php?referrerUID=<?php echo $_SESSION['uid']?></a></h3>
        <button class="invite-btn blue-button" id="copy-referral-link"><?php //echo _USERDASHBOARD_COPY ?></button>
    </div>   -->

    <!-- <div class="spacing-div text-center">
    	<div class="width100 text-center">
    		<img src="img/invitation.png" class="invitation-img" alt="<?php echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php echo _USERDASHBOARD_INVITATION_LINK ?>">
        </div>
        <div class="clear"></div>
        <input type="hidden" id="linkCopy" value="http://poppifx4u.com/register.php?referrerUID=<?php echo $_SESSION['uid']?>">
        <h3 class="invite-h3"><b><?php echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://poppifx4u.com/register.php?referrerUID=<?php echo $_SESSION['uid']?></a></h3>
        <button class="invite-btn blue-button" id="copy-referral-link"><?php echo _USERDASHBOARD_COPY ?></button>
    </div>   -->

    <div class="clear"></div>  

    <div class="width100 text-center overflow extra-margin-top">
        <button class="invite-btn blue-button ow-purple-btn">
            <a href="adminAddOnCredit.php"><?php echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></a>
        </button>
    </div> 

    <div class="clear"></div>
    
    <div class="width100 text-center overflow extra-margin-top">
        <button class="invite-btn blue-button ow-purple-btn">
            <a href="adminNewCredit.php" class="red-link"><?php echo _ADMINDASHBOARD_UPLOAD_EQUITY_PL ?></a>
        </button>
    </div> 

    <div class="clear"></div>

    <div class="invite-div">
    <h3 class="invite-h3 text-center"><b><?php echo _USERDASHBOARD_INVITED_BY_ME ?></b></h3>

	<div class="left-invite-div">    
        <?php
        $conn = connDB();
        if($getWho)
        {
            echo '<ul>';
            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                if($thisPerson->getCurrentLevel() == $lowestLevel)
                {
                    // echo '<li id="'.$thisPerson->getReferralId().'">'.$thisPerson->getReferralId().'</li>';
                    echo '<li id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().'</li>';
                }
            }

            echo '<ul>';
            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                echo '
                <script type="text/javascript">
                var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                div.innerHTML += "<ul name=\'ul-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                </script>
                ';
            }
            echo '</ul>';
        }
        $conn->close();
        ?>
		</div>
		<div class="right-invite-div">
        	<img src="img/invited.png" class="invited-img" alt="<?php echo _USERDASHBOARD_INVITED_BY_ME ?>" title="<?php echo _USERDASHBOARD_INVITED_BY_ME ?>">
        </div>
	</div> 
        
</div>

<?php include 'js.php'; ?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

</body>
</html>