<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn);
// $userDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/multibankViewSignature.php" />
    <meta property="og:title" content="LPOA  | Victory 5" />
    <title>LPOA  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/multibankViewSignature.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<!-- <?php //include 'adminHeader.php'; ?> -->
<?php include 'multibankHeader.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text print-big-div">
<h1 class="doc-h1 opacity-hover black-text" onclick="goBack()"><img src="img/back3.png" class="back-png" alt="Go Back" title="Go Back"> LPOA</h1>
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");

    $userDoc = getStatus($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

    <div class="document-div2">
        <img src="img/multibank-blue.png" class="multibank-logo" alt="MultiBank FX International" title="MultiBank FX International">
        <h2 class="black-text document-h2">Power of Attorney & Discretionary Account Terms</h2>
        <p class="document-p black-text">
            Name of Attorney:<br><br>
            Address of Attorney:<br><br>
            The below under signed here in after referred to as the Customer, hereby authorises the Attorney‐ in-fact as its agent Attorney-in-fact to buy, sell (including short sales), exchange, assign or transfer and trade for it at any price (my) (our) attorney deems fair in contracts as defined in the Customer Agreement of MultiBank FX International Corporation (“MEX”). The Customer hereby indemnifies MEX and its directors, officers, employees and agents from and against all liability arising directly or indirectly, from following Attorney‐ in‐ fact's instruction and will pay MEX promptly, on demand, any losses arising from such trades and any debit balance resulting there from.<br><br>
            In all such purchases, sales or trades, MEX is authorised to follow Attorney-in-fact's instructions in every respect and Attorney-in-fact is authorised to act for the Customer with the same force and effect as Customer might do with respect to such purchase, sales or trades and all things necessary or incidental to the furtherance of such purchases, sales or trades. MEX is directed to make available to Attorney-in-fact a copy of all statements that MEX makes available to the Customer concerning Customer's account, including, but not limited to, monthly statements, confirmations and purchase and sale agreements. The Customer hereby ratifies and confirms any and all transactions with MEX heretofore and hereafter made by the Attorney‐ in‐ fact for Customer's account.<br><br>
            The Attorney‐ in‐ fact is not authorised to withdraw from Customer's account any monies, securities or any property either in the Customer's name or otherwise unless such withdrawal or payment is specifically authorised in writing by the Customer.<br><br>
            This Power of Attorney shall remain in full force and effect until MEX receives from the Customer written notification of Customer's revocation thereof.
            <br><br>
            The Customer understands that MEX is in no way responsible for any loss to the Customer occasioned by actions of the Attorney-in-fact and that MEX does not, by implication or otherwise, endorse the operation or methods of the Attorney- in-fact.<br><br>
            Fee Acknowledgement<br><br>
            The Client agrees that MEX has been authorised to compensate The Attorney for the Asset Managers services.<br><br>
            MEX accepts no responsibility for ratifying the performance or management fees instructed by the Attorney in relation to activity on this account, and this information is provided below to MEX purely for informational purposes but MEX accepts no responsibility as calculation agent to verify that the fees instructed by the Attorney are correct and is exempt from any liability from acting upon such an instruction.<br><br>
            Management fee: 0% Per Month<br><br>
            Incentive fee: 40% Per Month<br><br>
            Note: It is the assumed to be the responsibility of the Asset Manager or Trading Agent to request any fee payment. MultiBank FX International Corporation is not responsible for any losses which may result from a failure of them to do so or from any incorrect fee amount notified to MEX by this party.<br><br>
            *Please add the applicable fee type % amount above to the relevant section and nominatefrequency of payment by ticking the box.<br><br>
        </p>
        <div class="left-signature-div">
            <p class="signature-p acc-p">Account Number: <b class="black-text"><?php echo $userDetails[0]->getMpId();?></b></p>
            <p class="signature-p">Customer Signature:</p>
            <div class="signature-div">
                <img src="uploads/<?php echo $userDoc[0]->getSignature();?>" class="signature-img" alt="Signature" title="Signature">
            </div>
            <p class="signature-date"></p>
            <p class="signature-p">Date: <?php echo date('Y-m-d',strtotime($userDoc[0]->getDateCreated()));?></p>
        </div>
        <div class="right-signature-div">
            <p class="signature-p acc-p">Customer Name: <b class="black-text"><?php echo $userDetails[0]->getUsername();?></b></p>
            <p class="signature-p">Attorney-in-fact's Signature:</p>
            <div class="signature-div">
                <img src="img/logo.png" class="signature-logo" alt="Signature" title="Signature">
            </div>
            <p class="signature-date"></p>
            <p class="signature-p">Date: <?php echo date('Y-m-d',strtotime($userDoc[0]->getDateCreated()));?></p>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<div class="invite-btn blue-button text-center print-button" onclick="window.print()"><?php echo _MULTIBANK_PRINT ?></div>
        </div>
    </div>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>

</body>
</html>