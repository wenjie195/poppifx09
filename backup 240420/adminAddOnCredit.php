<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
    $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if(in_array($_FILES["file"]["type"],$allowedFileType))
    {
        $targetPath = 'uploadExcel/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);

            foreach ($Reader as $Row)
            {
                $userID = "";
                if(isset($Row[0]))
                {
                    $userID = mysqli_real_escape_string($conn,$Row[1]);
                }
                // $userName = "";
                // if(isset($Row[0]))
                // {
                //     $userName = mysqli_real_escape_string($conn,$Row[1]);
                // }
                $point = "";
                if(isset($Row[1]))
                {
                    $point = mysqli_real_escape_string($conn,$Row[3]);
                }

                // if (!empty($userName) || !empty($point) || !empty($userID))
                if (!empty($point) || !empty($userID))
                {
                    $query = "UPDATE `mpidrawdata` SET `balance`='".$point."' WHERE  mp_id = '".$userID."' ";
                    $result = mysqli_query($conn, $query);
                    if (! empty($result))
                    {
                        // echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminAddOnCredit.php'</script>";  

                        $query = "INSERT INTO mpidbackupdata (mp_id,balance) VALUES ('".$userID."','".$point."') ";
                        $result = mysqli_query($conn, $query);
                        if (! empty($result))
                        {
                            echo "<script>alert('Excel Uploaded !');window.location='../poppifx/adminAddOnCredit.php'</script>";  
                            // echo "<script>alert('Excel Uploaded !');window.location='../poppifx2/adminAddOnCredit.php'</script>";  
                        }
                        else
                        {
                            echo "<script>alert('Fail To Upload !');window.location='../poppifx/adminAddOnCredit.php'</script>";  
                            // echo "<script>alert('Fail To Upload !');window.location='../poppifx2/adminAddOnCredit.php'</script>";  
                        }


                    }
                    else
                    {
                        echo "<script>alert('Fail To Upload !');window.location='../poppifx/adminAddOnCredit.php'</script>";  
                        // echo "<script>alert('Fail To Upload !');window.location='../poppifx2/adminAddOnCredit.php'</script>";  
                    }
                }
                else
                {
                    // echo "dunno";    
                    echo "<script>alert('ERROR');window.location='../poppifx/adminAddOnCredit.php'</script>";   
                    // echo "<script>alert('ERROR');window.location='../poppifx2/adminAddOnCredit.php'</script>";   
                }

            }

        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../poppifx/adminAddOnCredit.php'</script>";    
        // echo "<script>alert('ERROR 2');window.location='../poppifx2/adminAddOnCredit.php'</script>"; 
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminAddOnCredit.php" />
    <meta property="og:title" content="Admin Add-ons Credit  | Victory 5" />
    <title>Admin Add-ons Credit  | Victory 5</title>
    <link rel="canonical" href="https://poppifx4u.com/adminAddOnCredit.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center">

    <!-- <div class="width100 text-center overflow margin-bottom50">
        <button class="clean blue-button one-button-width">
            <a href="addOnBonus.php" class="red-link">Add On Point</a>
        </button>
    </div>  -->
	<h1 class="pop-h1"><?php echo _ADMINDASHBOARD_ADD_ONS_CREDIT ?></h1>
    <div class="outer-container">
        <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
            <label><!--<?php echo _ADMINNEWCREDIT_SELECT_FILE ?>--></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
            <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
            <button type="submit" id="submit" name="import"  class="clean blue-button one-button-width"><?php echo _JS_SUBMIT ?></button>
            <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
        </form>
    </div>

    <div class="clear"></div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>