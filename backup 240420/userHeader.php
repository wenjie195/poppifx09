<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <!-- <a href="index.php"><img src="img/logo-white.png" class="logo-img" alt="Logo" title="Logo"></a> -->
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right admin-right-menu">
            	<a href="userDashboard.php" class="menu-item menu-a"><?php echo _ADMINHEADER_DASHBOARD ?></a>
                <div class="dropdown  menu-item menu-a menu-left-margin">
                    
                    <?php echo _USERHEADER_PROFILE ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>">
	
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_PROFILE ?></a></p>
                        <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_EDIT_PROFILE ?></a></p>
                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_EDIT_PASSWORD ?></a></p>
                        <p class="dropdown-p"><a href="userBankDetails.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_BANK_ACC ?></a></p>
                        <p class="dropdown-p"><a href="uploadDoc.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_UPLOAD_DOC ?></a></p>                        
                        <p class="dropdown-p"><a href="maa.php"  class="menu-padding dropdown-a black-menu-item menu-a">LPOA</a></p>
                	</div>
                </div>   
                <div class="dropdown  menu-item menu-a menu-left-margin">
                    
                    <?php echo _USERHEADER_REFER ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_REFER ?>" title="<?php echo _USERHEADER_REFER ?>">

                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="refer.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_REFER ?></a></p>
                        <p class="dropdown-p"><a href="hierarchy.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _USERHEADER_HIERARCHY ?></a></p>
                	</div>
                </div>                

                <div class="dropdown  menu-item menu-a menu-left-margin">
                    
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="Language / 语言" title="Language / 语言">
                              
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>  
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>
<!-- -->
                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li class="title-li"><a href="userDashboard.php" class="no-padding-left"><?php echo _ADMINHEADER_DASHBOARD ?></a></li>

                                  <!-- <li class="title-li">My Team</li>            -->
                                  <li class="title-li unclickable-li"><?php echo _USERHEADER_PROFILE ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>"></li>                          
                                  <li><a href="profile.php" class="mini-li"><?php echo _USERHEADER_PROFILE ?></a></li>
                                  <li><a href="editProfile.php" class="mini-li">Edit Profile</a></li>
                                  <li><a href="editPassword.php" class="mini-li">Edit Password</a></li>
                                  <li><a href="userBankDetails.php" class="mini-li"><?php echo _USERHEADER_BANK_ACC ?></a></li>
                                  <li><a href="uploadDoc.php" class="mini-li"><?php echo _USERHEADER_UPLOAD_DOC ?></a></li>
                                  <li><a href="maa.php" class="mini-li">LPOA</a></li>
								  <li class="title-li unclickable-li"><?php echo _USERHEADER_REFER ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_REFER ?>" title="<?php echo _USERHEADER_REFER ?>"></li>                                  
                                  <li><a href="refer.php" class="mini-li"><?php echo _USERHEADER_REFER ?></a></li>
                                  <li><a href="hierarchy.php" class="mini-li"><?php echo _USERHEADER_HIERARCHY ?></a></li>
                                  <!-- <li class="title-li">Language</li> -->
                                  <li class="title-li unclickable-li">Language/语言 <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->                                                   	
            </div>
                
            
            
            
            
            
            
        </div>

</header>
