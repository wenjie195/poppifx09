<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$dailyBonusDetails = getDailyBonus($conn, " WHERE uid = ? ORDER BY date_created DESC", array("uid"), array($uid), "s");

$monthlyProBon = getMonProBon($conn);

$cntAA = 1;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/userViewDailyOld.php" />
    <link rel="canonical" href="https://victory5.co/userViewDailyOld.php" />
    <meta property="og:title" content="Member's Daily Bonus  | Victory 5" />
    <title>Member's Daily Bonus  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">
	<h1 class="pop-h1 text-center"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></h1>
    <div class="width100 shipping-div2">
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_NAME ?></th>
                        <th><?php echo _DAILY_FROM ?></th>
                        <th><?php echo _DAILY_BONUS ?></th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _DAILY_TIME ?></th>
                        <!-- <th><?php //echo _ADMINVIEWBALANCE_VIEW ?></th> -->
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($dailyBonusDetails)
                    {
                        for($cnt = 0;$cnt < count($dailyBonusDetails) ;$cnt++)
                        {
                          if ($dailyBonusDetails[$cnt]->getBonus()) {
                        ?>
                            <tr>
                                <td><?php echo $cntAA++; ?></td>
                                <td><?php echo $dailyBonusDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $dailyBonusDetails[$cnt]->getFromWho();?></td>
                                <td><?php echo "$ ".number_format($dailyBonusDetails[$cnt]->getBonus(),2);?></td>
                                <td><?php echo date('d/m/Y',strtotime($dailyBonusDetails[$cnt]->getDateCreated())) ?></td>
                                <td><?php echo date('h:i a',strtotime($dailyBonusDetails[$cnt]->getDateCreated())) ?></td>
                                <!-- <td><a href="#" class="blue-link"><?php //echo _ADMINVIEWBALANCE_VIEW ?></a></td> -->
                            </tr>
                        <?php
                      }}
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
