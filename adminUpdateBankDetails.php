<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
$uid = $_GET['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://victory5.co/adminUpdateBankDetails.php" />
    <link rel="canonical" href="https://victory5.co/adminUpdateBankDetails.php" />
    
    <meta property="og:title" content="Admin Edit Profile  | Victory 5" />
    <title>Admin Edit Profile  | Victory 5</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">

    <?php
    if(isset($_GET['uid']))
    {
    $conn = connDB();
    $customerDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_GET['uid']),"s");
    $userData = $customerDetails[0];
    ?>

        <!-- <form action="utilities/editProfileFunction.php" method="POST"> -->
        <form action="utilities/adminUpdateBankFunction.php" method="POST">
            <h3 class="small-h1-a text-center"><a href="adminUpdateCustomerDetails.php?uid=<?php echo $uid ?>"><?php echo _JS_EDIT_USER_PROFILE ?></a> | <a href="adminUpdateUserPassword.php?uid=<?php echo $uid ?>"><?php echo _JS_CHANGE_USER_PASSWORD ?></a> | <?php echo _ADMINVIEWBALANCE_EDIT.""._USERHEADER_BANK_DETAILS ?> </h3>

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _BANKDETAILS_ACC_NAME ?></p>
                        <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_ACC_NAME ?>" value="<?php echo $userData->getBankAccName();?>" id="update_bank_acc_name" name="update_bank_acc_name" required>
                    </div>



                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _BANKDETAILS_ACC_NO ?></p>
                        <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_ACC_NO ?>" value="<?php echo $userData->getBankAccNumber();?>" id="update_bank_acc_number" name="update_bank_acc_number" required>
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _BANKDETAILS_ACC_TYPE ?></p>
                        <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_ACC_TYPE ?>" value="<?php echo $userData->getBankAccType();?>" id="update_bank_acc_type" name="update_bank_acc_type" required>
            		</div>



                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _BANKDETAILS_BANK ?></p>
                        <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_BANK ?>" value="<?php echo $userData->getBankName();?>" id="update_bank_name" name="update_bank_name" required>
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _BANKDETAILS_BANK_SWIFT_CODE ?></p>
                        <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_BANK_SWIFT_CODE ?>" value="<?php echo $userData->getBankSwiftCode();?>"   id="update_bank_swift_code" name="update_bank_swift_code" required>
                    </div>


                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
                        <!-- <input class="clean pop-input" type="text" placeholder="Country" id="update_bank_country" name="update_bank_country" required> -->
                        <select class="clean pop-input" id="update_bank_country" name="update_bank_country" required>
                            <!-- <option><?php //echo _MAINJS_SELECT_COUNTRY ?></option> -->
                            <option><?php echo $userData->getBankCountry();?></option>
                            <?php
                            for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                            {
                            ?>

                            <option value="<?php echo $countryList[$cntPro]->getEnName();
                            ?>">
                            <?php
                            echo $countryList[$cntPro]->getEnName(); //take in display the options
                            ?>
                            </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="clear"></div>

            		<div class="width100 text-center">
                    	<button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
                    </div>

                    <input type="hidden" value="<?php echo $userData->getUid();?>" id="user_uid" name="user_uid" >

                </form>

        </form>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>
</body>
</html>
