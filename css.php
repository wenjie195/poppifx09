<link rel="shortcut icon" href="https://victory5.co/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://victory5.co/img/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@200;300;400;500;700;900&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" >

<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/component.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/upload-css.css">
<link rel="stylesheet" type="text/css" href="css/style.css?version=1.2.0">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-174160493-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-174160493-1');
</script>
